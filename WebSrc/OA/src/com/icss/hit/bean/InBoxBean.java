package com.icss.hit.bean;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.icss.hit.bean.interfaces.InBox;
import com.icss.hit.hibernate.HibernateSessionFactory;
import com.icss.hit.hibernate.vo.Message;
import com.icss.hit.hibernate.vo.ReceiverInfo;
import com.icss.hit.hibernate.vo.Schedule;
import com.icss.hit.hibernate.vo.SysUser;
public class InBoxBean implements InBox{
	public static int PAGE_SIZE = 10;
	
	/* (non-Javadoc)
	 * @see com.icss.hit.bean.interfaces.InBox#getInboxMessages(long, int)
	 */
	@Override
	public List<ReceiverInfo> getInBoxMessages(String searchType,String content,long receiverId,int pageNo){
		Session sess = HibernateSessionFactory.getSession();
		Transaction tx = null;
		String hql=getSearchHQL(searchType,content);
		try {
			tx = sess.beginTransaction();
			// �������ʾҳ��ʼ���
			int offset = (pageNo - 1) * PAGE_SIZE;
			Query query=null;
			// �����������Ϊ�գ�Ĭ��Ϊ����ȫ��
			if(searchType==null||content==null||content.equals("")){
				query = sess.createQuery(hql).setLong(0, receiverId);
			}
			else{
				query = sess.createQuery(hql).setLong(0, receiverId).setString(1, "%"+content+"%");
			}
			
			// ���÷�ҳ��Ϣ
			query.setFirstResult(offset).setMaxResults(PAGE_SIZE);
			List i = query.list();
			Iterator it = i.iterator();
			tx.commit();
			List<ReceiverInfo> inBoxList = new ArrayList<ReceiverInfo>();
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				ReceiverInfo r = (ReceiverInfo) obj[0];
				Message m = (Message) obj[1];
				SysUser u = (SysUser) obj[2];
				
				// ��role��dept��position����Ϊuser������
				r.setMessage(m);
				r.setSysUser(u);
				inBoxList.add(r);
			}
			if(inBoxList.size() == 0)
				return null;
			return inBoxList;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			sess.close();
		}
	}
	
	private String getSearchHQL(String searchType,String content){
		String hql = "from ReceiverInfo r inner join r.message m inner join r.sysUser s inner join m.sysUser ms "+
			"where r.riDelete = '0' and r.riBox ='3' and s.suId=? ";
		if(searchType==null || content==null || content.equals("")){
			return hql;
		}
		if(searchType.equals("name")){
			hql+="and ms.suUsername like ?";
		}
		else if(searchType.equals("title")){
			hql+="and m.msTitle like ?";
		}
		return hql;
	}
	
	/* (non-Javadoc)
	 * @see com.icss.hit.bean.interfaces.InBox#getInBoxMessagesCount(long)
	 */
	@Override
	public int getInBoxMessagesCount(String searchType,String content,long receiverId){
		Session sess = HibernateSessionFactory.getSession();
		Transaction tx = null;
		String hql="select count(r.riId)"+getSearchHQL(searchType,content);
		try {
			tx = sess.beginTransaction();
			// ��ѯָ��ID���û���ӵ��ճ̰��ŵ�����
			Query query=null;
			List list=null;
			// �����������Ϊ�գ�Ĭ��Ϊ����ȫ��
			if(searchType == null || content == null || content.equals("")){
				query = sess.createQuery(hql).setLong(0, receiverId);
			}
			else{
				query = sess.createQuery(hql).setLong(0, receiverId).setString(1, "%"+content+"%");
			}
			
			Object o = query.uniqueResult();
			tx.commit();
			return Integer.parseInt(o.toString());
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			sess.close();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.icss.hit.bean.interfaces.InBox#getPageCount(int, int)
	 */
	@Override
	public int getPageCount(int count, int pageSize) {
		return ( count + pageSize - 1) / pageSize;
	}
	
	/* (non-Javadoc)
	 * @see com.icss.hit.bean.interfaces.InBox#getReceiverInfo(long, long)
	 */
	@Override
	public ReceiverInfo getReceiverInfo(long riId,long userId){
		Session sess = HibernateSessionFactory.getSession();
		Transaction tx = null;
		String hql = "from ReceiverInfo r inner join r.message m inner join r.sysUser s inner join m.sysUser ms "+
					 "where r.riDelete = '0' and r.riBox ='3' and s.suId=? and r.riId=?";
		try {
			tx = sess.beginTransaction();

			List l = sess.createQuery(hql).setLong(0, userId).setLong(1, riId).list();
			tx.commit();
			Iterator it = l.iterator();
			if (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				ReceiverInfo ri = (ReceiverInfo) obj[0];
				Message m = (Message) obj[1];
				SysUser ru = (SysUser) obj[2];
				SysUser mu = (SysUser) obj[3];
				m.setSysUser(ru);
				ri.setMessage(m);
				ri.setSysUser(mu);
			
				return ri;
			}
			return null;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			sess.close();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.icss.hit.bean.interfaces.InBox#update(com.icss.hit.hibernate.vo.ReceiverInfo)
	 */
	@Override
	public boolean update(ReceiverInfo ri) {
		Session sess = HibernateSessionFactory.getSession();
		Transaction tx = null;
		try {
			tx = sess.beginTransaction();
			// ����ָ�����ճ�
			sess.update(ri);
			tx.commit();
			return true;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
			return false;
		} finally {
			sess.close();
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.icss.hit.bean.interfaces.InBox#delete(com.icss.hit.hibernate.vo.ReceiverInfo)
	 */
	@Override
	public boolean delete(ReceiverInfo ri) {
		Session sess = HibernateSessionFactory.getSession();
		Transaction tx = null;
		try {
			tx = sess.beginTransaction();
			// ����ָ�����ճ�
			sess.delete(ri);
			tx.commit();
			return true;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
			return false;
		} finally {
			sess.close();
		}
	}
	/* (non-Javadoc)
	 * @see com.icss.hit.bean.interfaces.InBox#delete(long, long)
	 */
	@Override
	public boolean delete(long riId, long userId) {
		Session sess = HibernateSessionFactory.getSession();
		Transaction tx = null;
		String hql = "delete ReceiverInfo r "+
					 "where r.riDelete = '1' and r.message.msDelete='1' and r.riBox ='3' and r.sysUser.suId=? and r.riId=?";
		try {
			tx = sess.beginTransaction();
			// ɾ��ָ��ID���ճ�
			Query query = sess.createQuery(hql).setLong(0, userId).setLong(1, riId);
			int re = query.executeUpdate();
			tx.commit();
			if( re > 0 )
				return true;
			return false;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
			return false;
		} finally {
			sess.close();
		}
	}
}
