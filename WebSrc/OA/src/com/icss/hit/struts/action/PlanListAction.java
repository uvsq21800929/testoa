/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import java.util.List;

import com.icss.hit.bean.WorkListBean;
import com.icss.hit.bean.interfaces.WorkList;
import com.icss.hit.hibernate.vo.Schedule;

/** 
 * ���������嵥(ǰ5��)
 * Creation date: 08-05-2009
 * @author ����
 * XDoclet definition:
 * @struts.action validate="true"
 * @struts.action-forward name="planList.succeed" path="/work/planList.jsp"
 */
public class PlanListAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// ��ǰ��¼�û�ID
		long id=-1;
		if( request.getSession().getAttribute("UserId") != null ){
			id = Long.parseLong(request.getSession().getAttribute("UserId").toString());
		}else{
			return mapping.findForward("NullLogin");
		}
		
		List<Schedule> list = null;
		if( id != -1){
			WorkList work = new WorkListBean();
			list = work.getFiveWorkPlan(id);
			request.setAttribute("planList", list);
		}
		return mapping.findForward("planList.succeed");
	}
}