/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import java.util.List;

import com.icss.hit.bean.ConfigWorkBean;
import com.icss.hit.bean.WorkPlanBean;
import com.icss.hit.bean.interfaces.ConfigWork;
import com.icss.hit.bean.interfaces.WorkPlanDao;
import com.icss.hit.hibernate.vo.Schedule;
import com.icss.hit.hibernate.vo.ScheduleConfig;
import com.icss.hit.hibernate.vo.SysUser;
import com.icss.hit.struts.form.AddWorkConfigForm;
import com.icss.hit.struts.form.ModifyCardForm;
import java.sql.Date;
/** 
 * �½��ճ̰���
 * Creation date: 08-03-2009
 * @author xw-pc
 * XDoclet definition:
 * @struts.action validate="true"
 */
public class NewWorkConfigAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		long userId =1;
		if( request.getSession().getAttribute("UserId") != null ){
			userId = Long.parseLong(request.getSession().getAttribute("UserId").toString());
		}else{
			return mapping.findForward("NullLogin");
		}
		WorkPlanDao cw = new WorkPlanBean();
		//�õ���userId�����е��ճ̰��ţ��������ж��½��İ���ʱ���Ƿ��ͻ
		List<ScheduleConfig> sc = cw.getScheuleConfig(userId);
		request.setAttribute("sclist", sc);
		return mapping.findForward("NewWorkConfig.succeed");
	}
}