/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import java.util.List;

import com.icss.hit.bean.CheckPower;
import com.icss.hit.bean.OutputGraphBean;
import com.icss.hit.bean.interfaces.outputGraphDao;
import com.icss.hit.struts.form.GraphOutputForm;
import com.icss.hit.component.DrawLine;
import java.awt.Color;
/** 
 * ����дͼƬ
 * Creation date: 08-10-2009
 * @author ��ӱ��
 * XDoclet definition:
 * @struts.action path="/graphOutput" name="graphOutputForm" input="/outputGraph.do" scope="request" validate="true"
 */
public class GraphOutputAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		GraphOutputForm graphOutputForm = (GraphOutputForm) form;// TODO Auto-generated method stub
		//����û���ID
		long uid;
		if( request.getSession().getAttribute("UserId") != null ){
			uid = Long.parseLong(request.getSession().getAttribute("UserId").toString());
		}else{
			return mapping.findForward("NullLogin");
		}

		// �ж��û�Ȩ��
		CheckPower check = new CheckPower(uid);
		check.getPower();
		if( !check.check(CheckPower.ADMIN_ROOM_REG)){
			return mapping.findForward("NullLogin");
		}
		
		
		String month = graphOutputForm.getMonth();
		String year = graphOutputForm.getYear();
		// �õ�����ͼƬ��Ӳ��·��
		String path = request.getRealPath("/file/pie/") + "\\"+year+"_"+month + "_fileline.jpg";
		outputGraphDao outputGraph = new OutputGraphBean();
		List<Integer> dayList = outputGraph.getRoomsRegistCount(year, month);
		DrawLine drawLine = DrawLine.getInstance();
		drawLine.setXYPointList("�㼯1");
		//��ӵ���
		for(int i = 1;i <=dayList.size();i++)
		{
			//���ÿһ��Ĵ��е��������ֵ
			double dayCount = dayList.get(i-1);
			drawLine.addPoint(i, dayCount);
		}
		//��ӽ��㼯
		drawLine.addPointList();
		//��ʼ��
		drawLine.init();
		//��������ͼ����
		drawLine.setTitle(year+"��"+month+"��"+"����������ͼ");
		//����X�����֣�������
		drawLine.setXTitle("����");
		//����Y�����֣�ÿ������������
		drawLine.setYTitle("ÿ����������");
		//ͼƬ�ĸ߶�
		drawLine.setWidth(746);
		drawLine.setHeight(280);
		drawLine.setBgColor(Color.LIGHT_GRAY);
		drawLine.setForeColor(Color.white);
		drawLine.setSpaceLineColor(Color.LIGHT_GRAY);
		
		//������ļ���
		if(drawLine.saveAbs(path))
		{
			ActionRedirect redirect = new ActionRedirect(mapping.findForward("graphOutput.successd"));
			redirect.addParameter("pic", "pic");
			redirect.addParameter("month", month);
			redirect.addParameter("year", year);
			return redirect;
		}
		
		return null;
		
	}
}