/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import com.icss.hit.bean.SysRoleBean;
import com.icss.hit.bean.UserInfoBean;
import com.icss.hit.bean.interfaces.SysRoleDao;
import com.icss.hit.bean.interfaces.UserInfo;
import com.icss.hit.hibernate.vo.SysRole;
import com.icss.hit.hibernate.vo.SysUser;

/** 
 * �޸��û�ϵͳ��ɫ
 * Creation date: 08-11-2009
 * @author ����
 * XDoclet definition:
 * @struts.action validate="true"
 * @struts.action-forward name="changeRole.failed" path="/role/roleArrange.jsp"
 * @struts.action-forward name="changeRole.succeed" path="/role/roleArrange.jsp"
 */
public class ChangeRoleAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// ��ɫ��ID
		long srId = -1;
		// ��Ҫ����ϵͳ��ɫ���û�ID
		long suId = -1;
		// ��ǰ��½�û�ID
		long userId = 1;
		if( request.getSession().getAttribute("UserId") != null ){
			userId = Long.parseLong(request.getSession().getAttribute("UserId").toString());
		}else{
			return mapping.findForward("NullLogin");
		}
		try{
			if(request.getParameter("role")!=null){
				srId = Long.parseLong(request.getParameter("role"));
			}
			if(request.getParameter("suId")!=null){
				suId = Long.parseLong(request.getParameter("suId"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return mapping.findForward("changeRole.failed");
		}
		String dept= request.getParameter("dept");
		String page = request.getParameter("page");
		SysRoleDao roleDao = new SysRoleBean();
		UserInfo info = new UserInfoBean();
		// �õ���Ҫ�޸Ľ�ɫ���û���Ϣ
		SysUser user = info.getUserInfo(suId);
		SysRole role = roleDao.getRoleInfo(srId);
		if(user!=null){
			user.setSysRole(role);
			info.updateUserInfo(user);
		}
		ActionRedirect redirect = new ActionRedirect(mapping.findForward("changeRole.succeed"));
		redirect.addParameter("dept",dept);
		redirect.addParameter("page", page);
		return redirect;
	}
}