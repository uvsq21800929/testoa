/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.icss.hit.bean.MessageSaveBean;
import com.icss.hit.bean.interfaces.MessageDao;
import com.icss.hit.component.PageBean;
import com.icss.hit.hibernate.vo.Message;
import com.icss.hit.hibernate.vo.MessageReceivers;
import com.icss.hit.hibernate.vo.SysUser;

/** 
 * ������ʾ�ݸ������������
 * Creation date: 08-07-2009
 * @author ��ӱ��
 * XDoclet definition:
 * @struts.action validate="true"
 * @struts.action-forward name="draftBox.successd" path="/message/draftBox.jsp"
 * @struts.action-forward name="draftBox.fail" path="/message/draftBox.jsp"
 */
public class DraftBoxAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		MessageDao draftMessage = new MessageSaveBean();
		//��ȡ��ǰ�û���ID
		long uid = 1;
		if( request.getSession().getAttribute("UserId") != null ){
			uid = Long.parseLong(request.getSession().getAttribute("UserId").toString());
		}else{
			return mapping.findForward("NullLogin");
		}
		// ������Ա������
		int count = 0;
		// ��ҳ������
		int pageCount = 0;
		
		int pageNo = 1;
		try{
			pageNo = Integer.parseInt(request.getParameter("page"));
		}catch(Exception e){
			// ��������쳣
			pageNo = 1;
		}
		// �õ������ͷ�ҳ
		count = draftMessage.getDraftMessageCount(uid);
		pageCount = draftMessage.getPageConut(count, MessageSaveBean.PAGE_SIZE);
		
		// ����ҳ��ķ�Χ
		pageNo = pageNo < 1? 1:pageNo;
		pageNo = pageNo > count? count: pageNo;
		
		List<MessageReceivers> allDraftMessage;
		//List<String[]> receiverList = null;
		//��òݸ�����������е���Ϣ
		allDraftMessage = draftMessage.getAllDraftMessage(uid, pageNo);
		//���õ�request����ȥ���Ա���ʾ
		request.setAttribute("draftMessage", allDraftMessage);
		
		// ����ǰ���ҳ����ʾ
		PageBean pagebean = new PageBean();
		pagebean.setLink("draftBox.do");
		pagebean.setTotal(pageCount);
		pagebean.setThispage(pageNo);
		
		request.setAttribute("pageString", pagebean.getPageString());
		return mapping.findForward("draftBox.successd");
		
	}
}