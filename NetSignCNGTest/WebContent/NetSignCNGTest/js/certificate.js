document
		.write("<script src='../NetSignCNGTest/js/InfosecNetSignCNGAgent.min.js'></script>");

var CertStoreName = "";// 证书存储区
var defaultDN = "/|user";// 默认证书 DN
var defaultIsSuerDN = "";// 默认证书颁发者 DN
var defaultCertSN = "";// 默认证书 SN
var CertListMode = "false" // 后台证书列表模式
var DigestArithmetic = "SHA1";// 摘要算法
var SymmetryArithmetic = "RC4"; // 对称算法
var selectCertIndex = -1; // 序号 从 0 开始
var CertIndexAry = [ -1, -1 ]; // 选择证书序号的数组 最多两张 证书

var ISCertListShowCheckBox = "false"; // 显示 证书列表时 是 复选框:true 还是 单选框：false

// var req = null;
// var xmlhttp=false;//创建一个新变量并赋值false，使用false作为判断条件说明还没有创建XMLHTTPRequest对象
var obj = new IWSAgent();

function GetValue(Id) {
	var m_nValue = Id.value;
	if (m_nValue == "") {
		return m_nValue;
	}
	var m_FindN = m_nValue.search('\n');
	var m_FindR = m_nValue.search('\r');

	if (((m_FindN - m_FindR) != 1) && (m_FindN >= 1 && m_FindR == -1)) {
		m_nValue = m_nValue.replace(/\n/g, '\r\n')
	}

	return m_nValue;
}

function GetSelectCertIndexAry() {
	var IndexTempAry = [];
	var oRadio = document.getElementsByName("certselect");
	for (var i = 0; i < oRadio.length; i++) {
		if (oRadio[i].checked == true) {
			IndexTempAry[IndexTempAry.length] = i;
		}
	}
	return IndexTempAry;
}
function buttonAddSpecCertDN_Succeed(result) {// 获取 指定证书DN 成功后的 数据处理
	/*
	 * if(result.length == 1) { selectCertIndex = 0 ; OperaFunction();//调回处理函数
	 * selectCertIndex = -1 ; return ; }
	 */
	if (result.length > 1) {
		buttonAddCertDataToTable_onclick(result);
		alert("请在下边列表中选择一张证书，然后点击确定按钮!");
		return;
	}
	if (result.length <= 0) {
		return;
	}
}
function buttonAddAllCert_Succeed(CertListData) {// 获取 所有证书 成功后数据处理

	if (CertListData[0] != undefined) {
		if (CertListData[0].errorCode != undefined) {
			alert("证书数量为空!    错误码 ： " + CertListData[0].errorCode);
			return;
		}
	}
	var m_CertListMode = top.GetCertListMode();
	if (m_CertListMode == "true") {
		var m_CertListData = top.GetStoreListModeData();
		if (m_CertListData == CertListData) {

		} else {
			top.SetStoreListModeData(CertListData);
			// alert("设置 证书 存储列表数据 ");
		}
	}
	buttonAddCertDataToTable_onclick(CertListData.length, CertListData);
	// alert("请在下边列表中选择一张证书，然后点击确定按钮!") ;
	return;
}
function buttonAddCertDataToTable_onclick(CertLength, CertData) {// 将 证书数据
																	// 添加到 证书列表
	var result = CertData;// eval('('+ret+')');
	var ret = CertLength;
	if (ret > 0) {
		var oTab = document.getElementById("table1");
		var oTrAll = oTab.getElementsByTagName("tr");
		for (i = oTrAll.length - 1; i > 0; i--) {
			oTab.deleteRow(i);
		}

		var m_nTypeCheck = ""
		if (ISCertListShowCheckBox == "true") {// 复选框
			m_nTypeCheck = 'checkbox';
		} else {// 单选框
			m_nTypeCheck = 'radio';
		}

		for (i = 0; i < ret; i++) {
			var oTr = document.createElement('tr');// 创建一个tr
			// 创建第一个td
			var oTd = document.createElement('td');
			var oInput = document.createElement('input');
			oInput.type = m_nTypeCheck;
			oInput.id = 'certselect';
			oInput.name = 'certselect';
			oInput.value = oTab.tBodies[0].rows.length + 1;
			oTd.appendChild(oInput);
			oTr.appendChild(oTd);// 将td插入tr
			// 创建第二个td certDN
			var oTd = document.createElement('td');
			oTd.innerHTML = result[i].certDN;
			oTr.appendChild(oTd);// 将td插入tr
			// 创建第三个td issuerDN
			var oTd = document.createElement('td');
			oTd.innerHTML = result[i].issuerDN;
			oTr.appendChild(oTd);// 将td插入tr
			// 创建第四个td certSN
			var oTd = document.createElement('td');
			oTd.innerHTML = result[i].certSN;
			oTr.appendChild(oTd);// 将td插入tr
			// 创建第五个td validBegin
			var oTd = document.createElement('td');
			oTd.innerHTML = result[i].validBegin;
			oTr.appendChild(oTd);// 将td插入tr
			// 创建第六个td validEnd
			var oTd = document.createElement('td');
			oTd.innerHTML = result[i].validEnd;
			oTr.appendChild(oTd);// 将td插入tr
			// 创建第7个td CertStore 不显示 证书存储区
			/*
			 * var oTd=document.createElement('td');
			 * oTd.innerHTML=result[i].CertStore; oTr.appendChild(oTd);//将td插入tr
			 */
			// 创建第8个td KeyUsage
			var oTd = document.createElement('td');
			oTd.innerHTML = result[i].KeyUsage;
			oTr.appendChild(oTd);// 将td插入tr
			// 创建第9个td CertType
			var oTd = document.createElement('td');
			oTd.innerHTML = result[i].CertType;
			oTr.appendChild(oTd);// 将td插入tr

			oTab.tBodies[0].appendChild(oTr);// 将整个tr插入到表格中
		}
	}
}
function buttonSelectCert_onclick() {// 选择 证书
	CertListMode = top.GetCertListMode();
	// alert(CertListMode);
	var IndexTempAry = GetSelectCertIndexAry();

	// 判断选择的证书 数量
	if (IndexTempAry.length <= 0) {
		alert("请选择一张证书");
		return;
	}
	if (IndexTempAry.length > 2) {
		alert("您选择了两张以上证书，请选择 两张证书");
		return;
	}

	CertIndexAry[0] = -1;
	CertIndexAry[1] = -1;

	if (IndexTempAry.length == 1) {
		CertIndexAry[0] = IndexTempAry[0];

		var m_selTemp = IndexTempAry[0] + 1;
		alert("已选择 第" + m_selTemp + "张证书");
	}

	if (IndexTempAry.length == 2) {
		CertIndexAry[0] = IndexTempAry[0];
		CertIndexAry[1] = IndexTempAry[1];

		var m_selTemp1 = IndexTempAry[0] + 1;
		var m_selTemp2 = IndexTempAry[1] + 1;
		alert("已选择 第" + m_selTemp1 + "张证书 和 第" + m_selTemp2 + "张证书");
	}

	if (CertListMode != "true") { // 清除
		var oTab = document.getElementById("table1");
		var oTrAll = oTab.getElementsByTagName("tr");
		for (i = oTrAll.length - 1; i > 0; i--) {
			oTab.deleteRow(i);
		}
	} else {
		for (var i = 0; i < oRadio.length; i++) {
			oRadio[i].checked = false;
		}
	}
	return;
}

function buttonSetDefaultDN_onclick() {
	if (FORMCert.defaultDNTextBox.value) {
		defaultDN = FORMCert.defaultDNTextBox.value;
		// alert("设置默认证书DN成功");
	} else {
		defaultDN = "";
		// alert("默认证书DN置空");
	}
}
function buttonSetDefaultIsSuerDN_onclick() {
	if (FORMCert.defaultIsSuerDNTextBox.value) {
		defaultIsSuerDN = FORMCert.defaultIsSuerDNTextBox.value;
		// alert("颁发者DN成功");
	} else {
		defaultIsSuerDN = "";
		// alert("颁发者DN置空");
	}
}
function buttonSetDefaultCertSN_onclick() {
	if (FORMCert.defaultCertSNTextBox.value) {
		defaultCertSN = FORMCert.defaultCertSNTextBox.value;
		// alert("设置证书序列号SN成功");
	} else {
		defaultCertSN = "";
		// alert("证书序列号SN置空");
	}
}

function AddAllCertInitTablePage(KeyTypeSpec) {// 获取所有证书(全部)
	//console.log("[*] KeyTypeSpec: "+KeyTypeSpec);
	var CertStoreNameSM2 = top.GetCertStoreName();

	var CertListMode = top.GetCertListMode();
	if (CertListMode == "true") {// 记住列表模式
		// 用已经存储好的 证书列表数据 显示
		var m_CertListData = top.GetStoreListModeData();
		if (m_CertListData == "") {

		} else {
			buttonAddAllCert_Succeed(m_CertListData);
			return;
		}
	}

	var m_nKeySpecValue = 0;
	if (KeyTypeSpec == "SignAndEncrypt" || KeyTypeSpec == "Sign") {
		
		var oRadio = document.getElementsByName("CertKeySpec");
		for (var i = 0; i < oRadio.length; i++) {
			if (oRadio[i].checked == true) {
				m_nKeySpecValue = oRadio[i].value;
			}
		}
	}
	
	//console.log("[*] m_nKeySpecValue: "+m_nKeySpecValue);
	
	var CertStoreNameRSA = KeyTypeSpec;

	var m_CertType = 0; // 0 RSA and SM2 ，1 RSA , 2 SM2
	var oRadioCertType = document.getElementsByName("CertTpye");
	//console.log("[*] oRadioCertType: "+oRadioCertType);
	for (var i = 0; i < oRadioCertType.length; i++) {
		if (oRadioCertType[i].checked == true) {
			m_CertType = oRadioCertType[i].value;
		}
	}
	//console.log("[*] m_CertType == ",m_CertType);
	if (m_CertType == 1) {
		CertStoreNameSM2 = "";
	}
	if (m_CertType == 2) {
		CertStoreNameRSA = "";
	}
	console.log("[*] CertStoreNameSM2: ",CertStoreNameSM2, " CertStoreNameRSA: ",CertStoreNameRSA," m_nKeySpecValue: ", m_nKeySpecValue);
	obj.IWSAGetAllCertsListInfo(CertStoreNameSM2, CertStoreNameRSA,
			m_nKeySpecValue, buttonAddAllCert_Succeed);
	return;
}
function AddAllCertTest(KeyTypeSpec) {// 获取所有证书(全部)
	var CertStoreNameSM2 = top.GetCertStoreName();
	top.SetStoreListModeData("");

	var m_nKeySpecValue = 0;
	var oRadio = document.getElementsByName("CertKeySpec");
	for (var i = 0; i < oRadio.length; i++) {
		if (oRadio[i].checked == true) {
			m_nKeySpecValue = oRadio[i].value;
		}
	}
	var CertStoreNameRSA = KeyTypeSpec;

	var m_CertType = 0; // 0 RSA and SM2 ，1 RSA , 2 SM2
	var oRadioCertType = document.getElementsByName("CertTpye");
	for (var i = 0; i < oRadioCertType.length; i++) {
		if (oRadioCertType[i].checked == true) {
			m_CertType = oRadioCertType[i].value;
		}
	}
	if (m_CertType == 1) {
		CertStoreNameSM2 = "";
	}
	if (m_CertType == 2) {
		CertStoreNameRSA = "";
	}

	obj.IWSAGetAllCertsListInfo(CertStoreNameSM2, CertStoreNameRSA,
			m_nKeySpecValue, buttonAddAllCert_Succeed);
	return;
}
function AddSpecCertDNTest(KeyTypeSpec, defaultDN) {// 按 默认证书DN 过滤证书
	buttonSetDefaultDN_onclick();
	var CertStoreNameSM2 = top.GetCertStoreName();

	top.SetStoreListModeData("");
	// alert("清空 证书存储列表 数据");

	// 注：如果不设置 秘钥过滤，此参数可忽略
	var m_nKeySpecValue = 0;
	var oRadio = document.getElementsByName("CertKeySpec");
	for (var i = 0; i < oRadio.length; i++) {
		if (oRadio[i].checked == true) {
			m_nKeySpecValue = oRadio[i].value;
		}
	}
	var CertStoreNameRSA = KeyTypeSpec;

	var m_CertType = 0; // 0 RSA and SM2 ，1 RSA , 2 SM2
	var oRadioCertType = document.getElementsByName("CertTpye");
	for (var i = 0; i < oRadioCertType.length; i++) {
		if (oRadioCertType[i].checked == true) {
			m_CertType = oRadioCertType[i].value;
		}
	}
	if (m_CertType == 1) {
		CertStoreNameSM2 = "";
	}
	if (m_CertType == 2) {
		CertStoreNameRSA = "";
	}

	obj.IWSAGetAllCertsListInfoByCertDN(CertStoreNameSM2, CertStoreNameRSA,
			defaultDN, m_nKeySpecValue, buttonAddAllCert_Succeed);
	return;

}
function AllCertByIsSuerDNAndSN(KeyTypeSpec, defaultIsSuerDN, defaultCertSN) {// 按
																				// 颁发者DN
																				// 和 证书
																				// SN
																				// 过滤证书
	var CertStoreNameSM2 = top.GetCertStoreName();
	top.SetStoreListModeData("");
	// alert("清空 证书存储列表 数据");

	// 注：如果不设置 秘钥过滤，此参数可忽略
	var m_nKeySpecValue = 0;
	var oRadio = document.getElementsByName("CertKeySpec");
	for (var i = 0; i < oRadio.length; i++) {
		if (oRadio[i].checked == true) {
			m_nKeySpecValue = oRadio[i].value;
		}
	}
	var CertStoreNameRSA = KeyTypeSpec;

	var m_CertType = 0; // 0 RSA and SM2 ，1 RSA , 2 SM2
	var oRadioCertType = document.getElementsByName("CertTpye");
	for (var i = 0; i < oRadioCertType.length; i++) {
		if (oRadioCertType[i].checked == true) {
			m_CertType = oRadioCertType[i].value;
		}
	}
	if (m_CertType == 1) {
		CertStoreNameSM2 = "";
	}
	if (m_CertType == 2) {
		CertStoreNameRSA = "";
	}

	obj.IWSAGetAllCertsListInfoByCertIssuerDNAndCertSN(CertStoreNameSM2,
			CertStoreNameRSA, defaultIsSuerDN, defaultCertSN, m_nKeySpecValue,
			buttonAddAllCert_Succeed);
	return;
}
/**
 * ************* 对外接口 获取 已经选择的证书序号
 * ********************************************************
 */
function GetSelectedCertIndexONE() {
	return CertIndexAry[0];
}
function GetSelectedCertIndexTWO() {
	return CertIndexAry[1];
}
/** ******************************************************************************************************* */
/*******************************************************************************
 * 获取 到的证书列表 按 单选按钮显示 签名证书 加密证书 全部证书 ************* KeyTypespec："SignAndEncrypt"
 * 全部证书 KeyTypespec："Encrypt" 加密操作用的 证书 KeyTypespec："Sign" 签名操作用的 证书
 * 
 * CheckBoxOrRadio:"CheckBox" 返回证书列表 中 选择框是 复选框 CheckBoxOrRadio:"Radio" 返回证书列表 中
 * 选择框是 单选框 获取 到的证书列表 按 单选或复选按钮显示 签名证书 加密证书 全部证书 *************
 ******************************************************************************/
function buttonAddAllCertInitTablePage_onclick(KeyTypeSpec, CheckBoxOrRadio) {// 获取所有证书(全部)
																				// 点击
																				// 证书
																				// tab
																				// 页面时调用
																				// ,显示证书列表为
																				// 单选按钮
	if (CheckBoxOrRadio == "CheckBox") {
		ISCertListShowCheckBox = "true";
	} else if (CheckBoxOrRadio == "Radio") {
		ISCertListShowCheckBox = "false";
	} else {
		alert("参数错误");
		return;
	}

	AddAllCertInitTablePage(KeyTypeSpec);
	return;
}
function buttonAddAllCertTest_onclick(KeyTypeSpec, CheckBoxOrRadio) {// 获取所有证书(全部)
																		// 测试按钮
																		// ,显示证书列表为
																		// 单选按钮
	if (CheckBoxOrRadio == "CheckBox") {
		ISCertListShowCheckBox = "true";
	} else if (CheckBoxOrRadio == "Radio") {
		ISCertListShowCheckBox = "false";
	} else {
		alert("参数错误");
		return;
	}
	AddAllCertTest(KeyTypeSpec);
	return;
}
function buttonAddSpecCertDNTest_onclick(KeyTypeSpec, CheckBoxOrRadio) {// 按
																		// 默认证书DN
																		// 过滤证书
																		// ,显示证书列表为
																		// 单选按钮

	buttonSetDefaultDN_onclick();
	if (defaultDN == "") {
		alert("未设置 默认 证书 DN");
		return;
	}
	if (CheckBoxOrRadio == "CheckBox") {
		ISCertListShowCheckBox = "true";
	} else if (CheckBoxOrRadio == "Radio") {
		ISCertListShowCheckBox = "false";
	} else {
		alert("参数错误");
		return;
	}

	AddSpecCertDNTest(KeyTypeSpec, defaultDN);
	return;

}
function buttonAllCertByIsSuerDNAndSN_onclick(KeyTypeSpec, CheckBoxOrRadio) {// 按
																				// 颁发者DN
																				// 和 证书
																				// SN
																				// 过滤证书
																				// ,显示证书列表为
																				// 单选按钮
	buttonSetDefaultIsSuerDN_onclick();
	buttonSetDefaultCertSN_onclick();

	if (defaultIsSuerDN == "") {
		alert("未设置 颁发者DN");
		return;
	}
	if (defaultCertSN == "") {
		alert("未设置 证书SN");
		return;
	}
	if (CheckBoxOrRadio == "CheckBox") {
		ISCertListShowCheckBox = "true";
	} else if (CheckBoxOrRadio == "Radio") {
		ISCertListShowCheckBox = "false";
	} else {
		alert("参数错误");
		return;
	}
	AllCertByIsSuerDNAndSN(KeyTypeSpec, defaultIsSuerDN, defaultCertSN);
	return;
}
/** ******************************************************************************************************** */
/**
 * ************ 获取 证书列表操作
 * 签名操作*******************************************************
 */
function buttonAddAllCertInitTablePage_Sign_onclick() {
	buttonAddAllCertInitTablePage_onclick("Sign", "Radio");
	return;
}
function buttonAddAllCertTest_Sign_onclick() {
	buttonAddAllCertTest_onclick("Sign", "Radio");
	return;
}
function buttonAddSpecCertDNTest_Sign_onclick() {
	buttonAddSpecCertDNTest_onclick("Sign", "Radio");
	return;
}
function buttonAllCertByIsSuerDNAndSN_Sign_onclick() {
	buttonAllCertByIsSuerDNAndSN_onclick("Sign", "Radio");
	return;
}
function buttonSkfGetCertList_onclick() {
	var dllFilename = top.GetDllLibreayFilePath();
	// alert("已设置Usbkeyd动态库dll文件："+top.GetDllLibreayFilename());
	obj.IWSASkfGetCertList(dllFilename, buttonAddAllCert_Succeed);
	return;
}
/**
 * ************ 获取 证书列表操作
 * 加密操作*******************************************************
 */
function buttonAddAllCertInitTablePage_Encrypt_onclick() {
	buttonAddAllCertInitTablePage_onclick("Encrypt", "Radio");
	return;
}
function buttonAddAllCertTest_Encrypt_onclick() {
	buttonAddAllCertTest_onclick("Encrypt", "Radio");
	return;
}
function buttonAddSpecCertDNTest_Encrypt_onclick() {
	buttonAddSpecCertDNTest_onclick("Encrypt", "Radio");
	return;
}
function buttonAllCertByIsSuerDNAndSN_Encrypt_onclick() {
	buttonAllCertByIsSuerDNAndSN_onclick("Encrypt", "Radio");
	return;
}

/**
 * ************ 获取 证书列表操作 全部证书
 * 操作*******************************************************
 */
function buttonAddAllCertInitTablePage_SignAndEncrypt_onclick() {
	buttonAddAllCertInitTablePage_onclick("SignAndEncrypt", "CheckBox");
	return;
}
function buttonAddAllCertTest_SignAndEncrypt_onclick() {
	buttonAddAllCertTest_onclick("SignAndEncrypt", "CheckBox");
	return;
}
function buttonAddSpecCertDNTest_SignAndEncrypt_onclick() {
	buttonAddSpecCertDNTest_onclick("SignAndEncrypt", "CheckBox");
	return;
}
function buttonAllCertByIsSuerDNAndSN_SignAndEncrypt_onclick() {
	buttonAllCertByIsSuerDNAndSN_onclick("SignAndEncrypt", "CheckBox");
	return;
}

/** ******************************************************************************************************** */
function buttonGetCertInfo_onclick() {// 获取 指定证书信息
	var IndexTempAry = GetSelectCertIndexAry();

	// 判断选择的证书 数量
	if (IndexTempAry.length <= 0) {
		alert("请选择一张证书");
		return;
	}
	if (IndexTempAry.length > 1) {
		alert("仅支持查看一张证书的信息，请选中一张证书");
		return;
	}

	obj.IWSAGetCertInfoForIndex(IndexTempAry[0],
			buttonGetCertInfo_Succeed_onclick);
	return;
}

function buttonGetCertInfo_Succeed_onclick(CertInfoData) {// 获取 指定证书信息 成功后数据处理
	if (CertInfoData == "") {
		alert("未收到数据");
	} else {
		alert(CertInfoData);
	}
}
function buttonGetCertPublicKey_onclick() {// 获取 指定 证书公钥
	var IndexTempAry = GetSelectCertIndexAry();

	// 判断选择的证书 数量
	if (IndexTempAry.length <= 0) {
		alert("请选择一张证书");
		return;
	}
	if (IndexTempAry.length > 1) {
		alert("仅支持查看一张证书的信息，请选中一张证书");
		return;
	}

	obj.IWSAGetCertPublicKeyInfoForIndex(IndexTempAry[0],
			buttonGetCertPublicKey_Succeed_onclick);
	return;

}

function buttonGetCertPublicKey_Succeed_onclick(CertPublicKeyInfoData) {// 获取 指定
																		// 证书公钥
																		// 成功后数据处理

	if (CertPublicKeyInfoData == "") {
		alert("未收到数据");
	} else {
		alert(CertPublicKeyInfoData);
	}

}