console.log("[*] NetSignCNGAgent");
var SendToIp = "http://127.0.0.1:";
var PortList = [ 63451, 63455, 63460, 52100, 52110, 52150, 43110, 45130, 47101 ];
//var m_StorePort = 0;
var m_StorePort = 63451;
var m_SetTimeOut = 6e4;
var m_SM2IssuerCert = "";
var m_Async = true;
var m_ListForm = [];
var m_ListFile = [];
var m_ListFormResponsed = [];
var m_ListFileNameResponsed = [];
var m_PlanTextConvertMode = "\r\n";
var m_CspName = "";
var SkfDeviceType = -1;
var SkfUseDeviceNameAry = [ "&Provider=", "&DevSN2Name=", "&bDevSN=" ];
var SkfUseDeviceInfoAry = [ "", "", "" ];
var SkfSetDeviceNameAry = [ "&Provider=", "&Device=", "&Application=" ];
var SkfSetDeviceInfoAry = [ "", "", "" ];
function XDRInfosec(userOptions) {
	if (!window.XDomainRequest) {
		return
	}
	var xdr = null;
	var postData = "";
	var userType = (userOptions.dataType || "").toLowerCase();
	xdr = new XDomainRequest;
	if (/^\d+$/.test(userOptions.timeout)) {
		xdr.timeout = userOptions.timeout
	}
	xdr.ontimeout = function() {
		userOptions.error(500, "timeout")
	};
	xdr.onload = function() {
		var allResponseHeaders = "Content-Length: " + xdr.responseText.length
				+ "\r\nContent-Type: " + xdr.contentType;
		var status = {
			code : 200,
			message : "success"
		};
		var responses = {
			text : xdr.responseText
		};
		try {
			if (userType === "html" || /text\/html/i.test(xdr.contentType)) {
				responses.html = xdr.responseText
			} else if (userType === "json" || userType !== "text"
					&& /\/json/i.test(xdr.contentType)) {
				try {
					responses.json = $.parseJSON(xdr.responseText)
				} catch (e) {
					status.code = 500;
					status.message = "parseerror"
				}
			} else if (userType === "xml" || userType !== "text"
					&& /\/xml/i.test(xdr.contentType)) {
				var doc = new ActiveXObject("Microsoft.XMLDOM");
				doc.async = false;
				try {
					doc.loadXML(xdr.responseText)
				} catch (e) {
					doc = undefined
				}
				if (!doc || !doc.documentElement
						|| doc.getElementsByTagName("parsererror").length) {
					status.code = 500;
					status.message = "parseerror";
					throw "Invalid XML: " + xdr.responseText
				}
				responses.xml = doc
			}
		} catch (e) {
			throw e
		} finally {
			if (status.code == 200 && status.message == "success") {
				result = eval("(" + xdr.responseText + ")");
				userOptions.success(result)
			} else {
				userOptions.error(status.code, "error", responses)
			}
		}
	};
	xdr.onprogress = function() {
	};
	xdr.onerror = function() {
		userOptions.error(500, "error", {
			text : xdr.responseText
		})
	};
	if (userOptions.data) {
		postData = userOptions.data
	}
	xdr.open(userOptions.method, userOptions.url);
	xdr.send(postData)
}
function ajaxInfosec(opts) {
	
	const socket = io('http://localhost');
	
	
	
	
	
	if (window.XDomainRequest) {
		XDRInfosec(opts);
		return
	}
	var defaults = {
		method : "GET",
		dataType : "json",
		url : "",
		data : "",
		async : true,
		cache : true,
		contentType : "application/x-www-form-urlencoded",
		success : function() {
		},
		error : function() {
		},
		custom : function() {
		},
		complete : function() {
		}
	};
	for ( var key in opts) {
		defaults[key] = opts[key]
	}
	if (typeof defaults.data === "object") {
		var str = "";
		var value = "";
		for ( var key in defaults.data) {
			value = defaults.data[key];
			if (defaults.data[key].indexOf("&") !== -1)
				value = defaults.data[key].replace(/&/g, escape("&"));
			if (key.indexOf("&") !== -1)
				key = key.replace(/&/g, escape("&"));
			str += key + "=" + value + "&"
		}
		defaults.data = str.substring(0, str.length - 1)
	}
	defaults.method = defaults.method.toUpperCase();
	defaults.cache = defaults.cache ? "" : "&" + (new Date).getTime();
	if (defaults.method === "GET" && (defaults.data || defaults.cache))
		defaults.url += "?" + defaults.data + defaults.cache;
	var oXhr = window.XMLHttpRequest ? new XMLHttpRequest : new ActiveXObject(
			"Microsoft.XMLHTTP");
	oXhr.open(defaults.method, defaults.url, defaults.async);
	oXhr.setRequestHeader("Accept",
			"application/json, text/javascript, */*; q=0.01");
	if (defaults.method === "GET")
		oXhr.send(null);
	else {
		oXhr.setRequestHeader("Content-type", defaults.contentType);
		oXhr.send(defaults.data)
	}
	/*
	console.log("success ajaxInfosec");
	var result = oXhr.responseText;
	defaults.success.call(oXhr, result);
	a developper
	*/
	
	if (!defaults.async) {
		if (oXhr.readyState === 4) {
			if (oXhr.status === 200) {
				var result;
				if (defaults.dataType === "json") {
					result = eval("(" + oXhr.responseText + ")")
				} else {
					result = oXhr.responseText
				}
				defaults.success.call(oXhr, result)
			} else {
				defaults.error.call(oXhr, oXhr.responseText)
			}
			
			var result;
			if (defaults.dataType === "json") {
				result = eval("(" + oXhr.responseText + ")")
			} else {
				result = oXhr.responseText
			}
			defaults.success.call(oXhr, result)
		}
	} else {
		oXhr.onreadystatechange = function() {
			if (oXhr.readyState === 4) {
				if (oXhr.status === 200) {
					var result;
					if (defaults.dataType === "json") {
						result = eval("(" + oXhr.responseText + ")")
					} else {
						result = oXhr.responseText
					}
					defaults.success.call(oXhr, result)
				} else {
					defaults.error()
				}
			}
		}
	}
	
}
function GetPlanTextForConvertMode(e) {
	var t = e;
	if (m_PlanTextConvertMode == "\r\n") {
		var r = t.search("\n");
		var a = t.search("\r");
		if (r - a != 1 && (r >= 1 && a == -1)) {
			t = t.replace(/\n/g, "\r\n")
		}
		return t
	}
	if (m_PlanTextConvertMode == "\n") {
		var r = t.search("\n");
		var a = t.search("\r");
		if (r - a == 1 && (r >= 1 && a > -1)) {
			t = t.replace(/\r\n/g, "\n")
		}
		return t
	}
	return t
}
function BASE64URLEN(e) {
	if (e == undefined) {
		return ""
	}
	var t = e;
	t = t.replace(/\+/g, "-");
	t = t.replace(/\//g, "_");
	return t
}
function BASE64URLDE(e) {
	if (e == undefined) {
		return ""
	}
	var t = e;
	t = t.replace(/-/g, "+");
	t = t.replace(/_/g, "/");
	return t
}
function JointSendData(e, t) {
	var r = "";
	if (t.length != e.length || t.length < 0) {
		return r
	}
	for (var a = 0; a < t.length; a++) {
		t[a] = encodeURIComponent(t[a]);
		t[a] = encodeURIComponent(t[a]);
		r = r + e[a];
		r = r + t[a]
	}
	return r
}
function JsonPlantextSpecialCharProcess(e) {
	if (e == "" || e == undefined) {
		return ""
	}
	e = decodeURIComponent(e);
	e = decodeURIComponent(e);
	return e
}
function GetPortForIE() {
	try {
		var e = new ActiveXObject("InfosecIWSAComPort.InfosecComPortMain.1");
		var t = e.GetPort();
		if (t <= 0) {
			return 0
		}
		m_StorePort = t;
		return t
	} catch (e) {
		return 0
	}
}
function sendAjaxSniffingData(t) {
	/*
	var e = Math.random();
	var r = SendToIp + t + "/PortRight" + "?randtmp=" + e;
	var a = 0;
	if (navigator.appName == "Microsoft Internet Explorer") {
		return GetPortForIE()
	}
	ajaxInfosec({
		url : r,
		method : "GET",
		timeout : 100,
		data : "",
		async : false,
		dataType : "json",
		beforeSend : function() {
		},
		complete : function(e, t) {
		},
		error : function(e, t, r) {
			a = 0;
			return
		},
		success : function(e) {
			if (e[0].errorCode == 0) {
				a = t;
				return
			} else {
				a = 0;
				return
			}
		}
	});
	return a
	*/
}
function sendAjaxSniffingDataAsync(t) {
	var e = Math.random();
	var r = SendToIp + t + "/PortRight" + "?randtmp=" + e;
	ajaxInfosec({
		url : r,
		method : "GET",
		timeout : 100,
		data : "",
		async : true,
		dataType : "json",
		beforeSend : function() {
		},
		complete : function(e, t) {
		},
		error : function(e, t, r) {
		},
		success : function(e) {
			if (e[0].errorCode == 0) {
				m_StorePort = t;
				return
			}
		}
	})
}
function PortIsRight() {
	for (var e = 0; e < PortList.length; e++) {
		if (m_StorePort == PortList[e]) {
			return true
		}
	}
	return false
}
function GetRightPORT() {
	var e = 0;
	m_StorePort = 0;
	for (var t = 0; t < PortList.length; t++) {
		var r = sendAjaxSniffingData(PortList[t]);
		if (r > 0) {
			e = r;
			m_StorePort = e;
			break
		}
	}
	return e
}
function AddFormListFromResponse(resultForm) {
	if (resultForm == "" || resultForm == undefined) {
		return
	}
	resultForm = JsonPlantextSpecialCharProcess(resultForm);
	var objForm = eval("(" + resultForm + ")");
	if (objForm.length < 0) {
		return
	}
	for (var i = 0; i < objForm.length; i++) {
		objForm[i].FormValue = JsonPlantextSpecialCharProcess(objForm[i].FormValue);
		m_ListFormResponsed.push(objForm[i].FormValue)
	}
	return
}
function AddFileNameListFileNameResponse(resultFileName) {
	if (resultFileName == "" || resultFileName == undefined) {
		return
	}
	resultFileName = JsonPlantextSpecialCharProcess(resultFileName);
	var objFileName = eval("(" + resultFileName + ")");
	if (objFileName.length < 0) {
		return
	}
	for (var i = 0; i < objFileName.length; i++) {
		objFileName[i].FileNameValue = JsonPlantextSpecialCharProcess(objFileName[i].FileNameValue);
		m_ListFileNameResponsed.push(objFileName[i].FileNameValue)
	}
	return
}
function GetStringAryMagic() {
	var e = new Array;
	for (var t = 0; t < arguments.length; t++) {
		e[t] = arguments[t]
	}
	return e
}
function SendDataSucceedProcess(e, t, r) {
	if (e == "GetVersion") {
		if (r != undefined) {
			r(t[0].Version);
			return
		}
		return t[0].Version
	} else if (e == "NSBase64Encode") {
		var a = t[0].errorCode;
		var n = t[0].TextData;
		n = BASE64URLDE(n);
		if (r != undefined) {
			r(a, n);
			return
		}
		return GetStringAryMagic(a, n)
	} else if (e == "NSBase64Decode") {
		var n = t[0].TextData;
		n = JsonPlantextSpecialCharProcess(n);
		if (r != undefined) {
			r(t[0].errorCode, n);
			return
		}
		return GetStringAryMagic(t[0].errorCode, n)
	} else if (e == "NSHashAndBase64Encode") {
		var n = t[0].TextData;
		n = BASE64URLDE(n);
		if (r != undefined) {
			r(t[0].errorCode, n);
			return
		}
		return GetStringAryMagic(t[0].errorCode, n)
	} else if (e == "NSDetachedSign" || e == "NSDetachedSignDefaultDN") {
		var i = t[0].signedData;
		i = BASE64URLDE(i);
		if (r != undefined) {
			r(t[0].errorCode, i);
			return
		}
		return GetStringAryMagic(t[0].errorCode, i)
	} else if (e == "NSDetachedVerify") {
		if (r != undefined) {
			r(t[0].errorCode);
			return
		}
		return t[0].errorCode
	} else if (e == "NSSkfGetCertsListInfo") {
		var o = 0;
		for (o = 0; o < t.length; o++) {
			t[o].certDN = JsonPlantextSpecialCharProcess(t[o].certDN);
			t[o].issuerDN = JsonPlantextSpecialCharProcess(t[o].issuerDN);
			t[o].certSN = JsonPlantextSpecialCharProcess(t[o].certSN);
			t[o].validBegin = JsonPlantextSpecialCharProcess(t[o].validBegin);
			t[o].validEnd = JsonPlantextSpecialCharProcess(t[o].validEnd);
			t[o].CertStore = JsonPlantextSpecialCharProcess(t[o].CertStore);
			t[o].KeyUsage = JsonPlantextSpecialCharProcess(t[o].KeyUsage);
			t[o].CertType = JsonPlantextSpecialCharProcess(t[o].CertType)
		}
		r(t)
	} else if (e == "NSSkfAttachedSign") {
		var i = t[0].signedData;
		i = BASE64URLDE(i);
		if (r != undefined) {
			r(t[0].errorCode, i);
			return
		}
		return GetStringAryMagic(t[0].errorCode, i)
	} else if (e == "NSSkfAttachedSign" || e == "NSSkfDetachedSign"
			|| e == "NSSkfRawSign") {
		var i = t[0].signedData;
		i = BASE64URLDE(i);
		if (r != undefined) {
			r(t[0].errorCode, i);
			return
		}
		return GetStringAryMagic(t[0].errorCode, i)
	} else if (e == "NSSkfAttachedVerify") {
		t[0].PlainText = JsonPlantextSpecialCharProcess(t[0].PlainText);
		t[0].Cert = JsonPlantextSpecialCharProcess(t[0].Cert);
		t[0].certDN = JsonPlantextSpecialCharProcess(t[0].certDN);
		t[0].issuerDN = JsonPlantextSpecialCharProcess(t[0].issuerDN);
		t[0].certSN = JsonPlantextSpecialCharProcess(t[0].certSN);
		t[0].validBegin = JsonPlantextSpecialCharProcess(t[0].validBegin);
		t[0].validEnd = JsonPlantextSpecialCharProcess(t[0].validEnd);
		var s = t[0].certDN + " " + t[0].issuerDN + " " + t[0].certSN + " "
				+ t[0].validBegin + " " + t[0].validEnd;
		if (r != undefined) {
			r(t[0].errorCode, t[0].PlainText, s, t[0].Cert);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].PlainText, t[0].CertInfo,
				t[0].Cert)
	} else if (e == "NSSkfDetachedVerify" || e == "NSSkfRawVerify") {
		t[0].Cert = JsonPlantextSpecialCharProcess(t[0].Cert);
		t[0].certDN = JsonPlantextSpecialCharProcess(t[0].certDN);
		t[0].issuerDN = JsonPlantextSpecialCharProcess(t[0].issuerDN);
		t[0].certSN = JsonPlantextSpecialCharProcess(t[0].certSN);
		t[0].validBegin = JsonPlantextSpecialCharProcess(t[0].validBegin);
		t[0].validEnd = JsonPlantextSpecialCharProcess(t[0].validEnd);
		var s = t[0].certDN + " " + t[0].issuerDN + " " + t[0].certSN + " "
				+ t[0].validBegin + " " + t[0].validEnd;
		if (r != undefined) {
			r(t[0].errorCode, s, t[0].Cert);
			return
		}
		return GetStringAryMagic(t[0].errorCode, s, t[0].Cert)
	} else if (e == "NSSkfEncryptedSignEnvelop" || e == "NSSkfEncryptedEnvelop") {
		var S = JsonPlantextSpecialCharProcess(t[0].Data);
		S = BASE64URLDE(S);
		if (r != undefined) {
			r(t[0].errorCode, S);
			return
		}
		return GetStringAryMagic(t[0].errorCode, S)
	} else if (e == "NSAttachedSign" || e == "NSAttachedSignDefaultDN"
			|| e == "NSFormFileSign" || e == "NSFormFileSignDefaultDN") {
		var i = t[0].signedData;
		i = BASE64URLDE(i);
		if (r != undefined) {
			r(t[0].errorCode, i);
			return
		}
		return GetStringAryMagic(t[0].errorCode, i)
	} else if (e == "NSAttachedVerify") {
		t[0].PlainText = JsonPlantextSpecialCharProcess(t[0].PlainText);
		t[0].SigncertDN = JsonPlantextSpecialCharProcess(t[0].SigncertDN);
		if (r != undefined) {
			r(t[0].errorCode, t[0].PlainText, t[0].SigncertDN);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].PlainText,
				t[0].SigncertDN)
	} else if (e == "NSFormFileVerify") {
		t[0].SigncertDN = JsonPlantextSpecialCharProcess(t[0].SigncertDN);
		AddFormListFromResponse(t[0].FormTotal);
		AddFileNameListFileNameResponse(t[0].FileNameTotal);
		if (r != undefined) {
			r(t[0].errorCode, t[0].SigncertDN);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].SigncertDN)
	} else if (e == "NSFormFileGetFile" || e == "NSRawVerify"
			|| e == "NSRawVerifyDefaultDN") {
		if (r != undefined) {
			r(t[0].errorCode);
			return
		}
		return t[0].errorCode
	} else if (e == "NSRawSign" || e == "NSRawSignDefaultDN") {
		var i = t[0].signedData;
		i = BASE64URLDE(i);
		if (r != undefined) {
			r(t[0].errorCode, i);
			return
		}
		return GetStringAryMagic(t[0].errorCode, i)
	} else if (e == "NSEncryptedEnvelop" || e == "NSEncryptedEnvelopDefaultDN"
			|| e == "NSFormFileEncryptedEnvelop"
			|| e == "NSFormFileEncryptedEnvelopDefaultDN") {
		var c = t[0].envelopedMsg;
		c = BASE64URLDE(c);
		if (r != undefined) {
			r(t[0].errorCode, c);
			return
		}
		return GetStringAryMagic(t[0].errorCode, c)
	} else if (e == "NSDecryptEnvelop") {
		t[0].PlainText = JsonPlantextSpecialCharProcess(t[0].PlainText);
		t[0].EncryptcertDN = JsonPlantextSpecialCharProcess(t[0].EncryptcertDN);
		if (r != undefined) {
			r(t[0].errorCode, t[0].PlainText, t[0].EncryptcertDN);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].PlainText,
				t[0].EncryptcertDN)
	} else if (e == "NSFormFileDecryptedEnvelop") {
		t[0].EncryptcertDN = JsonPlantextSpecialCharProcess(t[0].EncryptcertDN);
		AddFormListFromResponse(t[0].FormTotal);
		AddFileNameListFileNameResponse(t[0].FileNameTotal);
		if (r != undefined) {
			r(t[0].errorCode, t[0].EncryptcertDN);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].EncryptcertDN)
	} else if (e == "NSEncryptedSignEnvelop"
			|| e == "NSEncryptedSignEnvelopDefaultDN"
			|| e == "NSFormFileEncryptedSignEnvelop"
			|| e == "NSFormFileEncryptedSignEnvelopDefaultDN") {
		var c = t[0].envelopedMsg;
		c = BASE64URLDE(c);
		if (r != undefined) {
			r(t[0].errorCode, c);
			return
		}
		return GetStringAryMagic(t[0].errorCode, c)
	} else if (e == "NSDecryptSignEnvelop") {
		t[0].PlainText = JsonPlantextSpecialCharProcess(t[0].PlainText);
		t[0].SigncertDN = JsonPlantextSpecialCharProcess(t[0].SigncertDN);
		t[0].EncryptcertDN = JsonPlantextSpecialCharProcess(t[0].EncryptcertDN);
		if (r != undefined) {
			r(t[0].errorCode, t[0].PlainText, t[0].SigncertDN,
					t[0].EncryptcertDN);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].PlainText,
				t[0].SigncertDN, t[0].EncryptcertDN)
	} else if (e == "NSFormFileDecryptedSignEnvelop") {
		t[0].SigncertDN = JsonPlantextSpecialCharProcess(t[0].SigncertDN);
		t[0].EncryptcertDN = JsonPlantextSpecialCharProcess(t[0].EncryptcertDN);
		AddFormListFromResponse(t[0].FormTotal);
		AddFileNameListFileNameResponse(t[0].FileNameTotal);
		if (r != undefined) {
			r(t[0].errorCode, t[0].SigncertDN, t[0].EncryptcertDN);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].SigncertDN,
				t[0].EncryptcertDN)
	} else if (e == "NSSetCertsListInfoCache") {
		if (r != undefined) {
			r(t[0].errorCode);
			return
		}
		return t[0].errorCode
	} else if (e == "NSGetCertInfoForIndex") {
		if (r != undefined) {
			r(t[0].CertInfoData);
			return
		}
		return t[0].CertInfoData
	} else if (e == "NSGetCertPublicKeyInfoForIndex") {
		if (r != undefined) {
			r(t[0].CertPublicKeyInfoData);
			return
		}
		return t[0].CertPublicKeyInfoData
	} else if (e == "NSGetCertsListInfo") {
		var o = 0;
		for (o = 0; o < t.length; o++) {
			t[o].certDN = JsonPlantextSpecialCharProcess(t[o].certDN);
			t[o].issuerDN = JsonPlantextSpecialCharProcess(t[o].issuerDN);
			t[o].certSN = JsonPlantextSpecialCharProcess(t[o].certSN);
			t[o].validBegin = JsonPlantextSpecialCharProcess(t[o].validBegin);
			t[o].validEnd = JsonPlantextSpecialCharProcess(t[o].validEnd);
			t[o].CertStore = JsonPlantextSpecialCharProcess(t[o].CertStore);
			t[o].KeyUsage = JsonPlantextSpecialCharProcess(t[o].KeyUsage);
			t[o].CertType = JsonPlantextSpecialCharProcess(t[o].CertType)
		}
		return ReturnOnlyOneParameter(t, r)
	} else if (e == "NSGetEVersion") {
		return ReturnOnlyOneParameter(t[0].Version, r)
	} else if (e == "NSGetRsaCspListProvider"
			|| e == "NSSm2CfcaGetProviderList"
			|| e == "NSSm2SkfGetProviderList" || e == "NSSm2SkfGetDeviceList"
			|| e == "NSSm2SkfGetApplicationList"
			|| e == "NSSm2SkfGetContainerList") {
		if (r != undefined) {
			r(t);
			return
		}
		return t
	} else if (e == "NSGetRsaCspCountOfCert" || e == "NSSm2CfcaGetCountOfCert"
			|| e == "NSSm2SkfGetCountOfCert") {
		if (r != undefined) {
			r(t[0].CertCount);
			return
		}
		return t[0].CertCount
	} else if (e == "NSGetRsaCspCertInfo" || e == "NSSm2CfcaGetCertInfo") {
		t[0].Provider = JsonPlantextSpecialCharProcess(t[0].Provider);
		t[0].Container = JsonPlantextSpecialCharProcess(t[0].Container);
		t[0].DN = JsonPlantextSpecialCharProcess(t[0].DN);
		t[0].Issuer = JsonPlantextSpecialCharProcess(t[0].Issuer);
		t[0].StartDate = JsonPlantextSpecialCharProcess(t[0].StartDate);
		t[0].EndDate = JsonPlantextSpecialCharProcess(t[0].EndDate);
		t[0].CertSN = JsonPlantextSpecialCharProcess(t[0].CertSN);
		t[0].CertPurpose = JsonPlantextSpecialCharProcess(t[0].CertPurpose);
		if (r != undefined) {
			r(t[0].errorCode, t[0].Provider, t[0].Container, t[0].DN,
					t[0].Issuer, t[0].StartDate, t[0].EndDate, t[0].CertSN,
					t[0].CertPurpose);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Provider, t[0].Container,
				t[0].DN, t[0].Issuer, t[0].StartDate, t[0].EndDate,
				t[0].CertSN, t[0].CertPurpose)
	} else if (e == "NSSm2SkfGetCertInfo") {
		t[0].Provider = JsonPlantextSpecialCharProcess(t[0].Provider);
		t[0].Device = JsonPlantextSpecialCharProcess(t[0].Device);
		t[0].DeviceSN = JsonPlantextSpecialCharProcess(t[0].DeviceSN);
		t[0].application = JsonPlantextSpecialCharProcess(t[0].application);
		t[0].Container = JsonPlantextSpecialCharProcess(t[0].Container);
		t[0].DN = JsonPlantextSpecialCharProcess(t[0].DN);
		t[0].Issuer = JsonPlantextSpecialCharProcess(t[0].Issuer);
		t[0].StartDate = JsonPlantextSpecialCharProcess(t[0].StartDate);
		t[0].EndDate = JsonPlantextSpecialCharProcess(t[0].EndDate);
		t[0].CertSN = JsonPlantextSpecialCharProcess(t[0].CertSN);
		t[0].CertPurpose = JsonPlantextSpecialCharProcess(t[0].CertPurpose);
		if (r != undefined) {
			r(t[0].errorCode, t[0].Provider, t[0].Device, t[0].DeviceSN,
					t[0].application, t[0].Container, t[0].DN, t[0].Issuer,
					t[0].StartDate, t[0].EndDate, t[0].CertSN, t[0].CertPurpose);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Provider, t[0].Device,
				t[0].DeviceSN, t[0].application, t[0].Container, t[0].DN,
				t[0].Issuer, t[0].StartDate, t[0].EndDate, t[0].CertSN,
				t[0].CertPurpose)
	} else if (e == "NSAdvRsaCspGenContainerP10") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].P10Value, t[0].EncKeyPair);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].P10Value, t[0].EncKeyPair)
	} else if (e == "NSRsaCspImportSignEncP7Cert"
			|| e == "NSRsaCspImportSignEncX509Cert"
			|| e == "NSRsaCspDelContainer" || e == "NSRsaCspCreateContainer"
			|| e == "NSRsaCspDeleteContainer" || e == "NSRsaCspVerifySignData"
			|| e == "NSSm2SkfVerifySignData" || e == "NSRsaCspImportEncP7Cert"
			|| e == "NSRsaCspImportSignP7Cert"
			|| e == "NSRsaCspImportEncX509Cert"
			|| e == "NSRsaCspImportSignX509Cert"
			|| e == "NSRsaCspImportX509CertToStore"
			|| e == "NSRsaCspDelX509CertInStore"
			|| e == "NSRsaCspDelEncKeyPair" || e == "NSSm2CfcaCreateContainer"
			|| e == "NSSm2CfcaDelContainer" || e == "NSSm2CfcaDeleteContainer"
			|| e == "NSSm2CfcaImportSignX509Cert"
			|| e == "NSSm2CfcaImportEncX509Cert"
			|| e == "NSSm2SkfImportSignX509Cert"
			|| e == "NSSm2SkfImportEncX509Cert" || e == "NSSm2SkfWriteData"
			|| e == "NSSm2SkfDelContainer" || e == "NSSm2SkfCreateContainer"
			|| e == "NSSm2SkfDeleteContainer" || e == "NSSm2SkfVerifyPin"
			|| e == "NSSm2SkfVerifyPinOfCtrl"
			|| e == "NSSetCertEnrollShowErrMsg"
			|| e == "NSSm2CfcaImportSignEncX509Cert"
			|| e == "NSSm2SkfImportSignEncX509Cert") {
		if (r != undefined) {
			r(t[0].errorCode);
			return
		}
		return t[0].errorCode
	} else if (e == "NSRsaCspGenContainer" || e == "NSSm2CfcaGenContainer"
			|| e == "NSSm2SkfGenContainer") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].Container);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Container)
	} else if (e == "NSRsaCspSignData" || e == "NSRsaCspEncryptData"
			|| e == "NSRsaCspDecryptData"
			|| e == "NSRsaCspExportContainerPfxCert"
			|| e == "NSRsaCspExportPfxCert"
			|| e == "NSRsaCspExportSignX509Cert"
			|| e == "NSRsaCspExportEncX509Cert" || e == "NSRsaCspGenEncKeyPair"
			|| e == "NSRsaCspGenP10" || e == "NSGenSecureAuthCode"
			|| e == "NSGetX509FromP7Cert" || e == "NSSm2CfcaSignData"
			|| e == "NSSm2SkfSignData" || e == "NSSm2SkfEncryptData"
			|| e == "NSSm2SkfDecryptData" || e == "NSSm2SkfExportSignX509Cert"
			|| e == "NSSm2SkfExportEncX509Cert" || e == "NSSm2SkfGenP10"
			|| e == "NSSm2SkfGetEnveloped") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].Data);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Data)
	} else if (e == "NSSm2SkfReadData") {
		t[0].Data = JsonPlantextSpecialCharProcess(t[0].Data);
		if (r != undefined) {
			r(t[0].errorCode, t[0].Data);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Data)
	} else if (e == "NSRsaCspGenContainerP10" || e == "NSSm2SkfGenContainerP10") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].Container, t[0].P10);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Container, t[0].P10)
	} else if (e == "NSGetRsaCertStore" || e == "NSGetSm2CFCACertStore") {
		return ReturnOnlyOneParameter(t[0].CertStore, r)
	} else if (e == "NSGetDistinctNameKeyValuePairNumber") {
		return ReturnOnlyOneParameter(t[0].Count, r)
	} else if (e == "NSGetDistinctNameKeyValuePair") {
		t[0].Key = JsonPlantextSpecialCharProcess(t[0].Key);
		t[0].Value = JsonPlantextSpecialCharProcess(t[0].Value);
		if (r != undefined) {
			r(t[0].errorCode, t[0].Key, t[0].Value);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Key, t[0].Value)
	} else if (e == "NSCompareCertTime") {
		return ReturnOnlyOneParameter(t[0].RetResult, r)
	} else if (e == "NSGetX509CertInfo") {
		t[0].DN = JsonPlantextSpecialCharProcess(t[0].DN);
		t[0].Issuer = JsonPlantextSpecialCharProcess(t[0].Issuer);
		t[0].StartDate = JsonPlantextSpecialCharProcess(t[0].StartDate);
		t[0].EndDate = JsonPlantextSpecialCharProcess(t[0].EndDate);
		t[0].CertSN = JsonPlantextSpecialCharProcess(t[0].CertSN);
		t[0].CertPurpose = JsonPlantextSpecialCharProcess(t[0].CertPurpose);
		if (r != undefined) {
			r(t[0].errorCode, t[0].DN, t[0].Issuer, t[0].StartDate,
					t[0].EndDate, t[0].CertSN, t[0].CertPurpose);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].DN, t[0].Issuer,
				t[0].StartDate, t[0].EndDate, t[0].CertSN, t[0].CertPurpose)
	} else if (e == "NSSm2CfcaGenContainerP10Encpubkey") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].Container, t[0].P10, t[0].Pubkey);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Container, t[0].P10,
				t[0].Pubkey)
	} else if (e == "NSSm2CfcaGenP10Encpubkey") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].P10, t[0].Pubkey);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].P10, t[0].Pubkey)
	} else if (e == "NSSm2SkfGetDeviceInfo") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].SN, t[0].Lable);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].SN, t[0].Lable)
	} else if (e == "NSSm2SkfGetPinInfo") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].MaxNum, t[0].NowSurplusNum);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].MaxNum,
				t[0].NowSurplusNum)
	} else if (e == "NSLoginExec" || e == "NSLoginExecParams"
			|| e == "NSLoginStartAll" || e == "NSLoginStopAll"
			|| e == "NSLoginStartMessageLoginInBack"
			|| e == "NSLoginStopMessageLoginInBack"
			|| e == "NSLoginStartMessageLogin"
			|| e == "NSLoginStartTabLoginLoginInBack"
			|| e == "NSLoginStopTabLoginInBack"
			|| e == "NSLoginStartTabLoginLogin" || e == "NSLoginIsLoginFailed"
			|| e == "NSLoginStartMessageMultiWindowLogin"
			|| e == "NSLoginautoLoginHTTP" || e == "NSLoginStartLogin"
			|| e == "NSLoginSetFailFormat"
			|| e == "NSLoginStartMessageLoginHXPOC") {
		if (r != undefined) {
			r(t[0].errorCode);
			return
		}
		return t[0].errorCode
	} else if (e == "NSLoginEncrypt" || e == "NSLoginDecrypt") {
		if (r != undefined) {
			r(t[0].errorCode, t[0].Data);
			return
		}
		return GetStringAryMagic(t[0].errorCode, t[0].Data)
	}
	return
}
function ReturnOnlyOneParameter(e, t) {
	if (t != undefined) {
		t(e);
		return
	}
	return e
}
function RetrySendAjaxData(t, e, r, a) {
	var n = Math.random();
	if (PortIsRight() == false) {
		GetRightPORT()
	}
	if (PortIsRight() == false) {
		return
	}
	var i = SendToIp + m_StorePort + "/" + t + "?randtmp=" + n;
	ajaxInfosec({
		url : i,
		method : e,
		data : r,
		dataType : "json",
		beforeSend : function() {
		},
		complete : function(e, t) {
		},
		error : function(e, t, r) {
		},
		success : function(e) {
			SendDataSucceedProcess(t, e, a)
		},
		timeout : m_SetTimeOut
	});
	return
}
function sendAjaxData(a, n, i, o) {
	var e = Math.random();
	var t;
	if (PortIsRight() == false) {
		GetRightPORT()
	}
	if (PortIsRight() == false) {
		return t
	}
	var r = SendToIp + m_StorePort + "/" + a + "?randtmp=" + e;
	console.log(r);
	ajaxInfosec({
		url : r,
		method : n,
		data : i,
		async : m_Async,
		dataType : "json",
		beforeSend : function() {
		},
		complete : function(e, t) {
		},
		error : function(e, t, r) {
			if (m_Async == true) {
				RetrySendAjaxData(a, n, i, o)
			}
		},
		success : function(e) {
			console.log("send success.");
			t = SendDataSucceedProcess(a, e, o)
		},
		timeout : m_SetTimeOut
	});
	return t
}
function GetDigestArithmeticByName(e) {
	if (e == "SM3") {
		return "1.2.156.10197.1.401"
	}
	if (e == "SHA1") {
		return "1.3.14.3.2.26"
	}
	if (e == "MD5") {
		return "1.2.840.113549.2.5"
	}
	if (e == "SHA256") {
		return "2.16.840.1.101.3.4.2.1"
	}
	return ""
}
function GetSymmetryArithmeticByName(e) {
	if (e == "RC4") {
		return "1.2.840.113549.3.4"
	}
	if (e == "3DES") {
		return "1.2.840.113549.3.7"
	}
	if (e == "RC2") {
		return "1.2.840.113549.3.2"
	}
	if (e == "DES") {
		return "1.3.14.3.2.7"
	}
	return ""
}
function GetCertStoreName(e, t) {
	var r = "";
	if (t == "SignAndEncrypt") {
		r = "MY,ROOT,CA,AddressBook"
	}
	if (t == "Encrypt") {
		r = "ROOT,CA,AddressBook"
	}
	if (t == "Sign") {
		r = "MY"
	}
	if (e == "" || e == undefined) {
		return r
	} else {
		if (r != "") {
			e = e + ",";
			e = e + r
		}
	}
	return e
}
function PrefixInteger(e, t) {
	return (Array(t).join(0) + e).slice(-t)
}
function mbStringLength(e) {
	var t = 0;
	var r;
	var a;
	for (r = 0; r < e.length; r++) {
		a = e.charCodeAt(r);
		if (a < 127) {
			t = t + 1
		} else if (128 <= a && a <= 2047) {
			t += 2
		} else if (2048 <= a && a <= 65535) {
			t += 3
		} else if (65536 <= a && a <= 2097151) {
			t += 4
		} else if (2097152 <= a && a <= 67108863) {
			t += 5
		} else if (67108864 <= a && a <= 2147483647) {
			t += 6
		}
	}
	return t
}
function GetSendFormFilePlantText() {
	var e = "";
	for (var t = 0; t < m_ListForm.length; t++) {
		e += "01";
		var r = mbStringLength(m_ListForm[t]);
		e += PrefixInteger(r, 31);
		e += m_ListForm[t]
	}
	for (var t = 0; t < m_ListFile.length; t++) {
		e += "02";
		var r = mbStringLength(m_ListFile[t]);
		e += PrefixInteger(r, 31);
		e += m_ListFile[t]
	}
	return e
}
function AttachedSignVerify(e, t, r, a) {
	r = BASE64URLEN(r);
	if (r == "") {
	}
	var n = [ "PortGrade=", "&signedMsg=" ];
	var i = [ t, r ];
	if (t == 1) {
		n = [ "PortGrade=", "&signedMsg=" ];
		i = [ t, r ]
	}
	if (t == 2) {
		m_SM2IssuerCert = BASE64URLEN(m_SM2IssuerCert);
		n = [ "PortGrade=", "&signedMsg=", "&SM2IssuerCert=" ];
		i = [ t, r, m_SM2IssuerCert ]
	}
	var o = JointSendData(n, i);
	return sendAjaxData(e, "POST", o, a)
}
function DecryptEnvelop(e, t, r) {
	if (t == "") {
	}
	t = BASE64URLEN(t);
	var a = [ "envelopedMsg=" ];
	var n = [ t ];
	var i = JointSendData(a, n);
	return sendAjaxData(e, "POST", i, r)
}
function DecryptSignEnvelop(e, t, r) {
	if (t == "") {
	}
	t = BASE64URLEN(t);
	var a = [ "envelopedMsg=" ];
	var n = [ t ];
	var i = JointSendData(a, n);
	return sendAjaxData(e, "POST", i, r)
}
function clearResponsedList() {
	m_ListFormResponsed = [];
	m_ListFileNameResponsed = []
}
function GetAllreadyDeviceName() {
	var e = [ "SkfDeviceType=", "&Null=", "&Null=", "&Null=" ];
	if (SkfDeviceType == 1) {
		e[1] = SkfUseDeviceNameAry[0];
		e[2] = SkfUseDeviceNameAry[1];
		e[3] = SkfUseDeviceNameAry[2]
	}
	if (SkfDeviceType == 2) {
		e[1] = SkfSetDeviceNameAry[0];
		e[2] = SkfSetDeviceNameAry[1];
		e[3] = SkfSetDeviceNameAry[2]
	}
	return e
}
function GetAllreadyDeviceInfo() {
	var e = [ "", "", "", "" ];
	e[0] = SkfDeviceType;
	if (SkfDeviceType == 1) {
		e[1] = SkfUseDeviceInfoAry[0];
		e[2] = SkfUseDeviceInfoAry[1];
		e[3] = SkfUseDeviceInfoAry[2]
	}
	if (SkfDeviceType == 2) {
		e[1] = SkfSetDeviceInfoAry[0];
		e[2] = SkfSetDeviceInfoAry[1];
		e[3] = SkfSetDeviceInfoAry[2]
	}
	return e
}
function GetSkfTitleParameterAndAllreadyDevice() {
	var e = new Array;
	var t = GetAllreadyDeviceName();
	for (var r = 0; r < t.length; r++) {
		e[r] = t[r]
	}
	var a = t.length;
	var n = "";
	for (var i = 0; i < arguments.length; i++) {
		n = "&";
		n = n + arguments[i];
		n = n + "=";
		e[a + i] = n
	}
	return e
}
function GetSkfDataParameterAndAllreadyDevice() {
	var e = new Array;
	var t = GetAllreadyDeviceInfo();
	for (var r = 0; r < t.length; r++) {
		e[r] = t[r]
	}
	var a = t.length;
	for (var n = 0; n < arguments.length; n++) {
		e[a + n] = arguments[n]
	}
	return e
}
function IWSAgent() {
	var e = new Object;
	
	e.IWSASendAvailable = function() {
		m_StorePort = 0;
		if (navigator.appName == "Microsoft Internet Explorer") {
			return GetPortForIE()
		}
		for (var e = 0; e < PortList.length; e++) {
			sendAjaxSniffingDataAsync(PortList[e])
		}
	};
	e.IWSAGetAvailable = function() {
		return PortIsRight()
	};
	e.IWSAAvailable = function() {
		for (var e = 0; e < PortList.length; e++) {
			var t = sendAjaxSniffingData(PortList[e]);
			if (t != 0) {
				m_StorePort = t;
				return true
			}
		}
		return false
	};
	e.IWSASetAsyncMode = function(e) {
		m_Async = e;
		return
	};
	e.IWSAGetAsyncMode = function() {
		return m_Async
	};
	e.IWSASetTimeOut = function(e) {
		m_SetTimeOut = e;
		return
	};
	e.IWSASetPlanTextConvertMode = function(e) {
		m_PlanTextConvertMode = e;
		return
	};
	e.IWSASetSM2IssuerCert = function(e) {
		m_SM2IssuerCert = e;
		return
	};
	e.IWSAGetVersion = function(e) {
		return sendAjaxData("GetVersion", "GET", "", e)
	};
	e.IWSABase64Encode = function(e, t) {
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var r = [ "PlainText=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSBase64Encode", "POST", n, t)
	};
	e.IWSABase64Decode = function(e, t) {
		if (e == "") {
		}
		e = BASE64URLEN(e);
		var r = [ "PlainText=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSBase64Decode", "POST", n, t)
	};
	e.IWSAHashAndBase64Encode = function(e, t, r) {
		t = GetDigestArithmeticByName(t);
		if (e == "" || t == "") {
		}
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var a = [ "PlainText=", "&DigestArithmetic=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSHashAndBase64Encode", "POST", i, r)
	};
	e.IWSADetachedSign = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (t != "") {
			t = GetPlanTextForConvertMode(t)
		}
		var i = [ "PortGrade=", "&PlainText=", "&CertIndex=",
				"&DigestArithmetic=" ];
		var o = [ e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSDetachedSign", "POST", s, n)
	};
	e.IWSADetachedVerify = function(e, t, r, a) {
		t = BASE64URLEN(t);
		if (t == "" || r == "") {
		}
		if (r != "") {
			r = GetPlanTextForConvertMode(r)
		}
		var n = [ "PortGrade=", "&signedMsg=", "&PlainText=", "&SM2IssuerCert=" ];
		var i = [ "", "", "", "" ];
		if (e == 1) {
			n = [ "PortGrade=", "&signedMsg=", "&PlainText=" ];
			i = [ e, t, r ]
		}
		if (e == 2) {
			m_SM2IssuerCert = BASE64URLEN(m_SM2IssuerCert);
			n = [ "PortGrade=", "&signedMsg=", "&PlainText=", "&SM2IssuerCert=" ];
			i = [ e, t, r, m_SM2IssuerCert ]
		}
		var o = JointSendData(n, i);
		return sendAjaxData("NSDetachedVerify", "POST", o, a)
	};
	e.IWSAAttachedSign = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (t != "") {
			t = GetPlanTextForConvertMode(t)
		}
		var i = [ "PortGrade=", "&PlainText=", "&CertIndex=",
				"&DigestArithmetic=" ];
		var o = [ e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSAttachedSign", "POST", s, n)
	};
	e.IWSAAttachedSignDefaultDN = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		CertStore = GetCertStoreName("", "Sign");
		var i = "";
		if (r == "1") {
			i = "2"
		}
		if (r == "0") {
			i = "0"
		}
		var o = [ "PortGrade=", "&PlainText=", "&CertStore=", "&defultDN=",
				"&Keyspec=", "&DigestArithmetic=" ];
		var s = [ 1, e, CertStore, t, i, a ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSAttachedSignDefaultDN", "POST", S, n)
	};
	e.IWSAAttachedSignAdvDefaultDN = function(e, t, r, a, n, i, o) {
		i = GetDigestArithmeticByName(i);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var s = GetCertStoreName("", "Sign");
		var S = "";
		if (n == "1") {
			S = "2"
		}
		if (n == "0") {
			S = "0"
		}
		var c = [ "PortGrade=", "&PlainText=", "&CertStore=", "&CertStoreSM2=",
				"&CertStoreType=", "&defultDN=", "&Keyspec=",
				"&DigestArithmetic=" ];
		var d = [ 2, e, s, t, r, a, S, i ];
		var f = JointSendData(c, d);
		return sendAjaxData("NSAttachedSignDefaultDN", "POST", f, o)
	};
	e.IWSADetachedSignDefaultDN = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var i = GetCertStoreName("", "Sign");
		var o = "";
		if (r == "1") {
			o = "2"
		}
		if (r == "0") {
			o = "0"
		}
		var s = [ "PortGrade=", "&PlainText=", "&CertStore=", "&defultDN=",
				"&Keyspec=", "&DigestArithmetic=" ];
		var S = [ 1, e, i, t, o, a ];
		var c = JointSendData(s, S);
		return sendAjaxData("NSDetachedSignDefaultDN", "POST", c, n)
	};
	e.IWSADetachedSignAdvDefaultDN = function(e, t, r, a, n, i, o) {
		i = GetDigestArithmeticByName(i);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var s = GetCertStoreName("", "Sign");
		var S = "";
		if (n == "1") {
			S = "2"
		}
		if (n == "0") {
			S = "0"
		}
		var c = [ "PortGrade=", "&PlainText=", "&CertStore=", "&CertStoreSM2=",
				"&CertStoreType=", "&defultDN=", "&Keyspec=",
				"&DigestArithmetic=" ];
		var d = [ 2, e, s, t, r, a, S, i ];
		var f = JointSendData(c, d);
		return sendAjaxData("NSDetachedSignDefaultDN", "POST", f, o)
	};
	e.IWSAAttachedVerify = function(e, t, r) {
		return AttachedSignVerify("NSAttachedVerify", e, t, r)
	};
	e.IWSARawSign = function(e, t, r, a) {
		r = GetDigestArithmeticByName(r);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var n = [ "PlainText=", "&CertIndex=", "&DigestArithmetic=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSRawSign", "POST", o, a)
	};
	e.IWSARawSignDefaultDN = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var i = GetCertStoreName("", "Sign");
		var o = "";
		if (r == "1") {
			o = "2"
		}
		if (r == "0") {
			o = "0"
		}
		var s = [ "PlainText=", "&CertStore=", "&defultDN=", "&Keyspec=",
				"&DigestArithmetic=" ];
		var S = [ e, i, t, o, a ];
		var c = JointSendData(s, S);
		return sendAjaxData("NSRawSignDefaultDN", "POST", c, n)
	};
	e.IWSARawVerifyDefaultDN = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		t = BASE64URLEN(t);
		var i = GetCertStoreName("", "Encrypt");
		var o = [ "PlainText=", "&signedMsg=", "&CertStore=", "&defultDN=",
				"&DigestArithmetic=" ];
		var s = [ e, t, i, r, a ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSRawVerifyDefaultDN", "POST", S, n)
	};
	e.IWSARawVerify = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		t = BASE64URLEN(t);
		var i = [ "PlainText=", "&signedMsg=", "&CertIndex=",
				"&DigestArithmetic=" ];
		var o = [ e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSRawVerify", "POST", s, n)
	};
	e.IWSAAddForm = function(e) {
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		m_ListForm.push(e);
		return
	};
	e.IWSAAddFile = function(e) {
		m_ListFile.push(e);
		return
	};
	e.IWSAClearFormList = function() {
		m_ListForm = [];
		return
	};
	e.IWSAClearFileList = function() {
		m_ListFile = [];
		return
	};
	e.IWSAGetAlreadyFormList = function() {
		return m_ListForm
	};
	e.IWSAGetAlreadyFileList = function() {
		return m_ListFile
	};
	e.IWSAGetResponsedForm = function() {
		var e = "";
		if (m_ListFormResponsed.length > 0) {
			e = m_ListFormResponsed[0];
			m_ListFormResponsed = m_ListFormResponsed.slice(1)
		}
		return e
	};
	e.IWSAGetResponsedFileName = function() {
		var e = "";
		if (m_ListFileNameResponsed.length > 0) {
			e = m_ListFileNameResponsed[0]
		}
		return e
	};
	e.IWSAGetResponsedFile = function(e, t) {
		var r = "";
		if (m_ListFileNameResponsed.length > 0) {
			r = m_ListFileNameResponsed[0];
			m_ListFileNameResponsed = m_ListFileNameResponsed.slice(1)
		}
		var a = [ "FileName=", "&FilePathName=" ];
		var n = [ r, e ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSFormFileGetFile", "POST", i, t)
	};
	e.IWSAFormFileSign = function(e, t, r) {
		var a = GetSendFormFilePlantText();
		t = GetDigestArithmeticByName(t);
		var n = [ "PortGrade=", "&PlainText=", "&CertIndex=",
				"&DigestArithmetic=" ];
		var i = [ "2", a, e, t ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSFormFileSign", "POST", o, r)
	};
	e.IWSAFormFileSignDefaultDN = function(e, t, r, a, n, i) {
		var o = GetSendFormFilePlantText();
		n = GetDigestArithmeticByName(n);
		var s = GetCertStoreName("", "Sign");
		var S = "";
		if (a == "1") {
			S = "2"
		}
		if (a == "0") {
			S = "0"
		}
		var c = [ "PortGrade=", "&PlainText=", "&CertStore=", "&CertStoreSM2=",
				"&CertStoreType=", "&defultDN=", "&Keyspec=",
				"&DigestArithmetic=" ];
		var d = [ 2, o, s, e, t, r, S, n ];
		var f = JointSendData(c, d);
		return sendAjaxData("NSFormFileSignDefaultDN", "POST", f, i)
	};
	e.IWSAFormFileVerify = function(e, t) {
		clearResponsedList();
		return AttachedSignVerify("NSFormFileVerify", "2", e, t)
	};
	e.IWSAEncryptedEnvelop = function(e, t, r, a) {
		r = GetSymmetryArithmeticByName(r);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var n = [ "PlainText=", "&CertIndex=", "&SymmetryArithmetic=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSEncryptedEnvelop", "POST", o, a)
	};
	e.IWSAEncryptedEnvelopDefaultDN = function(e, t, r, a) {
		r = GetSymmetryArithmeticByName(r);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var n = GetCertStoreName("", "Encrypt");
		var i = [ "PlainText=", "&CertStore=", "&defultDN=",
				"&SymmetryArithmetic=" ];
		var o = [ e, n, t, r ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSEncryptedEnvelopDefaultDN", "POST", s, a)
	};
	e.IWSADecryptEnvelop = function(e, t) {
		return DecryptEnvelop("NSDecryptEnvelop", e, t)
	};
	e.IWSAFormFileEncryptedEnvelop = function(e, t, r) {
		var a = GetSendFormFilePlantText();
		t = GetSymmetryArithmeticByName(t);
		var n = [ "PlainText=", "&CertIndex=", "&SymmetryArithmetic=" ];
		var i = [ a, e, t ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSFormFileEncryptedEnvelop", "POST", o, r)
	};
	e.IWSAFormFileEncryptedEnvelopDefaultDN = function(e, t, r) {
		var a = GetSendFormFilePlantText();
		var n = GetCertStoreName("", "Encrypt");
		t = GetSymmetryArithmeticByName(t);
		var i = [ "PlainText=", "&CertStore=", "&defultDN=",
				"&SymmetryArithmetic=" ];
		var o = [ a, n, e, t ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSFormFileEncryptedEnvelopDefaultDN", "POST", s, r)
	};
	e.IWSAFormFileDecryptEnvelop = function(e, t) {
		clearResponsedList();
		return DecryptEnvelop("NSFormFileDecryptedEnvelop", e, t)
	};
	e.IWSAEncryptedSignEnvelop = function(e, t, r, a, n, i) {
		a = GetDigestArithmeticByName(a);
		n = GetSymmetryArithmeticByName(n);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var o = [ "PlainText=", "&SignCertIndex=", "&EncryptCertIndex=",
				"&DigestArithmetic=", "&SymmetryArithmetic=" ];
		var s = [ e, t, r, a, n ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSEncryptedSignEnvelop", "POST", S, i)
	};
	e.IWSAEncryptedSignEnvelopDefaultDN = function(e, t, r, a, n, i, o) {
		n = GetDigestArithmeticByName(n);
		i = GetSymmetryArithmeticByName(i);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var s = GetCertStoreName("", "Sign");
		var S = GetCertStoreName("", "Encrypt");
		var c = "";
		if (a == "1") {
			c = "2"
		}
		if (a == "0") {
			c = "0"
		}
		var d = [ "PlainText=", "&CertStore=", "&CertStore1=",
				"&defultDNSign=", "&defultDNEncrypt=", "&Keyspec=",
				"&DigestArithmetic=", "&SymmetryArithmetic=" ];
		var f = [ e, s, S, t, r, c, n, i ];
		var l = JointSendData(d, f);
		return sendAjaxData("NSEncryptedSignEnvelopDefaultDN", "POST", l, o)
	};
	e.IWSADecryptSignEnvelop = function(e, t) {
		DecryptSignEnvelop("NSDecryptSignEnvelop", e, t);
		return
	};
	e.IWSAFormFileEncryptedSignEnvelop = function(e, t, r, a, n) {
		var i = GetSendFormFilePlantText();
		r = GetDigestArithmeticByName(r);
		a = GetSymmetryArithmeticByName(a);
		var o = [ "PlainText=", "&SignCertIndex=", "&EncryptCertIndex=",
				"&DigestArithmetic=", "&SymmetryArithmetic=" ];
		var s = [ i, e, t, r, a ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSFormFileEncryptedSignEnvelop", "POST", S, n)
	};
	e.IWSAFormFileEncryptedSignEnvelopDefaultDN = function(e, t, r, a, n, i) {
		var o = GetSendFormFilePlantText();
		a = GetDigestArithmeticByName(a);
		n = GetSymmetryArithmeticByName(n);
		var s = GetCertStoreName("", "Sign");
		var S = GetCertStoreName("", "Encrypt");
		var c = "";
		if (r == "1") {
			c = "2"
		}
		if (r == "0") {
			c = "0"
		}
		var d = [ "PlainText=", "&CertStore=", "&CertStore1=",
				"&defultDNSign=", "&defultDNEncrypt=", "&Keyspec=",
				"&DigestArithmetic=", "&SymmetryArithmetic=" ];
		var f = [ o, s, S, e, t, c, a, n ];
		var l = JointSendData(d, f);
		return sendAjaxData("NSFormFileEncryptedSignEnvelopDefaultDN", "POST",
				l, i)
	};
	e.IWSAFormFileDecryptSignEnvelop = function(e, t) {
		clearResponsedList();
		DecryptSignEnvelop("NSFormFileDecryptedSignEnvelop", e, t);
		return
	};
	e.IWSAGetAllCertsListInfo = function(e, t, r, a) {
		CertStore = GetCertStoreName(e, t);
		var n = [ "CertStore=", "&Keyspec=" ];
		var i = [ CertStore, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSGetCertsListInfo", "POST", o, a)
	};
	e.IWSAGetAllCertsListInfoByCertDN = function(e, t, r, a, n) {
		CertStore = GetCertStoreName(e, t);
		var i = [ "CertStore=", "&defultDN=", "&Keyspec=" ];
		var o = [ CertStore, r, a ];
		var s = JointSendData(i, o);
		sendAjaxData("NSGetCertsListInfo", "POST", s, n);
		return
	};
	e.IWSAGetAllCertsListInfoByCertIssuerDNAndCertSN = function(e, t, r, a, n,
			i) {
		CertStore = GetCertStoreName(e, t);
		var o = [ "CertStore=", "&IssuerDN=", "&CertSN=", "&Keyspec=" ];
		var s = [ CertStore, r, a, n ];
		var S = JointSendData(o, s);
		sendAjaxData("NSGetCertsListInfo", "POST", S, i);
		return
	};
	e.IWSASetCertsListInfoCache = function(e, t) {
		var r = [ "IsStore=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSetCertsListInfoCache", "POST", n, t)
	};
	e.IWSAGetCertInfoForIndex = function(e, t) {
		if (e < 0) {
		}
		var r = [ "CertIndex=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSGetCertInfoForIndex", "POST", n, t)
	};
	e.IWSAGetCertPublicKeyInfoForIndex = function(e, t) {
		if (e < 0) {
		}
		var r = [ "CertIndex=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSGetCertPublicKeyInfoForIndex", "POST", n, t)
	};
	e.IWSASkfGetCertList = function(e, t) {
		var r = [ "DllFilePath=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		sendAjaxData("NSSkfGetCertsListInfo", "POST", n, t);
		return
	};
	e.IWSASkfSignData = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		var i = [ "&PlainText=", "&CertIndex=", "&UsbKeyPin=",
				"&DigestArithmetic=" ];
		var o = [ e, t, r, a ];
		var s = JointSendData(i, o);
		sendAjaxData("NSSkfAttachedSign", "POST", s, n);
		return
	};
	e.IWSASkfAttachedSignWithDN = function(e, t, r, a, n, i, o) {
		a = GetDigestArithmeticByName(a);
		var s = "";
		if (i == "1") {
			s = "2"
		}
		if (i == "0") {
			s = "0"
		}
		var S = [ "PlainText=", "&defultDN=", "&UsbKeyPin=",
				"&DigestArithmetic=", "&DllFilename=", "&Keyspec=" ];
		var c = [ e, t, r, a, n, s ];
		var d = JointSendData(S, c);
		sendAjaxData("NSSkfAttachedSign", "POST", d, o);
		return
	};
	e.IWSASkfVerifySignData = function(e, t, r, a, n, i) {
		var o = [ "signedMsg=", "&PlainText=", "&CertIndex=", "&UsbKeyPin=",
				"&DllFilename=" ];
		var s = [ e, t, r, a, n ];
		var S = JointSendData(o, s);
		sendAjaxData("NSSkfAttachedVerify", "POST", S, i);
		return
	};
	e.IWSASkfDetachedSign = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		var i = [ "&PlainText=", "&CertIndex=", "&UsbKeyPin=",
				"&DigestArithmetic=" ];
		var o = [ e, t, r, a ];
		var s = JointSendData(i, o);
		sendAjaxData("NSSkfDetachedSign", "POST", s, n);
		return
	};
	e.IWSASkfDetachedSignDefaultDN = function(e, t, r, a, n, i, o) {
		a = GetDigestArithmeticByName(a);
		var s = "";
		if (i == "1") {
			s = "2"
		}
		if (i == "0") {
			s = "0"
		}
		var S = [ "PlainText=", "&DefaultDN=", "&UsbKeyPin=",
				"&DigestArithmetic=", "&DllFilePath=", "&Keyspec=" ];
		var c = [ e, t, r, a, n, s ];
		var d = JointSendData(S, c);
		sendAjaxData("NSSkfDetachedSign", "POST", d, o);
		return
	};
	e.IWSASkfDetachedVerify = function(e, t, r) {
		var a = [ "SignedMsg=", "&PlainText=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		sendAjaxData("NSSkfDetachedVerify", "POST", i, r);
		return
	};
	e.IWSASkfRawSign = function(e, t, r, a, n) {
		a = GetDigestArithmeticByName(a);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var i = [ "PlainText=", "&CertIndex=", "&UsbKeyPin=",
				"&DigestArithmetic=" ];
		var o = [ e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSSkfRawSign", "POST", s, n)
	};
	e.IWSASkfRawSignDefaultDN = function(e, t, r, a, n, i, o) {
		n = GetDigestArithmeticByName(n);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var s = "";
		if (r == "1") {
			s = "2"
		}
		if (r == "0") {
			s = "0"
		}
		var S = [ "PlainText=", "&DefaultDN=", "&Keyspec=", "&UsbKeyPin=",
				"&DigestArithmetic=", "&DllFilePath=" ];
		var c = [ e, t, s, a, n, i ];
		var d = JointSendData(S, c);
		return sendAjaxData("NSSkfRawSign", "POST", d, o)
	};
	e.IWSASkfRawVerify = function(e, t, r, a, n, i) {
		a = GetDigestArithmeticByName(a);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		t = BASE64URLEN(t);
		var o = [ "PlainText=", "&SignedMsg=", "&CertIndex=",
				"&DigestArithmetic=", "&DllFilePath=" ];
		var s = [ e, t, r, a, n ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSSkfRawVerify", "POST", S, i)
	};
	e.IWSASkfRawVerifyDefaultDN = function(e, t, r, a, n, i, o) {
		n = GetDigestArithmeticByName(n);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		t = BASE64URLEN(t);
		var s = "";
		if (a == "1") {
			s = "2"
		}
		if (a == "0") {
			s = "0"
		}
		var S = [ "PlainText=", "&SignedMsg=", "&DefaultDN=",
				"&DigestArithmetic=", "&DllFilePath=", "&Keyspec=" ];
		var c = [ e, t, r, n, i, s ];
		var d = JointSendData(S, c);
		return sendAjaxData("NSSkfRawVerify", "POST", d, o)
	};
	e.IWSASkfEncryptedEnvelop = function(e, t, r, a) {
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var n = [ "PlainText=", "&EncCert=", "&SymmetryArithmetic=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSSkfEncryptedEnvelop", "POST", o, a)
	};
	e.IWSASkfEncryptedSignEnvelop = function(e, t, r, a, n, i, o) {
		n = GetDigestArithmeticByName(n);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var s = [ "PlainText=", "&EncCert=", "&CertIndex=", "&UsbKeyPin=",
				"&DigestArithmetic=", "&SymmetryArithmetic=" ];
		var S = [ e, t, r, a, n, i ];
		var c = JointSendData(s, S);
		return sendAjaxData("NSSkfEncryptedSignEnvelop", "POST", c, o)
	};
	e.IWSASkfEncryptedSignEnvelopDefaultDN = function(e, t, r, a, n, i, o, s, S) {
		i = GetDigestArithmeticByName(i);
		if (e != "") {
			e = GetPlanTextForConvertMode(e)
		}
		var c = "";
		if (a == "1") {
			c = "2"
		}
		if (a == "0") {
			c = "0"
		}
		var d = [ "PlainText=", "&EncCert=", "&DefaultDN=", "&Keyspec=",
				"&UsbKeyPin=", "&DigestArithmetic=", "&SymmetryArithmetic=",
				"&DllFilePath=" ];
		var f = [ e, t, r, c, n, i, o, s ];
		var l = JointSendData(d, f);
		return sendAjaxData("NSSkfEncryptedSignEnvelop", "POST", l, S)
	};
	e.IWSASetCertEnrollShowErrMsg = function(e, t) {
		var r = [ "IsShow=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSetCertEnrollShowErrMsg", "POST", n, t)
	};
	e.IWSAGetEVersion = function(e) {
		return sendAjaxData("NSGetEVersion", "GET", "", e)
	};
	e.IWSAGetERsaCertStore = function(e) {
		return sendAjaxData("NSGetRsaCertStore", "GET", "", e)
	};
	e.IWSAGetESm2CFCACertStore = function(e) {
		return sendAjaxData("NSGetSm2CFCACertStore", "GET", "", e)
	};
	e.IWSAGetDistinctNameKeyValuePairNumber = function(e, t) {
		var r = [ "DN=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSGetDistinctNameKeyValuePairNumber", "POST", n, t)
	};
	e.IWSAGetDistinctNameKeyValuePair = function(e, t, r) {
		var a = [ "DN=", "&Index=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSGetDistinctNameKeyValuePair", "POST", i, r)
	};
	e.IWSAECompareCertTime = function(e, t, r) {
		var a = [ "CT1=", "&CT2=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSCompareCertTime", "POST", i, r)
	};
	e.IWSAGenSecureAuthCode = function(e, t, r, a) {
		var n = [ "RefNo=", "&AuthCode=", "&P10=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSGenSecureAuthCode", "POST", o, a)
	};
	e.IWSAGetX509CertInfo = function(e, t) {
		var r = [ "X509Cert=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSGetX509CertInfo", "POST", n, t)
	};
	e.IWSAGetX509FromP7Cert = function(e, t) {
		var r = [ "P7Cert=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSGetX509FromP7Cert", "POST", n, t)
	};
	e.IWSA_rsa_csp_listProvider = function(e) {
		return sendAjaxData("NSGetRsaCspListProvider", "GET", "", e)
	};
	e.IWSA_rsa_csp_getCountOfCert = function(e) {
		return sendAjaxData("NSGetRsaCspCountOfCert", "GET", "", e)
	};
	e.IWSA_rsa_csp_getCertInfo = function(e, t) {
		var r = [ "CertIndex=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSGetRsaCspCertInfo", "POST", n, t)
	};
	e.IWSA_rsa_csp_AdvgenContainerP10 = function(e, t, r, a) {
		var n = [ "CspName=", "&IKeySize=", "&DN=", "&bDoubleCert=" ];
		var i = [ m_CspName, e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSAdvRsaCspGenContainerP10", "POST", o, a)
	};
	e.IWSA_rsa_csp_AdvImportSignEncCert = function(e, t, r, a, n, i) {
		if (e == "X509") {
			var o = [ "CspName=", "&SignCert_Base64=", "&EncCert_Base64=",
					"&EncPrikey_Base64=", "&UKEK_Base64=" ];
			var s = [ m_CspName, t, r, a, n ];
			var S = JointSendData(o, s);
			return sendAjaxData("NSRsaCspImportSignEncX509Cert", "POST", S, i)
		}
		if (e == "P7") {
			var o = [ "CspName=", "&SignCert_Base64=", "&EncCert_Base64=",
					"&EncPrikey_Base64=", "&UKEK_Base64=" ];
			var s = [ m_CspName, t, r, a, n ];
			var S = JointSendData(o, s);
			return sendAjaxData("NSRsaCspImportSignEncP7Cert", "POST", S, i)
		}
	};
	e.IWSA_rsa_csp_setProvider = function(e) {
		m_CspName = e;
		return
	};
	e.IWSA_rsa_csp_deleteContainer = function(e, t) {
		var r = [ "CspName=", "&Container=" ];
		var a = [ m_CspName, e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSRsaCspDeleteContainer", "POST", n, t)
	};
	e.IWSA_rsa_csp_createContainer = function(e, t) {
		var r = [ "CspName=", "&Container=" ];
		var a = [ m_CspName, e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSRsaCspCreateContainer", "POST", n, t)
	};
	e.IWSA_rsa_csp_delContainer = function(e, t, r) {
		var a = [ "CspNamedel=", "&Container=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSRsaCspDelContainer", "POST", i, r)
	};
	e.IWSA_rsa_csp_genContainer = function(e) {
		var t = [ "CspName=" ];
		var r = [ m_CspName ];
		var a = JointSendData(t, r);
		return sendAjaxData("NSRsaCspGenContainer", "POST", a, e)
	};
	e.IWSA_rsa_csp_genContainerP10 = function(e, t, r, a, n, i, o, s, S) {
		var c = [ "CspName=", "&bSign=", "&KeySize=", "&DN=", "&DigestOID=",
				"&PubKeyAlgOID=", "&SignAlgOID=", "&bExport=", "&bProtect=" ];
		var d = [ m_CspName, e, t, r, a, n, i, o, s ];
		var f = JointSendData(c, d);
		return sendAjaxData("NSRsaCspGenContainerP10", "POST", f, S)
	};
	e.IWSA_rsa_csp_genP10 = function(e, t, r, a, n, i, o, s, S, c) {
		var d = [ "CspName=", "&Container=", "&bSign=", "&KeySize=", "&DN=",
				"&DigestOID=", "&PubKeyAlgOID=", "&SignAlgOID=", "&bExport=",
				"&bProtect=" ];
		var f = [ m_CspName, e, t, r, a, n, i, o, s, S ];
		var l = JointSendData(d, f);
		return sendAjaxData("NSRsaCspGenP10", "POST", l, c)
	};
	e.IWSA_rsa_csp_genEncKeyPair = function(e) {
		var t = [ "CspName=" ];
		var r = [ m_CspName ];
		var a = JointSendData(t, r);
		return sendAjaxData("NSRsaCspGenEncKeyPair", "POST", a, e)
	};
	e.IWSA_rsa_csp_delEncKeyPair = function(e) {
		var t = [ "CspName=" ];
		var r = [ m_CspName ];
		var a = JointSendData(t, r);
		return sendAjaxData("NSRsaCspDelEncKeyPair", "POST", a, e)
	};
	e.IWSA_rsa_csp_importX509CertToStore = function(e, t, r) {
		var a = [ "CspName=", "&X509Cert=", "&bRoot=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSRsaCspImportX509CertToStore", "POST", i, r)
	};
	e.IWSA_rsa_csp_delX509CertInStore = function(e, t, r) {
		var a = [ "CspName=", "&Issuer=", "&Serial=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSRsaCspDelX509CertInStore", "POST", i, r)
	};
	e.IWSA_rsa_csp_importSignX509Cert = function(e, t, r) {
		var a = [ "CspName=", "&Container=", "&X509Cert=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSRsaCspImportSignX509Cert", "POST", i, r)
	};
	e.IWSA_rsa_csp_importEncX509Cert = function(e, t, r, a, n, i, o) {
		var s = [ "CspName=", "&Container=", "&X509Cert=", "&EncPrikey=",
				"&UKEK=", "&bExport=", "&bProtect=" ];
		var S = [ m_CspName, e, t, r, a, n, i ];
		var c = JointSendData(s, S);
		return sendAjaxData("NSRsaCspImportEncX509Cert", "POST", c, o)
	};
	e.IWSA_rsa_csp_importSignP7Cert = function(e, t, r) {
		var a = [ "CspName=", "&Container=", "&P7Cert=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSRsaCspImportSignP7Cert", "POST", i, r)
	};
	e.IWSA_rsa_csp_importEncP7Cert = function(e, t, r, a, n, i, o) {
		var s = [ "CspName=", "&Container=", "&P7Cert=", "&EncPrikey=",
				"&UKEK=", "&bExport=", "&bProtect=" ];
		var S = [ m_CspName, e, t, r, a, n, i ];
		var c = JointSendData(s, S);
		return sendAjaxData("NSRsaCspImportEncP7Cert", "POST", c, o)
	};
	e.IWSA_rsa_csp_exportSignX509Cert = function(e, t) {
		var r = [ "CspName=", "&Container=" ];
		var a = [ m_CspName, e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSRsaCspExportSignX509Cert", "POST", n, t)
	};
	e.IWSA_rsa_csp_exportEncX509Cert = function(e, t) {
		var r = [ "CspName=", "&Container=" ];
		var a = [ m_CspName, e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSRsaCspExportEncX509Cert", "POST", n, t)
	};
	e.IWSA_rsa_csp_exportPfxCert = function(e, t, r, a, n) {
		var i = [ "CspName=", "&Issuer=", "&Serial=", "&Password=", "&bSaveAs=" ];
		var o = [ m_CspName, e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSRsaCspExportPfxCert", "POST", s, n)
	};
	e.IWSA_rsa_csp_exportContainerPfxCert = function(e, t, r, a, n) {
		var i = [ "CspName=", "&Container=", "&bSignCert=", "&Password=",
				"&bSaveAs=" ];
		var o = [ m_CspName, e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSRsaCspExportContainerPfxCert", "POST", s, n)
	};
	e.IWSA_rsa_csp_signData = function(e, t, r, a) {
		var n = [ "CspName=", "&Container=", "&PlantText=", "&DigestOID=" ];
		var i = [ m_CspName, e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSRsaCspSignData", "POST", o, a)
	};
	e.IWSA_rsa_csp_verifySignData = function(e, t, r, a, n) {
		var i = [ "CspName=", "&Container=", "&PlantText=", "&DigestOID=",
				"&SignedData=" ];
		var o = [ m_CspName, e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSRsaCspVerifySignData", "POST", s, n)
	};
	e.IWSA_rsa_csp_encryptData = function(e, t, r) {
		var a = [ "CspName=", "&Container=", "&PlantText=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSRsaCspEncryptData", "POST", i, r)
	};
	e.IWSA_rsa_csp_decryptData = function(e, t, r) {
		var a = [ "CspName=", "&Container=", "&EncryptText=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSRsaCspDecryptData", "POST", i, r)
	};
	e.IWSA_sm2_cfca_listProvider = function(e) {
		return sendAjaxData("NSSm2CfcaGetProviderList", "GET", "", e)
	};
	e.IWSA_sm2_cfca_getCountOfCert = function(e) {
		return sendAjaxData("NSSm2CfcaGetCountOfCert", "GET", "", e)
	};
	e.IWSA_sm2_cfca_getCertInfo = function(e, t) {
		var r = [ "CertIndex=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2CfcaGetCertInfo", "POST", n, t)
	};
	e.IWSA_sm2_cfca_setProvider = function(e) {
		m_CspName = e;
		return
	};
	e.IWSA_sm2_cfca_deleteContainer = function(e, t) {
		var r = [ "CspName=", "&Container=" ];
		var a = [ m_CspName, e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2CfcaDeleteContainer", "POST", n, t)
	};
	e.IWSA_sm2_cfca_createContainer = function(e, t) {
		var r = [ "CspName=", "&Container=" ];
		var a = [ m_CspName, e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2CfcaCreateContainer", "POST", n, t)
	};
	e.IWSA_sm2_cfca_delContainer = function(e, t, r) {
		var a = [ "CspNamedel=", "&Container=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2CfcaDelContainer", "POST", i, r)
	};
	e.IWSA_sm2_cfca_genContainer = function(e) {
		var t = [ "CspName=" ];
		var r = [ m_CspName ];
		var a = JointSendData(t, r);
		return sendAjaxData("NSSm2CfcaGenContainer", "POST", a, e)
	};
	e.IWSA_sm2_cfca_genContainerP10Encpubkey = function(e, t) {
		var r = [ "CspName=", "&DN=" ];
		var a = [ m_CspName, e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2CfcaGenContainerP10Encpubkey", "POST", n, t)
	};
	e.IWSA_sm2_cfca_genP10Encpubkey = function(e, t, r) {
		var a = [ "CspName=", "&Container=", "&DN=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2CfcaGenP10Encpubkey", "POST", i, r)
	};
	e.IWSA_sm2_cfca_importSignX509Cert = function(e, t, r) {
		var a = [ "CspName=", "&Container=", "&X509Cert=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2CfcaImportSignX509Cert", "POST", i, r)
	};
	e.IWSA_sm2_cfca_importEncX509Cert = function(e, t, r, a) {
		var n = [ "CspName=", "&Container=", "&X509Cert=", "&EncPrikey=" ];
		var i = [ m_CspName, e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSSm2CfcaImportEncX509Cert", "POST", o, a)
	};
	e.IWSA_sm2_cfca_AdvImportSignEncX509Cert = function(e, t, r, a) {
		var n = [ "CspName=", "&SignCert=", "&EncCert=", "&EncPrikey=" ];
		var i = [ m_CspName, e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSSm2CfcaImportSignEncX509Cert", "POST", o, a)
	};
	e.IWSA_sm2_cfca_signData = function(e, t, r) {
		var a = [ "CspName=", "&Container=", "&PlantText=" ];
		var n = [ m_CspName, e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2CfcaSignData", "POST", i, r)
	};
	e.IWSA_sm2_skf_getCountOfCert = function(e) {
		return sendAjaxData("NSSm2SkfGetCountOfCert", "GET", "", e)
	};
	e.IWSA_sm2_skf_getCertInfo = function(e, t) {
		var r = [ "CertIndex=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2SkfGetCertInfo", "POST", n, t)
	};
	e.IWSA_sm2_skf_getProviderList = function(e) {
		return sendAjaxData("NSSm2SkfGetProviderList", "GET", "", e)
	};
	e.IWSA_sm2_skf_getDeviceList = function(e, t) {
		var r = [ "Provider=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2SkfGetDeviceList", "POST", n, t)
	};
	e.IWSA_sm2_skf_getApplicationList = function(e, t, r) {
		var a = [ "Provider=", "&Device=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2SkfGetApplicationList", "POST", i, r)
	};
	e.IWSA_sm2_skf_useDevice = function(e, t, r) {
		SkfDeviceType = 1;
		SkfUseDeviceInfoAry[0] = e;
		SkfUseDeviceInfoAry[1] = t;
		SkfUseDeviceInfoAry[2] = "";
		if (r == "true" || r == "false") {
			SkfUseDeviceInfoAry[2] = r
		}
		return
	};
	e.IWSA_sm2_skf_setDevice = function(e, t, r) {
		SkfDeviceType = 2;
		SkfSetDeviceInfoAry[0] = e;
		SkfSetDeviceInfoAry[1] = t;
		SkfSetDeviceInfoAry[2] = r;
		return
	};
	e.IWSA_sm2_skf_getContainerList = function(e) {
		var t = GetSkfTitleParameterAndAllreadyDevice();
		var r = GetSkfDataParameterAndAllreadyDevice();
		var a = JointSendData(t, r);
		return sendAjaxData("NSSm2SkfGetContainerList", "POST", a, e)
	};
	e.IWSA_sm2_skf_getDeviceInfo = function(e, t, r) {
		var a = [ "Provider=", "&Device=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2SkfGetDeviceInfo", "POST", i, r)
	};
	e.IWSA_sm2_skf_verifyPin = function(e, t) {
		var r = GetSkfTitleParameterAndAllreadyDevice("PIN");
		var a = GetSkfDataParameterAndAllreadyDevice(e);
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2SkfVerifyPin", "POST", n, t)
	};
	e.IWSA_sm2_skf_verifyPinOfCtrl = function(e) {
		var t = GetSkfTitleParameterAndAllreadyDevice();
		var r = GetSkfDataParameterAndAllreadyDevice();
		var a = JointSendData(t, r);
		return sendAjaxData("NSSm2SkfVerifyPinOfCtrl", "POST", a, e)
	};
	e.IWSA_sm2_skf_getPinInfo = function(e) {
		var t = GetSkfTitleParameterAndAllreadyDevice();
		var r = GetSkfDataParameterAndAllreadyDevice();
		var a = JointSendData(t, r);
		return sendAjaxData("NSSm2SkfGetPinInfo", "POST", a, e)
	};
	e.IWSA_sm2_skf_createContainer = function(e, t, r) {
		var a = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container");
		var n = GetSkfDataParameterAndAllreadyDevice(e, t);
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2SkfCreateContainer", "POST", i, r)
	};
	e.IWSA_sm2_skf_delContainer = function(e, t, r, a, n, i) {
		var o = GetSkfTitleParameterAndAllreadyDevice("Providerdel",
				"Devicedel", "Applicationdel", "Containerdel", "bVerifyPin");
		var s = GetSkfDataParameterAndAllreadyDevice(e, t, r, a, n);
		var S = JointSendData(o, s);
		return sendAjaxData("NSSm2SkfDelContainer", "POST", S, i)
	};
	e.IWSA_sm2_skf_deleteContainer = function(e, t, r) {
		var a = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container");
		var n = GetSkfDataParameterAndAllreadyDevice(e, t);
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2SkfDeleteContainer", "POST", i, r)
	};
	e.IWSA_sm2_skf_writeData = function(e, t, r) {
		var a = GetSkfTitleParameterAndAllreadyDevice("PIN", "Data");
		var n = GetSkfDataParameterAndAllreadyDevice(e, t);
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2SkfWriteData", "POST", i, r)
	};
	e.IWSA_sm2_skf_readData = function(e, t) {
		var r = GetSkfTitleParameterAndAllreadyDevice("PIN");
		var a = GetSkfDataParameterAndAllreadyDevice(e);
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2SkfReadData", "POST", n, t)
	};
	e.IWSA_sm2_skf_genContainer = function(e, t) {
		var r = GetSkfTitleParameterAndAllreadyDevice("PIN");
		var a = GetSkfDataParameterAndAllreadyDevice(e);
		var n = JointSendData(r, a);
		return sendAjaxData("NSSm2SkfGenContainer", "POST", n, t)
	};
	e.IWSA_sm2_skf_genContainerP10 = function(e, t, r, a, n) {
		var i = GetSkfTitleParameterAndAllreadyDevice("PIN", "DN", "ID", "bID");
		var o = GetSkfDataParameterAndAllreadyDevice(e, t, r, a);
		var s = JointSendData(i, o);
		return sendAjaxData("NSSm2SkfGenContainerP10", "POST", s, n)
	};
	e.IWSA_sm2_skf_genP10 = function(e, t, r, a, n, i) {
		var o = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container", "DN",
				"ID", "bID");
		var s = GetSkfDataParameterAndAllreadyDevice(e, t, r, a, n);
		var S = JointSendData(o, s);
		return sendAjaxData("NSSm2SkfGenP10", "POST", S, i)
	};
	e.IWSA_sm2_skf_getEnveloped = function(e, t, r, a, n) {
		var i = [ "X509Cert=", "&SGDSMECB=", "&EncPrikey=", "&UKEK=" ];
		var o = [ e, t, r, a ];
		var s = JointSendData(i, o);
		return sendAjaxData("NSSm2SkfGetEnveloped", "POST", s, n)
	};
	e.IWSA_sm2_skf_importSignX509Cert = function(e, t, r, a) {
		var n = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container",
				"X509Cert");
		var i = GetSkfDataParameterAndAllreadyDevice(e, t, r);
		var o = JointSendData(n, i);
		return sendAjaxData("NSSm2SkfImportSignX509Cert", "POST", o, a)
	};
	e.IWSA_sm2_skf_importEncX509Cert = function(e, t, r, a, n) {
		var i = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container",
				"X509Cert", "EncPrikey");
		var o = GetSkfDataParameterAndAllreadyDevice(e, t, r, a);
		var s = JointSendData(i, o);
		return sendAjaxData("NSSm2SkfImportEncX509Cert", "POST", s, n)
	};
	e.IWSA_sm2_skf_AdvImportSignEncX509Cert = function(e, t, r, a, n) {
		var i = GetSkfTitleParameterAndAllreadyDevice("PIN", "SignCert",
				"EncCert", "EncPrikey");
		var o = GetSkfDataParameterAndAllreadyDevice(e, t, r, a);
		var s = JointSendData(i, o);
		return sendAjaxData("NSSm2SkfImportSignEncX509Cert", "POST", s, n)
	};
	e.IWSA_sm2_skf_exportSignX509Cert = function(e, t, r) {
		var a = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container");
		var n = GetSkfDataParameterAndAllreadyDevice(e, t);
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2SkfExportSignX509Cert", "POST", i, r)
	};
	e.IWSA_sm2_skf_exportEncX509Cert = function(e, t, r) {
		var a = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container");
		var n = GetSkfDataParameterAndAllreadyDevice(e, t);
		var i = JointSendData(a, n);
		return sendAjaxData("NSSm2SkfExportEncX509Cert", "POST", i, r)
	};
	e.IWSA_sm2_skf_signData = function(e, t, r, a, n, i) {
		var o = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container",
				"PlantText", "ID", "bID");
		var s = GetSkfDataParameterAndAllreadyDevice(e, t, r, a, n);
		var S = JointSendData(o, s);
		return sendAjaxData("NSSm2SkfSignData", "POST", S, i)
	};
	e.IWSA_sm2_skf_verifySignData = function(e, t, r, a, n, i, o) {
		var s = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container",
				"PlantText", "SignData", "ID", "bID");
		var S = GetSkfDataParameterAndAllreadyDevice(e, t, r, a, n, i);
		var c = JointSendData(s, S);
		return sendAjaxData("NSSm2SkfVerifySignData", "POST", c, o)
	};
	e.IWSA_sm2_skf_encryptData = function(e, t, r, a) {
		var n = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container",
				"PlantText");
		var i = GetSkfDataParameterAndAllreadyDevice(e, t, r);
		var o = JointSendData(n, i);
		return sendAjaxData("NSSm2SkfEncryptData", "POST", o, a)
	};
	e.IWSA_sm2_skf_decryptData = function(e, t, r, a) {
		var n = GetSkfTitleParameterAndAllreadyDevice("PIN", "Container",
				"EncryptText");
		var i = GetSkfDataParameterAndAllreadyDevice(e, t, r);
		var o = JointSendData(n, i);
		return sendAjaxData("NSSm2SkfDecryptData", "POST", o, a)
	};
	e.IWSA_exec = function(e, t) {
		var r = [ "bstrComm=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSLoginExec", "POST", n, t)
	};
	e.IWSA_execParams = function(e, t, r) {
		var a = [ "bstrComm=", "&bstrParam=" ];
		var n = [ e, t ];
		var i = JointSendData(a, n);
		return sendAjaxData("NSLoginExecParams", "POST", i, r)
	};
	e.IWSA_startAll = function(e, t, r, a) {
		var n = [ "bstrNames=", "&bstrFormats=", "&bstrInputs=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSLoginStartAll", "POST", o, a)
	};
	e.IWSA_stopAll = function(e) {
		return sendAjaxData("NSLoginStopAll", "GET", "", e)
	};
	e.IWSA_startMessageLoginInBack = function(e, t, r, a) {
		var n = [ "bstrNames=", "&bstrFormats=", "&bstrInputs=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSLoginStartMessageLoginInBack", "POST", o, a)
	};
	e.IWSA_stopMessageLoginInBack = function(e, t) {
		var r = [ "bstrName=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSLoginStopMessageLoginInBack", "POST", n, t)
	};
	e.IWSA_startMessageLogin = function(e, t, r, a) {
		var n = [ "bstrName=", "&bstrFormat=", "&bstrInput=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSLoginStartMessageLogin", "POST", o, a)
	};
	e.IWSA_startTabLoginLoginInBack = function(e, t, r, a, n, i) {
		var o = [ "bstrTitle=", "&bstrName=", "&bstrPassword=",
				"&bstrTabFormat=", "&bstrTabTab=" ];
		var s = [ e, t, r, a, n ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSLoginStartTabLoginLoginInBack", "POST", S, i)
	};
	e.IWSA_stopTabLoginInBack = function(e, t) {
		var r = [ "bstrName=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSLoginStopTabLoginInBack", "POST", n, t)
	};
	e.IWSA_startTabLogin = function(e, t, r, a, n, i) {
		var o = [ "bstrTitle=", "&bstrName=", "&bstrPassword=",
				"&bstrTabFormat=", "&bstrTabTab=" ];
		var s = [ e, t, r, a, n ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSLoginStartTabLoginLogin", "POST", S, i)
	};
	e.IWSA_encrypt = function(e, t) {
		var r = [ "bstrInput=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSLoginEncrypt", "POST", n, t)
	};
	e.IWSA_decrypt = function(e, t) {
		var r = [ "bstrInput=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSLoginDecrypt", "POST", n, t)
	};
	e.IWSA_isLoginFailed = function(e, t) {
		var r = [ "bstrName=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSLoginIsLoginFailed", "POST", n, t)
	};
	e.IWSA_startMessageMultiWindowLogin = function(e, t, r, a) {
		var n = [ "bstrName=", "&bstrFormat=", "&bstrInput=" ];
		var i = [ e, t, r ];
		var o = JointSendData(n, i);
		return sendAjaxData("NSLoginStartMessageMultiWindowLogin", "POST", o, a)
	};
	e.IWSA_autoLoginHTTP = function(e, t, r, a, n, i) {
		var o = [ "bstrPath=", "&bstrUsername=", "&bstrPwd=", "&bstrDefault=",
				"&bstrLoginType=" ];
		var s = [ e, t, r, a, n ];
		var S = JointSendData(o, s);
		return sendAjaxData("NSLoginautoLoginHTTP", "POST", S, i)
	};
	e.IWSA_SetFailFormat = function(e, t, r, a, n, i, o) {
		var s = [ "bstrTitle=", "&bstrErrortitle=", "&bstrFailtype=",
				"&bstrFailformat=", "&bstrFailvalue=", "&bstrFailtime=" ];
		var S = [ e, t, r, a, n, i ];
		var c = JointSendData(s, S);
		return sendAjaxData("NSLoginSetFailFormat", "POST", c, o)
	};
	e.IWSA_StartLogin = function(e, t) {
		var r = [ "ssoconfigxml=" ];
		var a = [ e ];
		var n = JointSendData(r, a);
		return sendAjaxData("NSLoginStartLogin", "POST", n, t)
	};
	e.IWSA_StartMessageLoginHXPOC = function(e, t, r, a, n, i, o, s) {
		var S = [ "bstrWindowName=", "&TabIndex=", "&NameIndex=",
				"&bstrNameValue=", "&PsdIndex=", "&bstrPsdValue=",
				"&EnterIndex=" ];
		var c = [ e, t, r, a, n, i, o ];
		var d = JointSendData(S, c);
		return sendAjaxData("NSLoginStartMessageLoginHXPOC", "POST", d, s)
	};
	return e
}