/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import com.icss.hit.bean.MeetingRoomBean;
import com.icss.hit.hibernate.vo.Room;
import com.icss.hit.struts.form.AddMeetingRoomForm;

/** 
 * MyEclipse Struts
 * Creation date: 08-08-2009
 * 
 * XDoclet definition:
 * @struts.action validate="true"
 * @struts.action-forward name="addMeetingRoom.success" path="/contents.jsp"
 * @struts.action-forward name="addMeetingRoom.error" path="/meeting/addMeetingRoom.jsp"
 */
public class AddMeetingRoomAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @author ���
	 * ��ӻ�������Ϣ
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		AddMeetingRoomForm rmForm = (AddMeetingRoomForm) form;// TODO Auto-generated method stub

		
		MeetingRoomBean getter = null;
		Room newMeetingRoom = null;
		getter = new MeetingRoomBean();
		
		
		
		newMeetingRoom = new Room();//ʵ�����µĻ�����
		newMeetingRoom.setRName(rmForm.getRoom_name());//��ʼ������������
		newMeetingRoom.setRContain(Long.parseLong(rmForm.getRoom_capacity()));//��ʼ����������������
		newMeetingRoom.setRInfo(rmForm.getRoom_describe());//��ʼ�������ұ�ע��Ϣ
		
		

		

			if(getter.addRoom(newMeetingRoom) == 0){
				//�ɹ����
				return mapping.findForward("addMeetingRoom.success");
			}
			else if(getter.addRoom(newMeetingRoom) == 1){
				//����������
				ActionErrors errors = new ActionErrors();
				errors.add("roomNameExist", new ActionMessage("AddMeetingRoom.roomNameExist"));
				saveErrors(request, errors);//���������Ĵ�����Ϣ
				return mapping.findForward("addMeetingRoom.error");
			}
			else {
				//δ֪�쳣
				return mapping.findForward("addMeetingRoom.error");
			}
	}
}