/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import java.util.List;

import com.icss.hit.bean.CheckPower;
import com.icss.hit.bean.DepartmentBean;
import com.icss.hit.bean.SysPositionBean;
import com.icss.hit.bean.SysRoleBean;
import com.icss.hit.bean.interfaces.Department;
import com.icss.hit.bean.interfaces.SysPositionDao;
import com.icss.hit.bean.interfaces.SysRoleDao;
import com.icss.hit.hibernate.vo.SysDept;
import com.icss.hit.hibernate.vo.SysPosition;
import com.icss.hit.hibernate.vo.SysRole;

/** 
 * ���һ��Ա��
 * Creation date: 08-12-2009
 * @author ����
 * XDoclet definition:
 * @struts.action validate="true"
 * @struts.action-forward name="addWorker.succeed" path="/role/addWorker.jsp"
 */
public class AddWorkerAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// ȡ���û���Ϣ
		long userId = 1;
		if( request.getSession().getAttribute("UserId") != null ){
			userId = Long.parseLong(request.getSession().getAttribute("UserId").toString());
		}else{
			return mapping.findForward("NullLogin");
		}
		// �ж��û�Ȩ��
		CheckPower check = new CheckPower(userId);
		check.getPower();
		if( !check.check(CheckPower.ADMIN_STUFF_ADD)){
			return mapping.findForward("NullLogin");
		}
		
		
		Department deptm = new DepartmentBean();
		// �õ����еĲ�����Ϣ
		List<SysDept> deptList = deptm.getAllDept();
		request.setAttribute("deptList",deptList);
		
		SysPositionDao pos = new SysPositionBean();
		// �õ�����ְλ�б�
		List<SysPosition> posList = pos.getAllPosition();
		request.setAttribute("posList",posList);
		
		SysRoleDao role = new SysRoleBean();
		// �õ����еĽ�ɫ��Ϣ
		List<SysRole> roleNameList = role.getAllSysRoleName();
		request.setAttribute("roleNameList",roleNameList);
		
		return mapping.findForward("addWorker.succeed");
	}
}