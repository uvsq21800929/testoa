/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package com.icss.hit.struts.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.icss.hit.bean.WorkPlanBean;
import com.icss.hit.bean.interfaces.WorkPlanDao;
import com.icss.hit.hibernate.vo.Schedule;
import com.icss.hit.struts.form.ModifyWorkConfigForm;

/** 
 * �޸��ճ̰���	
 * Creation date: 08-05-2009
 * @author xw-pc
 * XDoclet definition:
 * @struts.action path="/modifyWorkConfig" name="modifyWorkConfigForm" input="/work/modifyWorkConfig.jsp" scope="request" validate="true"
 */
public class ModifyWorkConfigAction extends Action {
	/*
	 * Generated Methods
	 */

	/** 
	 * Method execute
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ModifyWorkConfigForm modifyWorkConfigForm = (ModifyWorkConfigForm) form;
		
		long userId=1;
		
		if( request.getSession().getAttribute("UserId") != null ){
			userId = Long.parseLong(request.getSession().getAttribute("UserId").toString());
		}else{
			return mapping.findForward("NullLogin");
		}
		WorkPlanDao wp = new WorkPlanBean();
		Schedule sch = wp.getScheduleInfo(Long.parseLong(modifyWorkConfigForm.getSch_id()));
			
			//�ж�ʱ���Ƿ�Ϊ��
			boolean flag=true;
			if(modifyWorkConfigForm.getSch_begintime()!=null&&modifyWorkConfigForm.getSch_endtime()!=null&&modifyWorkConfigForm.getSch_begintime().length()>0&&modifyWorkConfigForm.getSch_endtime().length()>0&&modifyWorkConfigForm.getSch_content()!=null&&modifyWorkConfigForm.getSch_content().length()>0&&modifyWorkConfigForm.getSch_title()!=null&&modifyWorkConfigForm.getSch_title().length()>0)
			{
				SimpleDateFormat bartDateFormat =new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
				try {
					Date beginTime = bartDateFormat.parse(modifyWorkConfigForm
							.getSch_begintime());
					Date endTime = bartDateFormat.parse(modifyWorkConfigForm
							.getSch_endtime());
					//�ж�ʱ���Ƿ��ͻ
					
						List<Schedule> slist = wp.getSchedule(userId);
						try {
							for (int i = 0; i < slist.size(); i++) 
							{
								
								Schedule schedule = slist.get(i);
								// ���������������
								if( schedule.getSchId() == Long.parseLong(modifyWorkConfigForm.getSch_id())){
									continue;
								}
								//����Ŀ�ʼʱ��Ҫ�����Ѿ����ŵ��ճ̽�����ʱ��
								long m =beginTime.getTime()-schedule.getSchEndtime().getTime();
								//����Ľ���ʱ��Ҫ�����Ѿ����ŵ��ճ̿�ʼ��ʱ��
								long n = endTime.getTime()-schedule.getSchBegintime().getTime();
								if ( m >= 0 || n <= 0 )
								{
									continue;
								} 
								else
								{
									flag=false;
									break;
								}
							}
						if (flag)
						{
							sch.setSchBegintime(beginTime);
							sch.setSchEndtime(endTime);
							sch.setSchContent(modifyWorkConfigForm.getSch_content());
							sch.setSchTitle(modifyWorkConfigForm.getSch_title());
							if (wp.saveSchedule(sch))
							{
								return mapping.findForward("ModifyWorkConfig.succeed");
							}
							else 
							{
								return mapping.findForward("ModifyWorkConfig.faild");
					
							}
							
						}
						else
						{
							//���ʱ���ͻ����ʾ��Ϣ
							ActionErrors errors = new ActionErrors();
							errors.add("sch_begintime", new ActionMessage("WorkAdd.Timeconflict"));
			 				saveErrors(request, errors);
							return mapping.findForward("ModifyWorkConfig.faild");
						}
					} 
					catch (RuntimeException e)
					{
						e.printStackTrace();
					}

				}
				catch (ParseException e) {
					e.printStackTrace();
					
				}
				
			}
			
		return mapping.findForward("ModifyWorkConfig.faild");
		
		
	}
}