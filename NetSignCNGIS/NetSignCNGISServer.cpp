﻿#include <Winsock2.h>
#include "CertsListInfoIS.h"
#include "Constants.h"
#include <windows.h>
#include <cstdio>
#include <string>
#include <iostream>
#include <vector>
#pragma comment(lib,"ws2_32.lib")
using namespace std;

void tokenize(string& str, char delim, vector<string>& out);
void run()
{
    //https://blog.csdn.net/u010803748/article/details/72616445
    WSADATA wsaData;
    SOCKET sockServer;
    SOCKADDR_IN addrServer;
    SOCKET sockClient;
    SOCKADDR_IN addrClient;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    sockServer = socket(AF_INET, SOCK_STREAM, 0);
    addrServer.sin_addr.S_un.S_addr = htonl(INADDR_ANY);//INADDR_ANY表示任何IP
    addrServer.sin_family = AF_INET;
    addrServer.sin_port = htons(6000);//绑定端口6000
    bind(sockServer, (SOCKADDR*)&addrServer, sizeof(SOCKADDR));

    //Listen监听端
    listen(sockServer, 5);//5为等待连接数目
    printf("The server has started:\nListening...\n");
    int len = sizeof(SOCKADDR);
    char* sendBuf;//发送至客户端的字符串
    char recvBuf[100];//接受客户端返回的字符串

    //会阻塞进程，直到有客户端连接上来为止
    sockClient = accept(sockServer, (SOCKADDR*)&addrClient, &len);
    while(true){
        //接收并打印客户端数据
        recv(sockClient, recvBuf, 100, 0);
        string functionCalled = recvBuf;
        memset(recvBuf, 0, sizeof(recvBuf));
        cout << "Received data" << functionCalled << endl;
        if (functionCalled == "FIN" || functionCalled.size()==0) break;
        if (functionCalled.find("IWSAGetAllCertsListInfo") != string::npos) {
            string certStoreSM2, certStoreRSA;
            int keySpec;
            int beginIndex = functionCalled.find_first_of("?");
            string params = functionCalled.substr(beginIndex+1);
            //cout << params;
            vector<string> param_vector;
            tokenize(params, '&', param_vector);
            for (string p : param_vector) {
                //cout << p << endl;
                int pos = p.find_first_of("=");
                string key = p.substr(0, pos);//key=value pos=3
                string value = p.substr(pos+1,p.size()-pos-1);//9
                cout << key << " " << value<<endl;
                if (key == CERT_STORE_SM2) certStoreSM2 = value;
                if (key == CERT_STORE_RSA) certStoreRSA = value;
                if (key == KEY_SPEC) keySpec = atoi(value.c_str());
            }
            string certs = IWSAGetAllCertsListInfoIS(certStoreSM2.c_str(),certStoreRSA.c_str(),keySpec);
            cout << certs << endl;
            sendBuf = (char*) malloc(certs.size()+1);
            sendBuf[certs.size()] = '\0';
            strncpy(sendBuf, certs.c_str(),certs.size());
            send(sockClient, sendBuf, certs.size(), 0);
        }
    }
    //Send reply
    //send(sockClient, sendBuf, sizeof(sendBuf), 0);
    //printf("%s\n", recvBuf);

    //关闭socket
    closesocket(sockClient);
    WSACleanup();
}

void tokenize(string& str, char delim, vector<string>& out)
{
    //https://www.codegrepper.com/code-examples/delphi/c%2B%2B+split+string+by+delimiter
    size_t start;
    size_t end = 0;

    while ((start = str.find_first_not_of(delim, end)) != string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}