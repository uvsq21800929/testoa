
#include "gmpxx.h"

#include <string>
#include <sstream>
#include <windows.h>
using namespace std;
class CertificateIS
{
private:
	string certDN;
	string issuerDN;
	mpz_t certSN;
	SYSTEMTIME validBegin;
	SYSTEMTIME validEnd;
public:
	string CertStore;
	string KeyUsage;
	string CertType;
	CertificateIS(const string certDN, const string issuerDN, mpz_t certSN, const SYSTEMTIME& validBegin, const SYSTEMTIME& validEnd, const string CertStore, const string KeyUsage, const string CertType);
	void setCertDN(const string certDN);
	void setIssuerDN(const string issuerDN);
	void setCertSN(const mpz_t certSN);
	void setValidBegin(const SYSTEMTIME validBegin);
	void setValidEnd(const SYSTEMTIME validEnd);
	void toJson(string& result);
};

void CertificateIS::setCertDN(const string certDN) { this->certDN = certDN; }
CertificateIS::CertificateIS(const string certDN, const string issuerDN, mpz_t certSN, const SYSTEMTIME& validBegin, const SYSTEMTIME& validEnd, const string CertStore, const string KeyUsage, const string CertType) {
	this->certDN = certDN;
	this->issuerDN = issuerDN;
	mpz_init(this->certSN);
	mpz_set(this->certSN, certSN);
	this->validBegin = validBegin;
	this->validEnd = validEnd;
	this->CertStore = CertStore;
	this->KeyUsage = KeyUsage;
	this->CertType = CertType;
}

void CertificateIS::setIssuerDN(const string issuerDN) { this->certDN = certDN; }
void CertificateIS::setCertSN(const mpz_t certSN) { mpz_set(this->certSN, certSN); }
void CertificateIS::setValidBegin(const SYSTEMTIME validBegin) { this->validBegin = validBegin; }
void CertificateIS::setValidEnd(const SYSTEMTIME validEnd) { this->validEnd = validEnd; }



void CertificateIS::toJson(string& result) {
	char* tmp = mpz_get_str(NULL, 16, certSN);
	string serialnumber = tmp;
	ostringstream validBegin_strStream, validEnd_strStream;
	validBegin_strStream << validBegin.wDayOfWeek << " " << validBegin.wDay << " " << validBegin.wMonth << " " << validBegin.wYear << " " << validBegin.wHour << ":" << validBegin.wMinute << ":" << validBegin.wSecond;
	string validBegin_str = validBegin_strStream.str();
	//string validBegin_str = validBegin.wDayOfWeek + " " +validBegin.wDay + " " +validBegin.wMonth + " "+ validBegin.wYear + " " + validBegin.wHour + ":" + validBegin.wMinute + ":" +validBegin.wSecond;
	validEnd_strStream << validEnd.wDayOfWeek << " " << validEnd.wDay << " " << validEnd.wMonth << " " << validEnd.wYear << " " << validEnd.wHour << ":" << validEnd.wMinute << ":" << validEnd.wSecond;
	string validEnd_str = validEnd_strStream.str();

	result = "{'certDN':'" + certDN + "','issuerDN':'" + issuerDN + "','certSN':'" + serialnumber + "','validBegin':'" + validBegin_str + "','validEnd':'" + validEnd_str +
		"','CertStore':'" + CertStore + "','KeyUsage':'" + KeyUsage + "','CertType':'" + CertType + "'}";

}

