﻿#include "TestHeader.h"
#include <windows.h>
#include <wincrypt.h>
#include <cryptuiapi.h>
#include <tchar.h>
#include <iostream>
#include "TestGmp.h"
#include "NetSignCNGISServer.h"




#pragma comment (lib, "crypt32.lib")
#pragma comment (lib, "cryptui.lib")

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
using namespace std;

void maintest2();

int main() {

   cout << "Welcome to NetSignCNGIS.exe program." << endl;
   /* PHASE 1
   // testGmp();
   //testIWSAGetAllCertsListInfoISMY0();
   //maintest2();
   testIWSAGetAllCertsListInfoISNULL();
   testIWSAGetAllCertsListInfoISMY0();
   testIWSAGetAllCertsListInfoISMY2();
   testIWSAGetAllCertsListInfoISCA0();
   testIWSAGetAllCertsListInfoISCA2();
   testIWSAGetAllCertsListInfoISROOT0();
   testIWSAGetAllCertsListInfoISROOT2();
   // testIWSAGetAllCertsListInfoISADDRESSBOOK0();
   // testIWSAGetAllCertsListInfoISADDRESSBOOK2();
   //run();
   */
   //testIWSAGetAllCertsListInfoISADDRESSBOOK0();
   //testIWSAGetAllCertsListInfoISADDRESSBOOK2();
  // testIWSAGetAllCertsListInfoISTRUST0();
   //testSignAndEncryptIS
   //testIWSAGetAllCertsListInfoISMY0();
   //testIWSAGetAllCertsListInfoISADDRESSBOOK0();

   //testEncryptIS();
   //testSignIS();
   //testSignAndEncryptIS();
   //run();
   
   testBase64EncodeIS();
   testBase64DecodeIS();
   testSignIS();
   testSignByIndexIS();
   testDetachedSignIS();
   testDetachedSignDefaultDNIS();
   testDetachedVerifyIS();
   testAttachedSignIS();
   testAttachedSignDefaultDNIS();
   testAttachedVerifyIS();
   testRawSignIS();
   testRawSignFilterCNGIS();
   testRawSignDefaultDNCNGIS();
   testRawVerifyIS();
   return 0;

}