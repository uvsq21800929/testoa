#pragma once
void testIWSAGetAllCertsListInfoISNULL();
//void testIWSAGetAllCertsListInfoDetailISMY0();
void testIWSAGetAllCertsListInfoISMY0();
void testIWSAGetAllCertsListInfoISMY2();
void testIWSAGetAllCertsListInfoISCA0();
void testIWSAGetAllCertsListInfoISCA2();
void testIWSAGetAllCertsListInfoISROOT0();
void testIWSAGetAllCertsListInfoISROOT2();
void testIWSAGetAllCertsListInfoISTRUST0();
void testIWSAGetAllCertsListInfoISTRUST2();
void testIWSAGetAllCertsListInfoISADDRESSBOOK0();
void testIWSAGetAllCertsListInfoISADDRESSBOOK2();

void testSignAndEncryptIS();
void testEncryptIS();
void testSignIS();
void testSignByIndexIS();

void testDetachedSignIS();
void testDetachedSignDefaultDNIS();
void testDetachedVerifyIS();

void testAttachedSignIS();
void testAttachedSignDefaultDNIS();
void testAttachedVerifyIS();

void testRawSignIS();
void testRawSignFilterCNGIS();
void testRawSignDefaultDNCNGIS();
void testRawVerifyIS();

void testBase64EncodeIS();
void testBase64DecodeIS();