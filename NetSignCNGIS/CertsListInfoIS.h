#pragma once
#include <windows.h>
#include <string>
using namespace std;
string IWSAGetAllCertsListInfoIS(LPCSTR certStoreSM2, LPCSTR certStoreRSA, int keySpec);
//void IWSAGetAllCertsListInfoDetailIS(LPCSTR certStoreSM2, LPCSTR certStoreRSA, int keySpec);
//BYTE* SignAndEncryptIS(const BYTE* pbToBeSignedAndEncrypted, DWORD cbToBeSignedAndEncrypted, DWORD* pcbSignedAndEncryptedBlob, LPCSTR signerSubject, LPCSTR receiverSubject);
BYTE* SignAndEncryptIS(const BYTE* pbToBeSignedAndEncrypted, DWORD cbToBeSignedAndEncrypted, DWORD* pcbSignedAndEncryptedBlob, LPCSTR signerSubject, LPCSTR signerCertStore, LPCSTR recipientSubject, LPCSTR recipientCertStore);
BYTE* EncryptIS(const BYTE* pbToBeEncrypted, DWORD cbToBeEncrypted, DWORD* pcbEncryptedBlob, LPCSTR recipientSubject, LPCSTR certStore);
//BYTE* SignIS(const BYTE* pbToBeSigned, DWORD cbToBeSigned, DWORD* pcbSignedBlob, LPCSTR signerSubject, LPCSTR signerCertStore);
BOOL SignIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, BYTE*& pbSignedBlob, DWORD& cbSignedBlob, const string& signerSubject, const BOOL isKeySpec, const BOOL isDetached, const string& digestArithmetic,const string& signerCertStore);
BOOL SignIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, BYTE*& pbSignedBlob, DWORD& cbSignedBlob, const DWORD& signerCertIndex, const BOOL isKeySpec, const BOOL isDetached, const string& digestArithmetic, const string& signerCertStore);
void IWSADetachedSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& certIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore);
void IWSADetachedSignDefaultDNIS(DWORD*& errorCode, string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const string& defaultDN, const boolean isKeySpec, const string& digestArithmetic, const string& signerCertStore);
void IWSADetachedVerifyIS(DWORD*& errorCode, const string& portGrade, const string& signedData, const BYTE*& plainText, const DWORD& plainTextSize);
void IWSAAttachedSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& certIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore);
void IWSAAttachedSignDefaultDNIS(DWORD*& errorCode, string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const string& defaultDN, const boolean isKeySpec, const string& digestArithmetic, const string& signerCertStore);
void IWSAAttachedVerifyIS(DWORD*& errorCode, string& plainText, string& certDN, const string& portGrade, const string& signedData);
void IWSARawSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& signerCertIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore);
void IWSARawSignDefaultDNIS(DWORD*& errorCode, string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const string& defaultDN, const boolean isKeySpec, const string& digestArithmetic, const string& signerCertStore);
void IWSARawVerifyIS(DWORD*& errorCode, const string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& signerCertIndex, const string& digestArithmetic);

void IWSABase64EncodeIS(DWORD* errorCode, string& textData, const BYTE* plainText, const DWORD& plainTextSize);
void IWSABase64DecodeIS(DWORD* errorCode, BYTE*& textData, DWORD& cbBinary, const string& plainText);