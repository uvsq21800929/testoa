#pragma once
#include "gmpxx.h"
#include <timezoneapi.h>
#include <string>
using namespace std;
class CertificateIS
{
private:
	string certDN;
	string issuerDN;
	mpz_t certSN;
	SYSTEMTIME validBegin;
	SYSTEMTIME validEnd;
public:
	string CertStore;
	string KeyUsage;
	string CertType;
	CertificateIS(const string certDN, const string issuerDN,  mpz_t certSN, const SYSTEMTIME& validBegin, const SYSTEMTIME& validEnd, const string CertStore, const string KeyUsage, const string CertType);
	void setCertDN(const string certDN);
	void setIssuerDN(const string issuerDN);
	void setCertSN(const mpz_t certSN);
	void setValidBegin(const SYSTEMTIME validBegin);
	void setValidEnd(const SYSTEMTIME validEnd);
	void toJson(string& result);
};

