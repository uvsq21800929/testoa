﻿#include "gmpxx.h" // doit être importé au premier very peculiar
#include <windows.h>
#include <wincrypt.h>
#include <ncrypt.h>
#include <cryptuiapi.h>
#include <tchar.h>
#include <sstream>
#include <cstdio>
#include <timezoneapi.h>
#include "ErrorHandler.h"
#include "CertificateIS.h"
#include "Constants.h"
#include <vector>
#include <atlbase.h>

#include <iostream>

#pragma comment(lib, "ncrypt.lib")

//#define MY_TYPE   X509_ASN_ENCODING
#define MY_TYPE (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING) // cert encoding and message encoding
#define MAX_NAME  128
#define NT_SUCCESS(Status)          (((NTSTATUS)(Status)) >= 0)
#define STATUS_UNSUCCESSFUL         ((NTSTATUS)0xC0000001L)

using namespace std;
/*
IWSAGetAllCertsListInfo(CertStoreSM2, CertStoreRSA,Keyspec,SucceedFunction)

CertStoreSM2	字符串	国密证书存储区，没有填“”
CertStoreRSA	字符串	RSA证书存储区，全部、加密、签名
Keyspec	整数	按密钥用法过滤，为0返回全部证书,为2返回签名证书
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

Callback:
CertListData	JSON 格式的数据	证书列表信息

certDN	字符串	证书DN
issuerDN	字符串	证书颁发者DN
certSN	字符串	证书序列号，中间空格将忽略
validBegin	字符串	证书有效期开始日期
validEnd	字符串	证书有效期结束日期
CertStore	字符串	证书所在的存储区
KeyUsage	字符串	证书的秘钥用法
CertType	字符串	证书的证书类型(RSA，SM2)
*/

void IWSAGetAllCertsListInfoISTest(LPCSTR certStoreSM2, LPCSTR certStoreRSA, int keySpec) {


	HCERTSTORE       hCertStore;
	PCCERT_CONTEXT   pCertContext = NULL;
	void* pvData;
	DWORD            cbData;
	DWORD            dwPropId = 0;
	// Open a system certificate store.
	if (hCertStore = CertOpenSystemStore(NULL, certStoreRSA)) { fprintf(stderr, "The %s store has been opened. \n", certStoreRSA); }
	else { MyCertHandleError("The store was not opened."); }
	while (pCertContext = CertEnumCertificatesInStore(
		hCertStore,
		pCertContext))
	{
		while (dwPropId = CertEnumCertificateContextProperties(
			pCertContext, // The context whose properties are to be listed.
			dwPropId))    // Number of the last property found.  
						  // This must be zero to find the first 
						  // property identifier.
		{
			//-------------------------------------------------------------------
			// When the loop is executed, a property identifier has been found.
			// Print the property number.

			printf("Property # %d found->", dwPropId);

			//-------------------------------------------------------------------
			// Indicate the kind of property found.

			switch (dwPropId)
			{
			case CERT_FRIENDLY_NAME_PROP_ID:
			{
				printf("Display name: ");
				break;
			}
			case CERT_SIGNATURE_HASH_PROP_ID:
			{
				printf("Signature hash identifier ");
				break;
			}
			case CERT_KEY_PROV_HANDLE_PROP_ID:
			{
				printf("KEY PROVE HANDLE");
				break;
			}
			case CERT_KEY_PROV_INFO_PROP_ID:
			{
				printf("KEY PROV INFO PROP ID ");
				break;
			}
			case CERT_SHA1_HASH_PROP_ID:
			{
				printf("SHA1 HASH identifier");
				break;
			}
			case CERT_MD5_HASH_PROP_ID:
			{
				printf("md5 hash identifier ");
				break;
			}
			case CERT_KEY_CONTEXT_PROP_ID:
			{
				printf("KEY CONTEXT PROP identifier");
				break;
			}
			case CERT_KEY_SPEC_PROP_ID:
			{
				printf("KEY SPEC PROP identifier");
				break;
			}
			case CERT_ENHKEY_USAGE_PROP_ID:
			{
				printf("ENHKEY USAGE PROP identifier");
				break;
			}
			case CERT_NEXT_UPDATE_LOCATION_PROP_ID:
			{
				printf("NEXT UPDATE LOCATION PROP identifier");
				break;
			}
			case CERT_PVK_FILE_PROP_ID:
			{
				printf("PVK FILE PROP identifier ");
				break;
			}
			case CERT_DESCRIPTION_PROP_ID:
			{
				printf("DESCRIPTION PROP identifier ");
				break;
			}
			case CERT_ACCESS_STATE_PROP_ID:
			{
				printf("ACCESS STATE PROP identifier ");
				break;
			}
			case CERT_SMART_CARD_DATA_PROP_ID:
			{
				printf("SMART_CARD DATA PROP identifier ");
				break;
			}
			case CERT_EFS_PROP_ID:
			{
				printf("EFS PROP identifier ");
				break;
			}
			case CERT_FORTEZZA_DATA_PROP_ID:
			{
				printf("FORTEZZA DATA PROP identifier ");
				break;
			}
			case CERT_ARCHIVED_PROP_ID:
			{
				printf("ARCHIVED PROP identifier ");
				break;
			}
			case CERT_KEY_IDENTIFIER_PROP_ID:
			{
				printf("KEY IDENTIFIER PROP identifier ");
				break;
			}
			case CERT_AUTO_ENROLL_PROP_ID:
			{
				printf("AUTO ENROLL identifier. ");
				break;
			}
			} // End switch.

		 //-------------------------------------------------------------------
		 // Retrieve information on the property by first getting the 
		 // property size. 
		 // For more information, see CertGetCertificateContextProperty.

			if (CertGetCertificateContextProperty(
				pCertContext,
				dwPropId,
				NULL,
				&cbData))
			{
				//  Continue.
			}
			else
			{
				// If the first call to the function failed,
				// exit to an error routine.
				MyCertHandleError("Call #1 to GetCertContextProperty failed.");
			}
			//-------------------------------------------------------------------
			// The call succeeded. Use the size to allocate memory 
			// for the property.

			if (pvData = (void*)malloc(cbData))
			{
				// Memory is allocated. Continue.
			}
			else
			{
				// If memory allocation failed, exit to an error routine.
				MyCertHandleError("Memory allocation failed.");
			}
			//----------------------------------------------------------------
			// Allocation succeeded. Retrieve the property data.

			if (CertGetCertificateContextProperty(
				pCertContext,
				dwPropId,
				pvData,
				&cbData))
			{
				// The data has been retrieved. Continue.
				cout << "dwPropId:" << dwPropId << " pvData:" << pvData << " cbData:" << cbData << " ";
				//-------------------------------------------------------------------
			// Indicate the kind of property found.

				switch (dwPropId)
				{
				case CERT_ISSUER_PUBLIC_KEY_MD5_HASH_PROP_ID: {
					//cout << "Issuer public key md5: " << pvData << endl;
					mpz_t result;
					mpz_init(result);
					//gmp_printf("%Zd\n", result);
					mpz_import(result, 1, -1, cbData, 0, 0, pvData);
					//gmp_printf("%Zd\n", result);
					char* tmp = mpz_get_str(NULL, 16, result);
					string issuer_public_key_md5 = tmp;
					cout << "Issuer public key md5= " << issuer_public_key_md5 << endl;
					mpz_clear(result);
					break;
				}
				case CERT_SUBJECT_PUBLIC_KEY_MD5_HASH_PROP_ID: {
					mpz_t result;
					mpz_init(result);
					mpz_import(result, 1, -1, cbData, 0, 0, pvData);
					char* tmp = mpz_get_str(NULL, 16, result);
					string subject_public_key = tmp;
					cout << "Subject public key md5= " << subject_public_key << endl;
					mpz_clear(result);
					break;
				}
				case CERT_FRIENDLY_NAME_PROP_ID:
				{
					printf("Display name: ");
					break;
				}
				case CERT_SIGNATURE_HASH_PROP_ID:
				{
					mpz_t result;
					mpz_init(result);
					mpz_import(result, 1, -1, cbData, 0, 0, pvData);
					char* tmp = mpz_get_str(NULL, 16, result);
					string signature_hash = tmp;
					cout << "Signature hash identifier = " << signature_hash << endl;
					mpz_clear(result);
					break;
				}
				case CERT_SUBJECT_PUB_KEY_BIT_LENGTH_PROP_ID: {
					mpz_t result;
					mpz_init(result);
					mpz_import(result, 1, 1, cbData, 0, 0, pvData);
					char* tmp = mpz_get_str(NULL, 16, result);
					string subject_pub_key_bit_length = tmp;
					cout << "Subject pub key bit length = " << subject_pub_key_bit_length << endl;
					mpz_clear(result);
					break;
				}
				case CERT_KEY_PROV_HANDLE_PROP_ID:
				{
					printf("KEY PROVE HANDLE");
					break;
				}
				case CERT_KEY_PROV_INFO_PROP_ID:
				{
					mpz_t result;
					mpz_init(result);
					mpz_import(result, 1, -1, cbData, 0, 0, pvData);
					char* tmp = mpz_get_str(NULL, 16, result);
					string key_prov_info = tmp;
					cout << "KEY PROV INFO PROP ID : " << key_prov_info << endl;
					mpz_clear(result);
					break;
				}
				case CERT_SHA1_HASH_PROP_ID:
				{
					mpz_t result;
					mpz_init(result);
					mpz_import(result, 1, -1, cbData, 0, 0, pvData);
					char* tmp = mpz_get_str(NULL, 16, result);
					string sha1_hash_identifier = tmp;
					cout << "SHA1 HASH identifier = " << sha1_hash_identifier << endl;
					mpz_clear(result);
					break;
				}
				case CERT_MD5_HASH_PROP_ID:
				{
					mpz_t result;
					mpz_init(result);
					mpz_import(result, 1, -1, cbData, 0, 0, pvData);
					char* tmp = mpz_get_str(NULL, 16, result);
					string md5_hash_identifier = tmp;
					cout << "Md5 hash identifier = " << md5_hash_identifier << endl;
					mpz_clear(result);
					break;
				}
				case CERT_KEY_CONTEXT_PROP_ID:
				{
					printf("KEY CONTEXT PROP identifier");
					break;
				}
				case CERT_KEY_SPEC_PROP_ID:
				{
					printf("KEY SPEC PROP identifier");
					break;
				}
				case CERT_ENHKEY_USAGE_PROP_ID:
				{
					printf("ENHKEY USAGE PROP identifier");
					break;
				}
				case CERT_NEXT_UPDATE_LOCATION_PROP_ID:
				{
					printf("NEXT UPDATE LOCATION PROP identifier");
					break;
				}
				case CERT_PVK_FILE_PROP_ID:
				{
					printf("PVK FILE PROP identifier ");
					break;
				}
				case CERT_DESCRIPTION_PROP_ID:
				{
					printf("DESCRIPTION PROP identifier ");
					break;
				}
				case CERT_ACCESS_STATE_PROP_ID:
				{
					printf("ACCESS STATE PROP identifier ");
					break;
				}
				case CERT_SMART_CARD_DATA_PROP_ID:
				{
					printf("SMART_CARD DATA PROP identifier ");
					break;
				}
				case CERT_EFS_PROP_ID:
				{
					printf("EFS PROP identifier ");
					break;
				}
				case CERT_FORTEZZA_DATA_PROP_ID:
				{
					printf("FORTEZZA DATA PROP identifier ");
					break;
				}
				case CERT_ARCHIVED_PROP_ID:
				{
					printf("ARCHIVED PROP identifier ");
					break;
				}
				case CERT_KEY_IDENTIFIER_PROP_ID:
				{
					mpz_t result;
					mpz_init(result);
					mpz_import(result, 1, -1, cbData, 0, 0, pvData);
					char* tmp = mpz_get_str(NULL, 16, result);
					string key_identifier = tmp;
					cout << "KEY IDENTIFIER PROP identifier  = " << key_identifier << endl;
					mpz_clear(result);
					break;
				}
				case CERT_AUTO_ENROLL_PROP_ID:
				{
					printf("AUTO ENROLL identifier. ");
					break;
				}
				} // End switch.
			}
			else
			{
				// If an error occurred in the second call, 
				// exit to an error routine.
				MyCertHandleError("Call #2 failed.");
			}
			//---------------------------------------------------------------
			// Show the results.


			//printf("The Property Content is %d \n", pvData);

			//----------------------------------------------------------------
			// Free the certificate context property memory.

			free(pvData);
		}

		cout << endl << "=================================================================================" << endl;
	}
	//-------------------------------------------------------------------
	// Clean up.
	CertFreeCertificateContext(pCertContext);
	CertCloseStore(hCertStore, 0);

	printf("The function completed successfully. \n");
}

void IWSAGetAllCertsListInfoDetailIS(LPCSTR certStoreSM2, LPCSTR certStoreRSA, int keySpec) {
	//	CertificateIS(const string certDN, const string issuerDN, const mpz_t certSN, const SYSTEMTIME validBegin, const SYSTEMTIME validEnd, const string CertStore, const string KeyUsage, const string CertType);
	string certDN;
	string issuerDN;
	mpz_t certSN;
	SYSTEMTIME validBegin, validEnd;
	string certStore;
	string keyUsage;
	string certType;
	
	HCERTSTORE       hCertStore;
	PCCERT_CONTEXT   pCertContext = NULL;
	void* pvData;
	DWORD            cbData;
	DWORD            dwPropId = 0;
	// Open a system certificate store.
	if (hCertStore = CertOpenSystemStore(NULL, certStoreRSA)) { fprintf(stderr, "The %s store has been opened. \n", certStoreRSA); }
	else { MyCertHandleError("The store was not opened."); }
	while (pCertContext = CertEnumCertificatesInStore(
		hCertStore,
		pCertContext))
	{
		cout << "Version:               " << pCertContext->pCertInfo->dwVersion << endl;
		mpz_t result;
		mpz_init(result);
		mpz_import(result, 1, -1, pCertContext->pCertInfo->SerialNumber.cbData, 0, 0, pCertContext->pCertInfo->SerialNumber.pbData);
		char* tmp = mpz_get_str(NULL, 16, result);
		string serialnumber = tmp;
		cout << "SerialNumber:          " << serialnumber << endl;

		string signature_algorithm = pCertContext->pCertInfo->SignatureAlgorithm.pszObjId;
		cout << "SignatureAlgorithm:    " << signature_algorithm << endl;

		cout << "Issuer:			";
		CERT_NAME_INFO* pDecodeName;  // point variable to hold the address of the decoded buffer
		DWORD cbDecoded;              // variable to hold the length of the decoded buffer
		BYTE* pbDecoded;              // variable to hold a pointer to the decoded buffer
		if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertContext->pCertInfo->Issuer.pbData, pCertContext->pCertInfo->Issuer.cbData, NULL, NULL, NULL, &cbDecoded)) {}
		else { MyCertHandleError("The first call to the function failed.\n"); }
		//-------------------------------------------------------------------
		// Allocate memory for the decoded information.
		if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
		if (CryptDecodeObjectEx(
			MY_TYPE,
			X509_NAME,
			pCertContext->pCertInfo->Issuer.pbData,
			pCertContext->pCertInfo->Issuer.cbData,
			NULL,
			NULL,
			pbDecoded,
			&cbDecoded
		))
		{
			pDecodeName = (CERT_NAME_INFO*)pbDecoded;
			for (DWORD i = 0; i < pDecodeName->cRDN; i++)
			{
				for (DWORD j = 0; j < pDecodeName->rgRDN[i].cRDNAttr; j++)
				{
					CERT_RDN_ATTR& rdnAttribute = pDecodeName->rgRDN[i].rgRDNAttr[j];
					LPSTR psz;

					if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
					{
						MyCertHandleError("Buffer memory allocation failed.");
					}
					if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
						if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
							// cout << CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData) << endl;
							// cout << rdnAttribute.dwValueType << " " << rdnAttribute.Value.cbData << " " << rdnAttribute.Value.pbData << endl;
							string value_str = psz;
							string pszObjId_str = rdnAttribute.pszObjId;

							if (pszObjId_str == szOID_COUNTRY_NAME) {
								cout << "C = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
								cout << "S = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_LOCALITY_NAME) {
								cout << "L = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
								cout << "O = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
								cout << "OU = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_COMMON_NAME) {
								cout << "CN = " << value_str << " ";
							}
						}
					}
					else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {
						string pszObjId_str = rdnAttribute.pszObjId;
						if (pszObjId_str == szOID_COUNTRY_NAME) {
							cout << "C = " << rdnAttribute.Value.pbData << " ";
						}
						else if (pszObjId_str == szOID_RSA_emailAddr) {
							cout << "E = " << rdnAttribute.Value.pbData << " ";
						}

					}
					else { MyCertHandleError("CertRDNValueToStr failed."); }
					free(psz);
				}
			}
		}
		else
		{
			MyCertHandleError("Decode failed.");
		}
		cout << endl;

		LPSYSTEMTIME pst = new SYSTEMTIME();
		//if(pst = (LPSYSTEMTIME)malloc(sizeof(SYSTEMTIME))){MyCertHandleError("Buffer memory allocation failed."); }; BUGGED
		if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotBefore, pst)) {
			cout << "Start time:		" << pst->wDayOfWeek << " " << pst->wDay << " " << pst->wMonth << " " << pst->wYear << " " << pst->wHour << ":" << pst->wMinute << ":" << pst->wSecond << endl;
		}
		if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotAfter, pst)) {
			cout << "End time:		" << pst->wDayOfWeek << " " << pst->wDay << " " << pst->wMonth << " " << pst->wYear << " " << pst->wHour << ":" << pst->wMinute << ":" << pst->wSecond << endl;
		}

		cout << "Subject:		";

	

		if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertContext->pCertInfo->Subject.pbData, pCertContext->pCertInfo->Subject.cbData, NULL, NULL, NULL, &cbDecoded)) {}
		else { MyCertHandleError("The first call to the function failed.\n"); }
		if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
		if (CryptDecodeObjectEx(
			MY_TYPE,
			X509_NAME,
			pCertContext->pCertInfo->Subject.pbData,
			pCertContext->pCertInfo->Subject.cbData,
			NULL,
			NULL,
			pbDecoded,
			&cbDecoded
		))
		{
			pDecodeName = (CERT_NAME_INFO*)pbDecoded;
			for (DWORD i = 0; i < pDecodeName->cRDN; i++)
			{
				for (DWORD j = 0; j < pDecodeName->rgRDN[i].cRDNAttr; j++)
				{
					CERT_RDN_ATTR& rdnAttribute = pDecodeName->rgRDN[i].rgRDNAttr[j];
					LPSTR psz;
					if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
					{
						MyCertHandleError("Buffer memory allocation failed.");
					}
					if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
						if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
							// cout << CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData) << endl;
							// cout << rdnAttribute.dwValueType << " " << rdnAttribute.Value.cbData << " " << rdnAttribute.Value.pbData << endl;
							string value_str = psz;
							string pszObjId_str = rdnAttribute.pszObjId;

							if (pszObjId_str == szOID_COUNTRY_NAME) {
								cout << "C = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
								cout << "S = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_LOCALITY_NAME) {
								cout << "L = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
								cout << "O = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
								cout << "OU = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_COMMON_NAME) {
								cout << "CN = " << value_str << " ";
							}
						}
					}
					else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {
						string pszObjId_str = rdnAttribute.pszObjId;
						if (pszObjId_str == szOID_COUNTRY_NAME) {
							cout << "C = " << rdnAttribute.Value.pbData << " ";
						}
						else if (pszObjId_str == szOID_RSA_emailAddr) {
							cout << "E = " << rdnAttribute.Value.pbData << " ";
						}

					}
					else { MyCertHandleError("CertRDNValueToStr failed."); }
					free(psz);
				}
			}
		}
		else
		{
			MyCertHandleError("Decode failed.");
		}
		cout << endl;

		string algo_name;
		string algo_id = pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId;
		if (szOID_RSA_RSA == algo_id) {
			algo_name = "RSA ";
		}
		cout << algo_name << "Public Key:        ";// << pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData << endl;
		for (int i = 0; i < pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData; ++i) {
			printf("%02X ", pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData[i]);
		}
		cout << endl;

		cout << "Issuer Unique Id:	";
		for (int i = 0; i < pCertContext->pCertInfo->IssuerUniqueId.cbData; ++i) {
			printf("%02X ", pCertContext->pCertInfo->IssuerUniqueId.pbData[i]);
		}
		cout << endl;

		cout << "Subject Unique Id:	";
		for (int i = 0; i < pCertContext->pCertInfo->SubjectUniqueId.cbData; ++i) {
			printf("%02X ", pCertContext->pCertInfo->SubjectUniqueId.pbData[i]);
		}
		cout << endl;

		for (int i = 0; i < pCertContext->pCertInfo->cExtension; ++i) {
			CERT_EXTENSION& cert_extension = pCertContext->pCertInfo->rgExtension[i];
			//cout << cert_extension.pszObjId << " " << cert_extension.fCritical << " " << endl;
			string cert_str = cert_extension.pszObjId;
			if (cert_str == szOID_BASIC_CONSTRAINTS2) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_EXTENSIONS, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
				else { MyCertHandleError("Decode extension failed.\n"); }
				if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
				if (CryptDecodeObjectEx(MY_TYPE, X509_EXTENSIONS, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
					PCERT_BASIC_CONSTRAINTS2_INFO pCertBasicConstraints2info = (PCERT_BASIC_CONSTRAINTS2_INFO)pbDecoded;
					cout << "Extension Basic Constraint2: ";
					if (cert_extension.fCritical) cout << "Critical, ";
					cout << pCertBasicConstraints2info->fCA << " ";
					if (pCertBasicConstraints2info->fCA) {
						cout << pCertBasicConstraints2info->fPathLenConstraint << " ";
						if (pCertBasicConstraints2info->fPathLenConstraint) cout << pCertBasicConstraints2info->dwPathLenConstraint;
					}
				}
			}
			else if (cert_str == szOID_SUBJECT_KEY_IDENTIFIER) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_OCTET_STRING, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
				else { MyCertHandleError("Decode extension failed.\n"); }
				if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
				if (CryptDecodeObjectEx(MY_TYPE, X509_OCTET_STRING, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
					PCRYPT_DATA_BLOB  pCryptDataBlob = (PCRYPT_DATA_BLOB)pbDecoded;
					cout << "Extension Subject Key Identifier: ";
					for (int i = 0; i < pCryptDataBlob->cbData; ++i) {
						printf("%02X", pCryptDataBlob->pbData[i]);
					}
				}
			}
			else if (cert_str == szOID_AUTHORITY_KEY_IDENTIFIER2) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_AUTHORITY_KEY_ID2, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
				else { MyCertHandleError("Decode extension failed.\n"); }
				if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
				if (CryptDecodeObjectEx(MY_TYPE, X509_AUTHORITY_KEY_ID2, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
					PCERT_AUTHORITY_KEY_ID2_INFO   pCryptDataBlob = (PCERT_AUTHORITY_KEY_ID2_INFO)pbDecoded;
					cout << "Extension Authority Key Identifier2 Info:\n	KeyId: ";
					for (int i = 0; i < pCryptDataBlob->KeyId.cbData; ++i) {
						printf("%02X", pCryptDataBlob->KeyId.pbData[i]);
					}
					cout << endl;
					for (int i = 0; i < pCryptDataBlob->AuthorityCertIssuer.cAltEntry; ++i) {
						CERT_ALT_NAME_ENTRY& pCertAltNameEntry = pCryptDataBlob->AuthorityCertIssuer.rgAltEntry[i];
						//cout << "AltNameChoice:" << pCertAltNameEntry.dwAltNameChoice << endl;
						switch (pCertAltNameEntry.dwAltNameChoice) {
						case CERT_ALT_NAME_DIRECTORY_NAME:
							cout << "	DirectoryName:";
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.DirectoryName.pbData, pCertAltNameEntry.DirectoryName.cbData, NULL, NULL, NULL, &cbDecoded)) {}
							else { MyCertHandleError("Decode extension failed.\n"); }
							if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.DirectoryName.pbData, pCertAltNameEntry.DirectoryName.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
								PCERT_NAME_INFO pCertNameInfo = (PCERT_NAME_INFO)pbDecoded;
								for (DWORD i = 0; i < pCertNameInfo->cRDN; i++)
								{
									for (DWORD j = 0; j < pCertNameInfo->rgRDN[i].cRDNAttr; j++)
									{
										CERT_RDN_ATTR& rdnAttribute = pCertNameInfo->rgRDN[i].rgRDNAttr[j];
										LPSTR psz;

										if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
										{
											MyCertHandleError("Buffer memory allocation failed.");
										}
										if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
											if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
												// cout << CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData) << endl;
												// cout << rdnAttribute.dwValueType << " " << rdnAttribute.Value.cbData << " " << rdnAttribute.Value.pbData << endl;
												string value_str = psz;
												string pszObjId_str = rdnAttribute.pszObjId;

												if (pszObjId_str == szOID_COUNTRY_NAME) {
													cout << "C = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
													cout << "S = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_LOCALITY_NAME) {
													cout << "L = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
													cout << "O = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
													cout << "OU = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_COMMON_NAME) {
													cout << "CN = " << value_str << " ";
												}
											}
										}
										else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {

											string pszObjId_str = rdnAttribute.pszObjId;

											if (pszObjId_str == szOID_COUNTRY_NAME) {
												cout << "C = " << rdnAttribute.Value.pbData << " ";
											}
											else if (pszObjId_str == szOID_RSA_emailAddr) {
												cout << "E = " << rdnAttribute.Value.pbData << " ";
											}
										}
										else { MyCertHandleError("CertRDNValueToStr failed."); }
										free(psz);
									}
								}
							}
							break;
						case CERT_ALT_NAME_OTHER_NAME:
							cout << "	OtherName:";
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.pOtherName->Value.pbData, pCertAltNameEntry.pOtherName->Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
							else { MyCertHandleError("Decode extension failed.\n"); }
							if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.pOtherName->Value.pbData, pCertAltNameEntry.pOtherName->Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
								PCERT_NAME_INFO pCertNameInfo = (PCERT_NAME_INFO)pbDecoded;
								for (DWORD i = 0; i < pCertNameInfo->cRDN; i++)
								{
									for (DWORD j = 0; j < pCertNameInfo->rgRDN[i].cRDNAttr; j++)
									{
										CERT_RDN_ATTR& rdnAttribute = pCertNameInfo->rgRDN[i].rgRDNAttr[j];
										LPSTR psz;
										if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
										{
											MyCertHandleError("Buffer memory allocation failed.");
										}
										if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
											if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
												string value_str = psz;
												string pszObjId_str = rdnAttribute.pszObjId;
												if (pszObjId_str == szOID_COUNTRY_NAME) {
													cout << "C = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
													cout << "S = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_LOCALITY_NAME) {
													cout << "L = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
													cout << "O = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
													cout << "OU = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_COMMON_NAME) {
													cout << "CN = " << value_str << " ";
												}
											}
										}
										else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {
											string pszObjId_str = rdnAttribute.pszObjId;
											if (pszObjId_str == szOID_COUNTRY_NAME) {
												cout << "C = " << rdnAttribute.Value.pbData << " ";
											}
											else if (pszObjId_str == szOID_RSA_emailAddr) {
												cout << "E = " << rdnAttribute.Value.pbData << " ";
											}
										}
										else { MyCertHandleError("CertRDNValueToStr failed."); }
										free(psz);
									}
								}
							}
							break;
						case CERT_ALT_NAME_RFC822_NAME:
							wcout << "	RFC822Name: " << pCertAltNameEntry.pwszRfc822Name;
							break;
						case CERT_ALT_NAME_DNS_NAME:
							wcout << "	DNSName: " << pCertAltNameEntry.pwszDNSName;
							break;
						case CERT_ALT_NAME_URL:
							wcout << "	UrlName: " << pCertAltNameEntry.pwszURL;
						case CERT_ALT_NAME_IP_ADDRESS:
							cout << " IP Address: ";
							for (int i = 0; i < pCertAltNameEntry.IPAddress.cbData; ++i) {
								printf("%02X", pCertAltNameEntry.IPAddress.pbData[i]);
							}
							break;
						case CERT_ALT_NAME_REGISTERED_ID:
							cout << " RegisterdID: " << pCertAltNameEntry.pszRegisteredID;
							break;
						}

					}					
					cout << endl << "	Authority Key Serial Number: ";
					for (int j = pCryptDataBlob->AuthorityCertSerialNumber.cbData-1; j >=0 ; --j) {
						printf("%02X", pCryptDataBlob->AuthorityCertSerialNumber.pbData[j]);
					}

				}
			}
			else if (cert_str == szOID_KEY_USAGE) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {
					if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
					if (CryptDecodeObjectEx(MY_TYPE, X509_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
						PCRYPT_BIT_BLOB pCryptBitBlob = (PCRYPT_BIT_BLOB)pbDecoded;
						cout << "Extension KeyUsage: ";
						if (cert_extension.fCritical) cout << "Critical, ";
						for (int i = 0; i < pCryptBitBlob->cbData; ++i) {
							printf("%02X ", pCryptBitBlob->pbData[i]);
						}
					}
				}
				else { MyCertHandleError("Decode extension failed.\n"); }		
			}
			else if (cert_str == szOID_ENHANCED_KEY_USAGE) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_ENHANCED_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {
					if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
					if (CryptDecodeObjectEx(MY_TYPE, X509_ENHANCED_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
						PCERT_ENHKEY_USAGE pCertEnhkeyUsage = (PCERT_ENHKEY_USAGE)pbDecoded;
						cout << "Extension Enhanced KeyUsage: ";
						if (cert_extension.fCritical) cout << "Critical, ";
						for (int i = 0; i < pCertEnhkeyUsage->cUsageIdentifier; ++i) {
							string enhanced_keyUsage = pCertEnhkeyUsage->rgpszUsageIdentifier[i];
							if (enhanced_keyUsage == szOID_PKIX_KP) cout << "PKIX_KP ";
							if (enhanced_keyUsage == szOID_PKIX_KP_SERVER_AUTH) cout << "Server Auth ";
							if (enhanced_keyUsage == szOID_PKIX_KP_CLIENT_AUTH) cout << "Client Auth ";
							if (enhanced_keyUsage == szOID_PKIX_KP_CODE_SIGNING) cout << "Code Signing ";
							if (enhanced_keyUsage == szOID_PKIX_KP_EMAIL_PROTECTION) cout << "Email Protection ";
							if (enhanced_keyUsage == szOID_PKIX_KP_IPSEC_END_SYSTEM) cout << "IPSEC End System ";
							if (enhanced_keyUsage == szOID_PKIX_KP_IPSEC_TUNNEL) cout << "IPSEC Tunnel ";
							if (enhanced_keyUsage == szOID_PKIX_KP_IPSEC_USER) cout << "IPSEC User ";
							if (enhanced_keyUsage == szOID_PKIX_KP_TIMESTAMP_SIGNING) cout << "Time Stamping ";
							if (enhanced_keyUsage == szOID_PKIX_KP_OCSP_SIGNING) cout << "OCSP Signing ";
							if (enhanced_keyUsage == szOID_PKIX_OCSP_NOCHECK) cout << "OCSP Nochecking ";
							if (enhanced_keyUsage == szOID_PKIX_OCSP_NONCE) cout << "OCSP Nonce ";
							if (enhanced_keyUsage == szOID_IPSEC_KP_IKE_INTERMEDIATE) cout << "Internet Key Exchange ";
							if (enhanced_keyUsage == szOID_PKINIT_KP_KDC) cout << "KDC ";
						}
						cout << endl;
					}
				}
			else { MyCertHandleError("Decode extension failed.\n"); }
			}
			else continue;
			cout << endl;
		}

		//	CertificateIS(const string certDN, const string issuerDN, const mpz_t certSN, const SYSTEMTIME validBegin, const SYSTEMTIME validEnd, const string CertStore, const string KeyUsage, const string CertType);
		//CertificateIS* pCert = new CertificateIS();

		mpz_clear(result);
		delete(pst);
		cout << endl << "=================================================================================" << endl;
	}
	//-------------------------------------------------------------------
	// Clean up.
	CertFreeCertificateContext(pCertContext);
	CertCloseStore(hCertStore, 0);

	printf("The function completed successfully. \n");
}

string IWSAGetAllCertsListInfoIS(LPCSTR certStoreSM2, LPCSTR certStoreRSA, int keySpec) {
	//	CertificateIS(const string certDN, const string issuerDN, const mpz_t certSN, const SYSTEMTIME validBegin, const SYSTEMTIME validEnd, const string CertStore, const string KeyUsage, const string CertType);
	string certDN;
	string issuerDN;
	mpz_t certSN;
	SYSTEMTIME validBegin, validEnd;
	string certStore;
	string keyUsage;
	string certType;
	ostringstream ostreamTmp;
	vector<string> certsList_vector;
	if (certStoreSM2 != NULL)certStore = certStoreSM2;
	if (certStoreRSA != NULL)certStore = certStoreRSA;
	if (("" == certStoreSM2 || NULL == certStoreSM2) && ("" == certStoreRSA || NULL==certStoreRSA)) return "[]";

	mpz_init(certSN);

	HCERTSTORE       hCertStore;
	PCCERT_CONTEXT   pCertContext = NULL;
	void* pvData;
	DWORD            cbData;
	DWORD            dwPropId = 0;
	// Open a system certificate store.
	if (hCertStore = CertOpenSystemStore(NULL, certStoreRSA)) { fprintf(stderr, "The %s store has been opened. \n", certStoreRSA); }
	else { MyCertHandleError("The store was not opened."); }
	while (pCertContext = CertEnumCertificatesInStore(
		hCertStore,
		pCertContext))
	{
		cout << "Version:               " << pCertContext->pCertInfo->dwVersion << endl;
		mpz_t result;
		mpz_init(result);
		mpz_import(result, 1, -1, pCertContext->pCertInfo->SerialNumber.cbData, 0, 0, pCertContext->pCertInfo->SerialNumber.pbData);
		char* tmp = mpz_get_str(NULL, 16, result);
		string serialnumber = tmp;
		mpz_set(certSN, result);
		cout << "SerialNumber:          " << serialnumber << endl;

		string signature_algorithm = pCertContext->pCertInfo->SignatureAlgorithm.pszObjId;
		cout << "SignatureAlgorithm:    " << signature_algorithm << endl;

		cout << "Issuer:			";
		CERT_NAME_INFO* pDecodeName;  // point variable to hold the address of the decoded buffer
		DWORD cbDecoded;              // variable to hold the length of the decoded buffer
		BYTE* pbDecoded;              // variable to hold a pointer to the decoded buffer
		if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertContext->pCertInfo->Issuer.pbData, pCertContext->pCertInfo->Issuer.cbData, NULL, NULL, NULL, &cbDecoded)) {}
		else { MyCertHandleError("The first call to the function failed.\n"); }
		//-------------------------------------------------------------------
		// Allocate memory for the decoded information.
		if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
		if (CryptDecodeObjectEx(
			MY_TYPE,
			X509_NAME,
			pCertContext->pCertInfo->Issuer.pbData,
			pCertContext->pCertInfo->Issuer.cbData,
			NULL,
			NULL,
			pbDecoded,
			&cbDecoded
		))
		{
			pDecodeName = (CERT_NAME_INFO*)pbDecoded;
			for (DWORD i = 0; i < pDecodeName->cRDN; i++)
			{
				for (DWORD j = 0; j < pDecodeName->rgRDN[i].cRDNAttr; j++)
				{
					CERT_RDN_ATTR& rdnAttribute = pDecodeName->rgRDN[i].rgRDNAttr[j];
					LPSTR psz;

					if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
					{
						MyCertHandleError("Buffer memory allocation failed.");
					}
					if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
						if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
							string value_str = psz;
							string pszObjId_str = rdnAttribute.pszObjId;

							if (pszObjId_str == szOID_COUNTRY_NAME) {
								ostreamTmp << "C = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
								ostreamTmp << "S = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_LOCALITY_NAME) {
								ostreamTmp << "L = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
								ostreamTmp << "O = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
								ostreamTmp << "OU = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_COMMON_NAME) {
								ostreamTmp << "CN = " << value_str << " ";
							}
						}
					}
					else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {
						string pszObjId_str = rdnAttribute.pszObjId;
						if (pszObjId_str == szOID_COUNTRY_NAME) {
							ostreamTmp << "C = " << rdnAttribute.Value.pbData << " ";
						}
						else if (pszObjId_str == szOID_RSA_emailAddr) {
							ostreamTmp << "E = " << rdnAttribute.Value.pbData << " ";
						}

					}
					else { MyCertHandleError("CertRDNValueToStr failed."); }
					free(psz);
				}
			}
		}
		else
		{
			MyCertHandleError("Decode failed.");
		}
		issuerDN = ostreamTmp.str();
		ostreamTmp.str("");
		cout << issuerDN << endl;

		LPSYSTEMTIME pst = new SYSTEMTIME();
		//if(pst = (LPSYSTEMTIME)malloc(sizeof(SYSTEMTIME))){MyCertHandleError("Buffer memory allocation failed."); }; BUGGED
		if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotBefore, pst)) {
			cout << "Start time:		" << pst->wDayOfWeek << " " << pst->wDay << " " << pst->wMonth << " " << pst->wYear << " " << pst->wHour << ":" << pst->wMinute << ":" << pst->wSecond << endl;
			validBegin = *pst;
		}
		if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotAfter, pst)) {
			cout << "End time:		" << pst->wDayOfWeek << " " << pst->wDay << " " << pst->wMonth << " " << pst->wYear << " " << pst->wHour << ":" << pst->wMinute << ":" << pst->wSecond << endl;
			validEnd = *pst;
		}

		cout << "Subject:		";
		/*
		LPSTR pCertName;
		DWORD cCertName;

		cCertName = CertNameToStrA(X509_ASN_ENCODING, &pCertContext->pCertInfo->Subject, CERT_SIMPLE_NAME_STR, NULL, 0);
		pCertName = (LPSTR)malloc(cCertName);

		CertNameToStrA(X509_ASN_ENCODING, &pCertContext->pCertInfo->Subject, CERT_SIMPLE_NAME_STR, pCertName, cCertName);
		cout << pCertName << endl;
		
		
		LPSTR          pszNameString;
		DWORD          cchNameString;
		DWORD dType = CERT_X500_NAME_STR;
		cchNameString = CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, &dType, NULL, 0);
		pszNameString = (LPSTR)malloc(cchNameString);
		CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, &dType, pszNameString, cchNameString);
		cout << pszNameString << endl;
		*/
		if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertContext->pCertInfo->Subject.pbData, pCertContext->pCertInfo->Subject.cbData, NULL, NULL, NULL, &cbDecoded)) {}
		else { MyCertHandleError("The first call to the function failed.\n"); }
		if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
		if (CryptDecodeObjectEx(
			MY_TYPE,
			X509_NAME,
			pCertContext->pCertInfo->Subject.pbData,
			pCertContext->pCertInfo->Subject.cbData,
			NULL,
			NULL,
			pbDecoded,
			&cbDecoded
		))
		{
			pDecodeName = (CERT_NAME_INFO*)pbDecoded;
			for (DWORD i = 0; i < pDecodeName->cRDN; i++)
			{
				for (DWORD j = 0; j < pDecodeName->rgRDN[i].cRDNAttr; j++)
				{
					CERT_RDN_ATTR& rdnAttribute = pDecodeName->rgRDN[i].rgRDNAttr[j];
					LPSTR psz;
					if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
					{
						MyCertHandleError("Buffer memory allocation failed.");
					}
					if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
						if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
							string value_str = psz;
							string pszObjId_str = rdnAttribute.pszObjId;

							if (pszObjId_str == szOID_COUNTRY_NAME) {
								ostreamTmp << "C = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
								ostreamTmp << "S = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_LOCALITY_NAME) {
								ostreamTmp << "L = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
								ostreamTmp << "O = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
								ostreamTmp << "OU = " << value_str << " ";
							}
							else if (pszObjId_str == szOID_COMMON_NAME) {
								ostreamTmp << "CN = " << value_str << " ";
							}
						}
					}
					else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {
						string pszObjId_str = rdnAttribute.pszObjId;
						if (pszObjId_str == szOID_COUNTRY_NAME) {
							ostreamTmp << "C = " << rdnAttribute.Value.pbData << " ";
						}
						else if (pszObjId_str == szOID_RSA_emailAddr) {
							ostreamTmp << "E = " << rdnAttribute.Value.pbData << " ";
						}

					}
					else { MyCertHandleError("CertRDNValueToStr failed."); }
					free(psz);
				}
			}
		}
		else
		{
			MyCertHandleError("Decode failed.");
		}

		certDN = ostreamTmp.str();
		ostreamTmp.str("");
		cout << certDN << endl;
		cout << endl;

		string algo_name;
		string algo_id = pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId;
		if (szOID_RSA_RSA == algo_id) {
			algo_name = "RSA";
			certType = algo_name;
		}
		cout << algo_name << "Public Key:        ";// << pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData << endl;
		for (int i = 0; i < pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData; ++i) {
			printf("%02X ", pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData[i]);
		}
		cout << endl;

		cout << "Issuer Unique Id:	";
		for (int i = 0; i < pCertContext->pCertInfo->IssuerUniqueId.cbData; ++i) {
			printf("%02X ", pCertContext->pCertInfo->IssuerUniqueId.pbData[i]);
		}
		cout << endl;

		cout << "Subject Unique Id:	";
		for (int i = 0; i < pCertContext->pCertInfo->SubjectUniqueId.cbData; ++i) {
			printf("%02X ", pCertContext->pCertInfo->SubjectUniqueId.pbData[i]);
		}
		cout << endl;

		for (int i = 0; i < pCertContext->pCertInfo->cExtension; ++i) {
			CERT_EXTENSION& cert_extension = pCertContext->pCertInfo->rgExtension[i];
			//cout << cert_extension.pszObjId << " " << cert_extension.fCritical << " " << endl;
			string cert_str = cert_extension.pszObjId;
			if (cert_str == szOID_BASIC_CONSTRAINTS2) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_BASIC_CONSTRAINTS2, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
				else { MyCertHandleError("Basic constraint2 Decode extension failed.\n"); }
				if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
				if (CryptDecodeObjectEx(MY_TYPE, X509_BASIC_CONSTRAINTS2, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
					PCERT_BASIC_CONSTRAINTS2_INFO pCertBasicConstraints2info = (PCERT_BASIC_CONSTRAINTS2_INFO)pbDecoded;
					cout << "Extension Basic Constraint2: ";
					if (cert_extension.fCritical) cout << "Critical, ";
					cout << pCertBasicConstraints2info->fCA << " ";
					if (pCertBasicConstraints2info->fCA) {
						cout << pCertBasicConstraints2info->fPathLenConstraint << " ";
						if (pCertBasicConstraints2info->fPathLenConstraint) cout << pCertBasicConstraints2info->dwPathLenConstraint;
					}
				}
			}
			else if (cert_str == szOID_SUBJECT_KEY_IDENTIFIER) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_OCTET_STRING, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
				else { MyCertHandleError("Subject Key Identifier Decode extension failed.\n"); }
				if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
				if (CryptDecodeObjectEx(MY_TYPE, X509_OCTET_STRING, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
					PCRYPT_DATA_BLOB  pCryptDataBlob = (PCRYPT_DATA_BLOB)pbDecoded;
					cout << "Extension Subject Key Identifier: ";
					for (int i = 0; i < pCryptDataBlob->cbData; ++i) {
						printf("%02X", pCryptDataBlob->pbData[i]);
					}
				}
			}
			else if (cert_str == szOID_AUTHORITY_KEY_IDENTIFIER2) {
				if (CryptDecodeObjectEx(MY_TYPE, X509_AUTHORITY_KEY_ID2, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
				else { MyCertHandleError("Authority key identifier 2 Decode extension failed.\n"); }
				if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
				if (CryptDecodeObjectEx(MY_TYPE, X509_AUTHORITY_KEY_ID2, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
					PCERT_AUTHORITY_KEY_ID2_INFO   pCryptDataBlob = (PCERT_AUTHORITY_KEY_ID2_INFO)pbDecoded;
					cout << "Extension Authority Key Identifier2 Info:\n	KeyId: ";
					for (int i = 0; i < pCryptDataBlob->KeyId.cbData; ++i) {
						printf("%02X", pCryptDataBlob->KeyId.pbData[i]);
					}
					cout << endl;
					for (int i = 0; i < pCryptDataBlob->AuthorityCertIssuer.cAltEntry; ++i) {
						CERT_ALT_NAME_ENTRY& pCertAltNameEntry = pCryptDataBlob->AuthorityCertIssuer.rgAltEntry[i];
						//cout << "AltNameChoice:" << pCertAltNameEntry.dwAltNameChoice << endl;
						switch (pCertAltNameEntry.dwAltNameChoice) {
						case CERT_ALT_NAME_DIRECTORY_NAME:
							cout << "	DirectoryName:";
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.DirectoryName.pbData, pCertAltNameEntry.DirectoryName.cbData, NULL, NULL, NULL, &cbDecoded)) {}
							else { MyCertHandleError("DirectoryName Decode extension failed.\n"); }
							if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.DirectoryName.pbData, pCertAltNameEntry.DirectoryName.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
								PCERT_NAME_INFO pCertNameInfo = (PCERT_NAME_INFO)pbDecoded;
								for (DWORD i = 0; i < pCertNameInfo->cRDN; i++)
								{
									for (DWORD j = 0; j < pCertNameInfo->rgRDN[i].cRDNAttr; j++)
									{
										CERT_RDN_ATTR& rdnAttribute = pCertNameInfo->rgRDN[i].rgRDNAttr[j];
										LPSTR psz;

										if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
										{
											MyCertHandleError("Buffer memory allocation failed.");
										}
										if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
											if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
												// cout << CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData) << endl;
												// cout << rdnAttribute.dwValueType << " " << rdnAttribute.Value.cbData << " " << rdnAttribute.Value.pbData << endl;
												string value_str = psz;
												string pszObjId_str = rdnAttribute.pszObjId;

												if (pszObjId_str == szOID_COUNTRY_NAME) {
													cout << "C = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
													cout << "S = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_LOCALITY_NAME) {
													cout << "L = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
													cout << "O = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
													cout << "OU = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_COMMON_NAME) {
													cout << "CN = " << value_str << " ";
												}
											}
										}
										else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {

											string pszObjId_str = rdnAttribute.pszObjId;

											if (pszObjId_str == szOID_COUNTRY_NAME) {
												cout << "C = " << rdnAttribute.Value.pbData << " ";
											}
											else if (pszObjId_str == szOID_RSA_emailAddr) {
												cout << "E = " << rdnAttribute.Value.pbData << " ";
											}
										}
										else { MyCertHandleError("CertRDNValueToStr failed."); }
										free(psz);
									}
								}
							}
							break;
						case CERT_ALT_NAME_OTHER_NAME:
							cout << "	OtherName:";
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.pOtherName->Value.pbData, pCertAltNameEntry.pOtherName->Value.cbData, NULL, NULL, NULL, &cbDecoded)) {}
							else { MyCertHandleError("Decode extension failed.\n"); }
							if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
							if (CryptDecodeObjectEx(MY_TYPE, X509_NAME, pCertAltNameEntry.pOtherName->Value.pbData, pCertAltNameEntry.pOtherName->Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
								PCERT_NAME_INFO pCertNameInfo = (PCERT_NAME_INFO)pbDecoded;
								for (DWORD i = 0; i < pCertNameInfo->cRDN; i++)
								{
									for (DWORD j = 0; j < pCertNameInfo->rgRDN[i].cRDNAttr; j++)
									{
										CERT_RDN_ATTR& rdnAttribute = pCertNameInfo->rgRDN[i].rgRDNAttr[j];
										LPSTR psz;
										if (!(psz = (LPSTR)malloc(rdnAttribute.Value.cbData)))
										{
											MyCertHandleError("Buffer memory allocation failed.");
										}
										if (rdnAttribute.dwValueType == CERT_RDN_UTF8_STRING) {
											if (CertRDNValueToStr(rdnAttribute.dwValueType, &rdnAttribute.Value, psz, rdnAttribute.Value.cbData)) {
												string value_str = psz;
												string pszObjId_str = rdnAttribute.pszObjId;
												if (pszObjId_str == szOID_COUNTRY_NAME) {
													cout << "C = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_STATE_OR_PROVINCE_NAME) {
													cout << "S = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_LOCALITY_NAME) {
													cout << "L = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATION_NAME) {
													cout << "O = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_ORGANIZATIONAL_UNIT_NAME) {
													cout << "OU = " << value_str << " ";
												}
												else if (pszObjId_str == szOID_COMMON_NAME) {
													cout << "CN = " << value_str << " ";
												}
											}
										}
										else if (rdnAttribute.dwValueType != CERT_RDN_UTF8_STRING) {
											string pszObjId_str = rdnAttribute.pszObjId;
											if (pszObjId_str == szOID_COUNTRY_NAME) {
												cout << "C = " << rdnAttribute.Value.pbData << " ";
											}
											else if (pszObjId_str == szOID_RSA_emailAddr) {
												cout << "E = " << rdnAttribute.Value.pbData << " ";
											}
										}
										else { MyCertHandleError("CertRDNValueToStr failed."); }
										free(psz);
									}
								}
							}
							break;
						case CERT_ALT_NAME_RFC822_NAME:
							wcout << "	RFC822Name: " << pCertAltNameEntry.pwszRfc822Name;
							break;
						case CERT_ALT_NAME_DNS_NAME:
							wcout << "	DNSName: " << pCertAltNameEntry.pwszDNSName;
							break;
						case CERT_ALT_NAME_URL:
							wcout << "	UrlName: " << pCertAltNameEntry.pwszURL;
						case CERT_ALT_NAME_IP_ADDRESS:
							cout << " IP Address: ";
							for (int i = 0; i < pCertAltNameEntry.IPAddress.cbData; ++i) {
								printf("%02X", pCertAltNameEntry.IPAddress.pbData[i]);
							}
							break;
						case CERT_ALT_NAME_REGISTERED_ID:
							cout << " RegisterdID: " << pCertAltNameEntry.pszRegisteredID;
							break;
						}

					}
					cout << endl << "	Authority Key Serial Number: ";
					for (int j = pCryptDataBlob->AuthorityCertSerialNumber.cbData - 1; j >= 0; --j) {
						printf("%02X", pCryptDataBlob->AuthorityCertSerialNumber.pbData[j]);
					}

				}
			}
			else if (cert_str == szOID_KEY_USAGE) {
				// cout << "cert_extension oid: " << cert_extension.pszObjId << endl;
				if (CryptDecodeObjectEx(MY_TYPE, X509_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {
					if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
					if (CryptDecodeObjectEx(MY_TYPE, X509_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
						PCRYPT_BIT_BLOB pCryptBitBlob = (PCRYPT_BIT_BLOB)pbDecoded;
						cout << "Extension KeyUsage: ";
						if (cert_extension.fCritical) cout << "Critical, ";
						for (int i = 0; i < pCryptBitBlob->cbData; ++i) {
							printf("%02X ", pCryptBitBlob->pbData[i]);
							unsigned char flag = pCryptBitBlob->pbData[i];
							if (int(flag) == 0x80) keyUsage += "signature";
							if (int(flag) == 0x10) keyUsage += "encryption";
							if (int(flag) == 0x20) keyUsage += "envelope";
						}
					}
				}
				else { MyCertHandleError("KeyUsage Decode extension failed.\n"); }
			}
			else if (cert_str == szOID_ENHANCED_KEY_USAGE) {
				// cout << "cert_extension_enhanced_key_usage oid: " << cert_extension.pszObjId << endl;
				if (CryptDecodeObjectEx(MY_TYPE, X509_ENHANCED_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, NULL, &cbDecoded)) {
					if (!(pbDecoded = (BYTE*)malloc(cbDecoded))) { MyCertHandleError("Decode buffer memory allocation failed."); }
					if (CryptDecodeObjectEx(MY_TYPE, X509_ENHANCED_KEY_USAGE, cert_extension.Value.pbData, cert_extension.Value.cbData, NULL, NULL, pbDecoded, &cbDecoded)) {
						PCERT_ENHKEY_USAGE pCertEnhkeyUsage = (PCERT_ENHKEY_USAGE)pbDecoded;
						cout << "Extension Enhanced KeyUsage: ";
						if (cert_extension.fCritical) cout << "Critical, ";
						for (int i = 0; i < pCertEnhkeyUsage->cUsageIdentifier; ++i) {
							string enhanced_keyUsage = pCertEnhkeyUsage->rgpszUsageIdentifier[i];
							if (enhanced_keyUsage == szOID_PKIX_KP) cout << "PKIX_KP ";
							if (enhanced_keyUsage == szOID_PKIX_KP_SERVER_AUTH) cout << "Server Auth ";
							if (enhanced_keyUsage == szOID_PKIX_KP_CLIENT_AUTH) cout << "Client Auth ";
							if (enhanced_keyUsage == szOID_PKIX_KP_CODE_SIGNING) cout << "Code Signing ";
							if (enhanced_keyUsage == szOID_PKIX_KP_EMAIL_PROTECTION) cout << "Email Protection ";
							if (enhanced_keyUsage == szOID_PKIX_KP_IPSEC_END_SYSTEM) cout << "IPSEC End System ";
							if (enhanced_keyUsage == szOID_PKIX_KP_IPSEC_TUNNEL) cout << "IPSEC Tunnel ";
							if (enhanced_keyUsage == szOID_PKIX_KP_IPSEC_USER) cout << "IPSEC User ";
							if (enhanced_keyUsage == szOID_PKIX_KP_TIMESTAMP_SIGNING) cout << "Time Stamping ";
							if (enhanced_keyUsage == szOID_PKIX_KP_OCSP_SIGNING) cout << "OCSP Signing ";
							if (enhanced_keyUsage == szOID_PKIX_OCSP_NOCHECK) cout << "OCSP Nochecking ";
							if (enhanced_keyUsage == szOID_PKIX_OCSP_NONCE) cout << "OCSP Nonce ";
							if (enhanced_keyUsage == szOID_IPSEC_KP_IKE_INTERMEDIATE) cout << "Internet Key Exchange ";
							if (enhanced_keyUsage == szOID_PKINIT_KP_KDC) cout << "KDC ";
						}
						cout << endl;
					}
				}
				else { MyCertHandleError("EnhancedKeyUsage Decode extension failed.\n"); }
			}
			else continue;
			cout << endl;
		}

		//CertificateIS(const string certDN, const string issuerDN, const mpz_t certSN, const SYSTEMTIME validBegin, const SYSTEMTIME validEnd, const string CertStore, const string KeyUsage, const string CertType);
		
		CertificateIS cert = CertificateIS(certDN,issuerDN,certSN,validBegin,validEnd,certStore,keyUsage,certType);
		string certJson;
		cert.toJson(certJson);
		cout << certJson;
		if(0==keySpec) certsList_vector.push_back(certJson);
		else if (2 == keySpec) { if (cert.KeyUsage == "signature") certsList_vector.push_back(certJson); }
		ostreamTmp.str("");
		keyUsage.clear();
		//mpz_clear(certSN);
		mpz_clear(result);
		delete(pst);
		cout << endl << "=================================================================================" << endl;
		
	}
	//-------------------------------------------------------------------
	// Clean up.
	CertFreeCertificateContext(pCertContext);
	CertCloseStore(hCertStore, 0);

	printf("The function completed successfully. \n");
	
	string object_arr_toJS  = "[";
	for (string s : certsList_vector) {
		object_arr_toJS += s;
		object_arr_toJS += ",";
	}
	if (certsList_vector.size() > 0) object_arr_toJS.replace(object_arr_toJS.end() - 1, object_arr_toJS.end(), "]");
	else object_arr_toJS += "]";
	cout << object_arr_toJS<<endl;

	return object_arr_toJS;
}

//-------------------------------------------------------------------
// Begin definition of the SignAndEncrypt function.

BYTE* SignAndEncryptIS(const BYTE* pbToBeSignedAndEncrypted, DWORD cbToBeSignedAndEncrypted, DWORD* pcbSignedAndEncryptedBlob, LPCSTR signerSubject, LPCSTR signerCertStore, LPCSTR recipientSubject, LPCSTR recipientCertStore)
{
	
	//---------------------------------------------------------------
	// Declare and initialize local variables.

	HCERTSTORE hSignerCertStore, hRecipientCertStore;

	//---------------------------------------------------------------
	// pSignerCertContext will be the certificate of 
	// the message signer.

	PCCERT_CONTEXT pSignerCertContext = NULL;

	//---------------------------------------------------------------
	// pRecipientCertContext will be the certificate of the 
	// message receiver.

	PCCERT_CONTEXT pRecipientCertContext = NULL;
	
	TCHAR pszNameString[256];
	CRYPT_SIGN_MESSAGE_PARA SignPara;
	CRYPT_ENCRYPT_MESSAGE_PARA EncryptPara;
	DWORD cRecipientCert=0;
	PCCERT_CONTEXT rgpRecipientCert[5];
	BYTE* pbSignedAndEncryptedBlob = NULL;
	CERT_NAME_BLOB Subject_Blob;
	BYTE* pbDataIn;
	DWORD dwKeySpec;
	HCRYPTPROV hCryptProv;

	CRYPT_ALGORITHM_IDENTIFIER EncryptAlgorithm;
	BYTE* pbEncryptedBlob;
	DWORD    cbEncryptedBlob;

	DWORD EncryptParamsSize = sizeof(EncryptPara);
	DWORD EncryptAlgSize = sizeof(EncryptAlgorithm);

	//-------------------------------------------------------------------
	// Get a handle to a cryptographic provider.
	if (CryptAcquireContext(
		&hCryptProv,        // Address for handle to be returned.
		NULL,               // Use the current user's logon name.
		NULL,               // Use the default provider.
		PROV_RSA_FULL,      // Need to both encrypt and sign.
		NULL))              // No flags needed.
	{
		printf("A CSP has been acquired. \n");
	}
	else
	{
		MyCertHandleError("Cryptographic context could not be acquired.");
	}

	// Open a system certificate store.
	if (hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore)) { fprintf(stderr, "The %s store has been opened. \n", signerCertStore); }
	else { MyCertHandleError("The store was not opened."); }

	//---------------------------------------------------------------
	// Get the certificate for the signer.

	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, signerSubject, -1, NULL, 0);
	LPWSTR lpSigner = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, signerSubject, -1, lpSigner, wchars_num);

	if (!(pSignerCertContext = CertFindCertificateInStore(
		hSignerCertStore,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		lpSigner,
		NULL))) {
		MyCertHandleError( "Cert not found.\n");
	}
	else {
		//---------------------------------------------------------------
	// Get and print the name of the message signer.
	// The following two calls to CertGetNameString with different
	// values for the second parameter get two different forms of 
	// the certificate subject's name.
		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
				TEXT("%s \n"),
				pszNameString);
		}
		else{MyCertHandleError(TEXT("Getting the name of the signer failed.\n"));}

		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_RDN_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The RDM_TYPE message signer's name is %s \n"),
				pszNameString);
		}
		else{MyCertHandleError(TEXT("Getting the name of the signer failed.\n"));}
		if (!(CryptAcquireCertificatePrivateKey(
			pSignerCertContext,
			0,
			NULL,
			&hCryptProv,
			&dwKeySpec,
			NULL))){MyCertHandleError(TEXT("CryptAcquireCertificatePrivateKey.\n"));}
		else {
			cout << "Acquire Private KeyUsage Success: " << dwKeySpec << endl;
		}
	}

	// Open a system certificate store.
	if (hRecipientCertStore = CertOpenSystemStore(NULL, recipientCertStore)) { fprintf(stderr, "The %s store has been opened. \n", recipientCertStore); }
	else { MyCertHandleError("The store was not opened."); }

	wchars_num = MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, NULL, 0);
	LPWSTR lpRecipient = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, lpRecipient, wchars_num);

	if (!(pRecipientCertContext = CertFindCertificateInStore(
		hRecipientCertStore,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		lpRecipient,
		NULL)))
	{
		MyCertHandleError(TEXT("Receiver certificate not found."));
	}
	else {
		if (CertGetNameString(
			pRecipientCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
				TEXT("%s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		if (CertGetNameString(
			pRecipientCertContext,
			CERT_NAME_RDN_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The RDM_TYPE message signer's name is %s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
		// Create a RecipientCertArray.
		rgpRecipientCert[0] = pRecipientCertContext;
		cRecipientCert += 1;
	}

	//---------------------------------------------------------------
	// Initialize variables and data structures
	// for the call to CryptSignAndEncryptMessage.

	SignPara.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
	SignPara.dwMsgEncodingType = MY_TYPE;
	SignPara.pSigningCert = pSignerCertContext;
	string tmpSign = szOID_RSA_SHA1RSA; // const -> non const
	SignPara.HashAlgorithm.pszObjId = (LPSTR)tmpSign.c_str();
	SignPara.HashAlgorithm.Parameters.cbData = 0;
	SignPara.pvHashAuxInfo = NULL;
	SignPara.cMsgCert = 1;
	SignPara.rgpMsgCert = &pSignerCertContext;
	SignPara.cMsgCrl = 0;
	SignPara.rgpMsgCrl = NULL;
	SignPara.cAuthAttr = 0;
	SignPara.rgAuthAttr = NULL;
	SignPara.cUnauthAttr = 0;
	SignPara.rgUnauthAttr = NULL;
	SignPara.dwFlags = 0;
	SignPara.dwInnerContentType = 0;

	memset(&EncryptPara, 0, EncryptParamsSize);
	EncryptPara.cbSize = EncryptParamsSize;
	EncryptPara.dwMsgEncodingType = MY_TYPE;
	EncryptPara.hCryptProv = hCryptProv;
	EncryptAlgSize = sizeof(EncryptAlgorithm);
	memset(&EncryptAlgorithm, 0, EncryptAlgSize);
	string tmpEnc = szOID_RSA_RC2CBC;
	EncryptAlgorithm.pszObjId = (LPSTR)tmpEnc.c_str();
	EncryptAlgorithm.Parameters.cbData = 0;
	EncryptPara.ContentEncryptionAlgorithm = EncryptAlgorithm;
	EncryptPara.pvEncryptionAuxInfo = NULL;
	
	if (CryptSignAndEncryptMessage(
		&SignPara,
		&EncryptPara,
		cRecipientCert,
		rgpRecipientCert,
		pbToBeSignedAndEncrypted,
		cbToBeSignedAndEncrypted,
		NULL, // the pbSignedAndEncryptedBlob
		pcbSignedAndEncryptedBlob))
	{
		_tprintf(TEXT("%d bytes for the buffer .\n"),
			*pcbSignedAndEncryptedBlob);
	}
	else { cout << unsigned long long(GetLastError()) << endl; MyCertHandleError(TEXT("Getting the buffer length failed.")); }
	
	//---------------------------------------------------------------
   // Allocate memory for the buffer.

	if (!(pbSignedAndEncryptedBlob =
		(unsigned char*)malloc(*pcbSignedAndEncryptedBlob)))
	{
		MyCertHandleError(TEXT("Memory allocation failed."));
	}

	//---------------------------------------------------------------
	// Call the function a second time to copy the signed and 
	// encrypted message into the buffer.

	if (CryptSignAndEncryptMessage(
		&SignPara,
		&EncryptPara,
		cRecipientCert,
		rgpRecipientCert,
		pbToBeSignedAndEncrypted,
		cbToBeSignedAndEncrypted,
		pbSignedAndEncryptedBlob,
		pcbSignedAndEncryptedBlob))
	{
		_tprintf(TEXT("The message is signed and encrypted.\n"));
	}
	else
	{
		MyCertHandleError(
			TEXT("The message failed to sign and encrypt."));
	}

	// Call CryptEncryptMessage.
	/*
	if (CryptEncryptMessage(
		&EncryptPara,
		cRecipientCert,
		rgpRecipientCert,
		pbToBeSignedAndEncrypted,
		cbToBeSignedAndEncrypted,
		NULL,
		pcbSignedAndEncryptedBlob))
	{
		printf("The encrypted message is %d bytes. \n", pcbSignedAndEncryptedBlob);
	}
	else
	{
		cout << GetLastError() << endl;
		MyCertHandleError("Getting EncryptedBlob size failed.");
	}
	


	// First, get the size of the signed BLOB.
	if (CryptSignMessage(
		&SignPara,
		FALSE,
		1,
		&pbToBeSignedAndEncrypted,
		&cbToBeSignedAndEncrypted,
		NULL,
		pcbSignedAndEncryptedBlob))
	{
		_tprintf(TEXT("%d bytes needed for the encoded BLOB.\n"),
			pcbSignedAndEncryptedBlob);
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}*/

	delete[] lpSigner,lpRecipient;
	cout << "==================" << endl;
	return pbSignedAndEncryptedBlob;

}

BYTE* EncryptIS(const BYTE* pbToBeEncrypted, DWORD cbToBeEncrypted, DWORD* pcbEncryptedBlob, LPCSTR recipientSubject, LPCSTR certStore) {
	HCRYPTPROV hCryptProv;                      // CSP handle
	HCERTSTORE hStoreHandle;
	PCCERT_CONTEXT pRecipientCert;
	PCCERT_CONTEXT RecipientCertArray[1];
	DWORD EncryptAlgSize;
	CRYPT_ALGORITHM_IDENTIFIER EncryptAlgorithm;
	CRYPT_ENCRYPT_MESSAGE_PARA EncryptParams;
	DWORD EncryptParamsSize;
	BYTE* pbEncryptedBlob;
	DWORD    cbEncryptedBlob;

	//-------------------------------------------------------------------
	//  Begin processing.

	printf("About to begin with the message %s.\n", pbToBeEncrypted);
	printf("The message length is %d bytes. \n", cbToBeEncrypted);

	//-------------------------------------------------------------------
	// Get a handle to a cryptographic provider.

	if (CryptAcquireContext(
		&hCryptProv,        // Address for handle to be returned.
		NULL,               // Use the current user's logon name.
		NULL,               // Use the default provider.
		PROV_RSA_FULL,      // Need to both encrypt and sign.
		NULL))              // No flags needed.
	{
		printf("A CSP has been acquired. \n");
	}
	else
	{
		MyCertHandleError("Cryptographic context could not be acquired.");
	}
	//-------------------------------------------------------------------
	// Open a system certificate store.

	if (hStoreHandle = CertOpenSystemStore(
		hCryptProv,
		certStore))
	{
		printf("The %s store is open. \n", certStore);
	}
	else
	{
		MyCertHandleError("Error getting store handle.");
	}

	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, NULL, 0);
	LPWSTR lp = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, lp, wchars_num);

	if (!(pRecipientCert = CertFindCertificateInStore(
		hStoreHandle,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		//L"FR, Ldf, Infosec, Dpt_Buz, Encrypt",only have pub key ne marche pas ?
	   // L"FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com",// has to use private key
		//L"FR, Ldf, Paris, Infosec, Dpt_Buz, User Client Encryption Infosec, enc@infosec.com",
		lp,
		NULL)))
	{
		MyCertHandleError(TEXT("Receiver certificate not found."));
	}

	//-------------------------------------------------------------------
   // Create a RecipientCertArray.

	RecipientCertArray[0] = pRecipientCert;

	//-------------------------------------------------------------------
	// Initialize the algorithm identifier structure.

	EncryptAlgSize = sizeof(EncryptAlgorithm);

	//-------------------------------------------------------------------
	// Initialize the structure to zero.

	memset(&EncryptAlgorithm, 0, EncryptAlgSize);

	//-------------------------------------------------------------------
	// Set the necessary member.

	string tmp = szOID_RSA_RC2CBC;
	EncryptAlgorithm.pszObjId = (LPSTR)tmp.c_str();

	//-------------------------------------------------------------------
	// Initialize the CRYPT_ENCRYPT_MESSAGE_PARA structure. 

	EncryptParamsSize = sizeof(EncryptParams);
	memset(&EncryptParams, 0, EncryptParamsSize);
	EncryptParams.cbSize = EncryptParamsSize;
	EncryptParams.dwMsgEncodingType = MY_TYPE;
	EncryptParams.hCryptProv = hCryptProv;
	EncryptParams.ContentEncryptionAlgorithm = EncryptAlgorithm;

	//-------------------------------------------------------------------
	// Call CryptEncryptMessage.

	if (CryptEncryptMessage(
		&EncryptParams,
		1,
		RecipientCertArray,
		pbToBeEncrypted,
		cbToBeEncrypted,
		NULL,
		&cbEncryptedBlob))
	{
		printf("The encrypted message is %d bytes. \n", cbEncryptedBlob);
	}
	else
	{
		cout << GetLastError() << endl;
		MyCertHandleError("Getting EncryptedBlob size failed.");
	}
	//-------------------------------------------------------------------
	// Allocate memory for the returned BLOB.

	if (pbEncryptedBlob = (BYTE*)malloc(cbEncryptedBlob))
	{
		printf("Memory has been allocated for the encrypted BLOB. \n");
	}
	else
	{
		MyCertHandleError("Memory allocation error while encrypting.");
	}
	//-------------------------------------------------------------------
	// Call CryptEncryptMessage again to encrypt the content.

	if (CryptEncryptMessage(
		&EncryptParams,
		1,
		RecipientCertArray,
		pbToBeEncrypted,
		cbToBeEncrypted,
		pbEncryptedBlob,
		&cbEncryptedBlob))
	{
		printf("Encryption succeeded. \n");
	}
	else
	{
		MyCertHandleError("Encryption failed.");
	}

	cout << "==================" << endl;

	return pbEncryptedBlob;

}

BOOL SignIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, BYTE*& pbSignedBlob, DWORD& cbSignedBlob, const string& signerSubject, const BOOL isKeySpec, const BOOL isDetached, const string& digestArithmetic, const string& signerCertStore) {
	//---------------------------------------------------------------
	// Declare and initialize local variables.

	HCERTSTORE hSignerCertStore;

	//---------------------------------------------------------------
	// pSignerCertContext will be the certificate of 
	// the message signer.

	PCCERT_CONTEXT pSignerCertContext = NULL, pPreSignerCertContext = NULL;
	TCHAR pszNameString[256];
	CRYPT_SIGN_MESSAGE_PARA SignPara;
	DWORD dwKeySpec;
	HCRYPTPROV hCryptProv;

	//-------------------------------------------------------------------
	// Get a handle to a cryptographic provider.
	if (CryptAcquireContext(
		&hCryptProv,        // Address for handle to be returned.
		NULL,               // Use the current user's logon name.
		NULL,               // Use the default provider.
		PROV_RSA_FULL,      // Need to both encrypt and sign.
		NULL))              // No flags needed.
	{
		printf("A CSP has been acquired. \n");
	}
	else
	{
		MyCertHandleError("Cryptographic context could not be acquired.");
	}

	// Open a system certificate store.
	if (hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore.c_str())) { fprintf(stderr, "The %s store has been opened. \n", signerCertStore.c_str()); }
	else { MyCertHandleError("The store was not opened."); }

	//---------------------------------------------------------------
	// Get the certificate for the signer.

	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, signerSubject.c_str(), -1, NULL, 0);
	LPWSTR lpSigner = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, signerSubject.c_str(), -1, lpSigner, wchars_num);

	while ((pSignerCertContext = CertFindCertificateInStore(
		hSignerCertStore,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		lpSigner,
		pPreSignerCertContext)))
	{
		pPreSignerCertContext = pSignerCertContext;
		//---------------------------------------------------------------
		// Get and print the name of the message signer.
		// The following two calls to CertGetNameString with different
		// values for the second parameter get two different forms of 
		// the certificate subject's name.
		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
				TEXT("%s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_RDN_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The RDM_TYPE message signer's name is %s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
		if (!(CryptAcquireCertificatePrivateKey(
			pSignerCertContext,
			0,
			NULL,
			&hCryptProv,
			&dwKeySpec,
			NULL))) {
			MyCertHandleError(TEXT("CryptAcquireCertificatePrivateKey.\n"));
		}
		else {
			if (isKeySpec&&dwKeySpec != AT_SIGNATURE) {
				continue;
			}
			cout << "Acquire Private KeyUsage Success: " << dwKeySpec << endl;
			break;
		}
	}
	if (NULL == pSignerCertContext) MyCertHandleError("Cert not found or cert was filtered.");
	//---------------------------------------------------------------
	// Initialize variables and data structures
	// for the call to CryptSignMessage.

	string tmpSign;
	if (digestArithmetic == SHA256) {
		tmpSign = szOID_RSA_SHA256RSA; // const -> non const c -> c++
	}
	else if (digestArithmetic == SHA1) {
		tmpSign = szOID_RSA_SHA1RSA;
	}
	else {
		tmpSign = szOID_RSA_MD5RSA;
	}

	SignPara.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
	SignPara.dwMsgEncodingType = MY_TYPE;
	SignPara.pSigningCert = pSignerCertContext;
	SignPara.HashAlgorithm.pszObjId = (LPSTR)tmpSign.c_str();
	SignPara.HashAlgorithm.Parameters.cbData = 0;
	SignPara.pvHashAuxInfo = NULL;
	SignPara.cMsgCert = 1;
	SignPara.rgpMsgCert = &pSignerCertContext;
	SignPara.cMsgCrl = 0;
	SignPara.rgpMsgCrl = NULL;
	SignPara.cAuthAttr = 0;
	SignPara.rgAuthAttr = NULL;
	SignPara.cUnauthAttr = 0;
	SignPara.rgUnauthAttr = NULL;
	SignPara.dwFlags = 0;
	SignPara.dwInnerContentType = 0;

	if (CryptSignMessage(
		&SignPara,
		isDetached,
		1,
		&pbToBeSigned,
		const_cast<DWORD*>(&cbToBeSigned),
		NULL,
		&cbSignedBlob))
	{
		_tprintf(TEXT("%u bytes needed for the encoded BLOB.\n"),
			cbSignedBlob);
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}

	if (!(pbSignedBlob =
		(unsigned char*)malloc(cbSignedBlob)))
	{
		MyCertHandleError(TEXT("Memory allocation failed."));
	}

	if (CryptSignMessage(
		&SignPara,
		isDetached,
		1,
		&pbToBeSigned,
		const_cast<DWORD*>(&cbToBeSigned),
		pbSignedBlob,
		&cbSignedBlob))
	{
		_tprintf(TEXT("SignIS success.\n"));
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}

	delete[] lpSigner;

	cout << "==================" << endl;
	return TRUE;

}

BOOL SignIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, BYTE*& pbSignedBlob, DWORD& cbSignedBlob, const DWORD& signerCertIndex, const BOOL isKeySpec, const BOOL isDetached, const string& digestArithmetic, const string& signerCertStore) {
	//---------------------------------------------------------------
	// Declare and initialize local variables.
	HCERTSTORE hSignerCertStore;
	//---------------------------------------------------------------
	// pSignerCertContext will be the certificate of 
	// the message signer.

	PCCERT_CONTEXT pSignerCertContext = NULL;
	TCHAR pszNameString[256];
	CRYPT_SIGN_MESSAGE_PARA SignPara;
	DWORD dwKeySpec;
	HCRYPTPROV hCryptProv;

	//-------------------------------------------------------------------
	// Get a handle to a cryptographic provider.
	if (CryptAcquireContext(
		&hCryptProv,        // Address for handle to be returned.
		NULL,               // Use the current user's logon name.
		NULL,               // Use the default provider.
		PROV_RSA_FULL,      // Need to both encrypt and sign.
		NULL))              // No flags needed.
	{
		printf("A CSP has been acquired. \n");
	}
	else
	{
		MyCertHandleError("Cryptographic context could not be acquired.");
	}

	// Open a system certificate store.
	if (hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore.c_str())) { fprintf(stderr, "The %s store has been opened. \n", signerCertStore.c_str()); }
	else { MyCertHandleError("The store was not opened."); }

	//---------------------------------------------------------------
	// Get the certificate for the signer.
	DWORD indexCert = 0;
	while (pSignerCertContext = CertEnumCertificatesInStore(hSignerCertStore,pSignerCertContext)) {
		if (indexCert++ == signerCertIndex) {
			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_SIMPLE_DISPLAY_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
					TEXT("%s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_RDN_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The RDM_TYPE message signer's name is %s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
			if (!(CryptAcquireCertificatePrivateKey(
				pSignerCertContext,
				0,
				NULL,
				&hCryptProv,
				&dwKeySpec,
				NULL))) {
				MyCertHandleError(TEXT("CryptAcquireCertificatePrivateKey.\n"));
			}
			else {
				if (isKeySpec && dwKeySpec != AT_SIGNATURE) MyCertHandleError("[x] Key is filtered.");
				cout << "Acquire Private KeyUsage Success: " << dwKeySpec << endl;
			}
			break;
		}
	}		

	//---------------------------------------------------------------
	// Initialize variables and data structures
	// for the call to CryptSignMessage.
	string tmpSign;
	if (digestArithmetic == SHA256) {
		tmpSign = szOID_RSA_SHA256RSA;
	}
	else if (digestArithmetic == SHA1) {
		tmpSign = szOID_RSA_SHA1RSA;
	}
	else {
		tmpSign = szOID_RSA_MD5RSA;
	}

	SignPara.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
	SignPara.dwMsgEncodingType = MY_TYPE;
	SignPara.pSigningCert = pSignerCertContext;
	 // const -> non const
	SignPara.HashAlgorithm.pszObjId = (LPSTR)tmpSign.c_str();
	SignPara.HashAlgorithm.Parameters.cbData = 0;
	SignPara.pvHashAuxInfo = NULL;
	SignPara.cMsgCert = 1;
	SignPara.rgpMsgCert = &pSignerCertContext;
	SignPara.cMsgCrl = 0;
	SignPara.rgpMsgCrl = NULL;
	SignPara.cAuthAttr = 0;
	SignPara.rgAuthAttr = NULL;
	SignPara.cUnauthAttr = 0;
	SignPara.rgUnauthAttr = NULL;
	SignPara.dwFlags = 0;
	SignPara.dwInnerContentType = 0;

	if (CryptSignMessage(
		&SignPara,
		isDetached,
		1,
		&pbToBeSigned,
		const_cast<DWORD*>(&cbToBeSigned),
		NULL,
		&cbSignedBlob))
	{
		_tprintf(TEXT("%u bytes needed for the encoded BLOB.\n"),
			&cbSignedBlob);
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}

	if (!(pbSignedBlob =
		(unsigned char*)malloc(cbSignedBlob)))
	{
		MyCertHandleError(TEXT("Memory allocation failed."));
	}

	if (CryptSignMessage(
		&SignPara,
		isDetached,
		1,
		&pbToBeSigned,
		const_cast<DWORD*>(&cbToBeSigned),
		pbSignedBlob,
		&cbSignedBlob))
	{
		_tprintf(TEXT("SignByCertIndexIS success.\n"));
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}
	cout << "==================" << endl;
	return TRUE;

}

BYTE* SignWithCertIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, DWORD*& pcbSignedBlob, const DWORD& signerCertIndex, const bool isKeySpec, const bool isDetached, const string& digestArithmetic, const string& signerCertStore) {
	//---------------------------------------------------------------
	// Declare and initialize local variables.
	HCERTSTORE hSignerCertStore;
	//---------------------------------------------------------------
	// pSignerCertContext will be the certificate of 
	// the message signer.

	PCCERT_CONTEXT pSignerCertContext = NULL;
	TCHAR pszNameString[256];
	CRYPT_SIGN_MESSAGE_PARA SignPara;
	BYTE* pbSignedBlob = NULL;
	DWORD dwKeySpec;
	HCRYPTPROV hCryptProv;

	//-------------------------------------------------------------------
	// Get a handle to a cryptographic provider.
	if (CryptAcquireContext(
		&hCryptProv,        // Address for handle to be returned.
		NULL,               // Use the current user's logon name.
		NULL,               // Use the default provider.
		PROV_RSA_FULL,      // Need to both encrypt and sign.
		NULL))              // No flags needed.
	{
		printf("A CSP has been acquired. \n");
	}
	else
	{
		MyCertHandleError("Cryptographic context could not be acquired.");
	}

	// Open a system certificate store.
	if (hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore.c_str())) { fprintf(stderr, "The %s store has been opened. \n", signerCertStore.c_str()); }
	else { MyCertHandleError("The store was not opened."); }

	//---------------------------------------------------------------
	// Get the certificate for the signer.
	int indexCert = 0;
	while (pSignerCertContext = CertEnumCertificatesInStore(hSignerCertStore, pSignerCertContext)) {
		if (indexCert++ == signerCertIndex) {
			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_SIMPLE_DISPLAY_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
					TEXT("%s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_RDN_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The RDM_TYPE message signer's name is %s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
			if (!(CryptAcquireCertificatePrivateKey(
				pSignerCertContext,
				0,
				NULL,
				&hCryptProv,
				&dwKeySpec,
				NULL))) {
				MyCertHandleError(TEXT("CryptAcquireCertificatePrivateKey.\n"));
			}
			else {
				if (isKeySpec && dwKeySpec != AT_SIGNATURE) MyCertHandleError("[x] Key is filtered.");
				cout << "Acquire Private KeyUsage Success: " << dwKeySpec << endl;
			}
			break;
		}
	}

	//---------------------------------------------------------------
	// Initialize variables and data structures
	// for the call to CryptSignMessage.
	string tmpSign;
	if (digestArithmetic == SHA256) {
		tmpSign = szOID_RSA_SHA256RSA;
	}
	else if (digestArithmetic == SHA1) {
		tmpSign = szOID_RSA_SHA1RSA;
	}
	else {
		tmpSign = szOID_RSA_MD5RSA;
	}

	SignPara.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
	SignPara.dwMsgEncodingType = MY_TYPE;
	SignPara.pSigningCert = pSignerCertContext;
	// const -> non const
	SignPara.HashAlgorithm.pszObjId = (LPSTR)tmpSign.c_str();
	SignPara.HashAlgorithm.Parameters.cbData = 0;
	SignPara.pvHashAuxInfo = NULL;
	SignPara.cMsgCert = 1;
	SignPara.rgpMsgCert = &pSignerCertContext;
	SignPara.cMsgCrl = 0;
	SignPara.rgpMsgCrl = NULL;
	SignPara.cAuthAttr = 0;
	SignPara.rgAuthAttr = NULL;
	SignPara.cUnauthAttr = 0;
	SignPara.rgUnauthAttr = NULL;
	SignPara.dwFlags = 0;
	SignPara.dwInnerContentType = 0;

	if (CryptSignMessage(
		&SignPara,
		isDetached,
		1,
		&pbToBeSigned,
		const_cast<DWORD*>(&cbToBeSigned),
		NULL,
		pcbSignedBlob))
	{
		_tprintf(TEXT("%u bytes needed for the encoded BLOB.\n"),
			pcbSignedBlob);
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}

	if (!(pbSignedBlob =
		(unsigned char*)malloc(*pcbSignedBlob)))
	{
		MyCertHandleError(TEXT("Memory allocation failed."));
	}

	if (CryptSignMessage(
		&SignPara,
		isDetached,
		1,
		&pbToBeSigned,
		const_cast<DWORD*>(&cbToBeSigned),
		pbSignedBlob,
		pcbSignedBlob))
	{
		_tprintf(TEXT("SignByCertIndexIS success.\n"),
			pcbSignedBlob);
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}
	cout << "==================" << endl;
	return pbSignedBlob;

}

/*
IWSAGetVersion(SucceedFunction)
3.2.1.2参数
参数名称	类  型	参数描述
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
*/
string IWSAGetVersion() {
	return VERSION;
}

/*
IWSASetTimeOut (TimeOut)
3.2.2.2参数
参数名称	类  型	参数描述
TimeOut	整数	设置的时间 (ms)，单位毫秒
3.2.2.3回调函数参数
无
3.2.2.4功能说明
设置AJAX 通讯的超时时间，不是后台服务的，是前端JS文件中的，默认为60000毫秒。
*/
void IWSASetTimeOut() {
	//à réaliser
}

/*
IWSASetSM2IssuerCert(SM2IssuerCert)
3.2.3.2参数
参数名称	类  型	参数描述
SM2IssuerCert	字符串	BASE64 格式的国密证书

3.2.3.3回调函数参数
无
3.2.3.4功能说明
如果设置国密证书的颁发者证书，base64格式，供国密签名验证时使用。否则验证国密签名时不验证颁发者证书。
*/
void IWSASetSM2IssuerCert(string SM2IssuerCert) {
	//à réaliser
}

/*
IWSABase64Encode(PlainText, SucceedFunction)
3.2.4.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.4.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
TextData	字符串	编码后的数据
3.2.4.4功能说明
对输入原文进行Base64编码。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
*/

void IWSABase64EncodeIS(DWORD* errorCode, string& textData, const BYTE* plainText, const DWORD& plainTextSize) {
	LPSTR pszString;
	DWORD pcchString;
	if (CryptBinaryToString(plainText, plainTextSize, CRYPT_STRING_BASE64, nullptr, &pcchString)) {
		pszString = (LPSTR) malloc(pcchString);
		if (CryptBinaryToString(plainText, plainTextSize, CRYPT_STRING_BASE64, pszString, &pcchString)) {
			textData = string(pszString,pcchString);
			*errorCode = 0;
		}
	}
	else {
		MyCertHandleError("Base64Encode size calculation failed.");
	}
}

/*
IWSABase64Decode (PlainText,SucceedFunction)
3.2.5.2参数
参数名称	类  型	参数描述
PlainText	字符串	Base64编码后的原文
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.5.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
TextData	字符串	解码后的数据
对输入的数据进行Base64格式的解码。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
*/
void IWSABase64DecodeIS(DWORD* errorCode, BYTE*& textData, DWORD& cbBinary, const string& plainText) {
	if(CryptStringToBinary(plainText.c_str(), plainText.size(), CRYPT_STRING_BASE64, NULL, &cbBinary, NULL, NULL)){
		textData = (BYTE*)malloc(cbBinary+1);
		textData[cbBinary] = '\0';
		if (CryptStringToBinary(plainText.c_str(), plainText.size(), CRYPT_STRING_BASE64, textData, &cbBinary, NULL, NULL)) {
			*errorCode = 0;
		}
	}
	else {
		MyCertHandleError("Base64Decode size calculation failed.");
	}
}

/*
IWSAHashAndBase64Encode (PlainText,DigestArithmetic,SucceedFunction)
3.2.6.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.6.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
TextData	字符串	原文用指定算法摘要产生的值，做BASE64编码
3.2.6.4功能说明
对输入的原文做指定的摘要，然后对摘要做Base64编码输出。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/
void IWSAHashAndBase64Encode() {

}

/*
3.2.7（方法）Detached方式签名
3.2.7.1名称
IWSADetachedSign(PortGrade, PlainText, CertIndex, DigestArithmetic, SucceedFunction)
3.2.7.2参数
参数名称	类  型	参数描述
PortGrade	整数	1为一代签名接口，2为 二代签名接口
PlainText	字符串	原文
CertIndex	整数	证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.7.3回调函数参数
参数名称	类 型	返回值说明
errorCode	整数	错误码
signedData	字符串	BASE64编码后的签名
3.2.7.4功能说明
对输入的原文做detached方式签名，再做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/

void IWSADetachedSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& certIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore) {
	DWORD cbSignedBlob;
	BYTE* pbSignedBlob;
	if (SignIS(plainText, plainTextSize, pbSignedBlob, cbSignedBlob, certIndex, isKeySpec, TRUE, digestArithmetic, signerCertStore)) {
		IWSABase64EncodeIS(errorCode, signedData, pbSignedBlob, cbSignedBlob);
	}
}

/*
3.2.8（方法）Detached方式使用默认DN签名一代KEY
3.2.8.1名称
IWSADetachedSignDefaultDN
(PlainText,DefaultDN,IsKeySpec,DigestArithmetic,SucceedFunction)
3.2.8.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
DefaultDN	字符串	默认证书DN
IsKeySpec	字符串	是否按秘钥用法过滤，“1”为过滤，“0”不过滤。
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.8.3回调函数参数
参数名称	类 型	返回值说明
errorCode	整数	错误码
signedData	字符串	BASE64编码后的签名
3.2.8.4功能说明
对输入的原文做detached方式签名，再做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/
void IWSADetachedSignDefaultDNIS(DWORD*& errorCode, string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const string& defaultDN, const boolean isKeySpec, const string& digestArithmetic, const string& signerCertStore) {
	DWORD cbSignedBlob;
	BYTE* pbSignedBlob;
	if (SignIS(plainText, plainTextSize, pbSignedBlob, cbSignedBlob, defaultDN, isKeySpec, TRUE, digestArithmetic, signerCertStore)) {
		IWSABase64EncodeIS(errorCode, signedData, pbSignedBlob, cbSignedBlob);
	}
}

/*
IWSADetachedSignAdvDefaultDN (PlainText,CertStoreSM2,CertStoreType,DefaultDN,IsKeySpec,DigestArithmetic,SucceedFunction)
3.2.9.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
CertStoreSM2	字符串	国密证书存储区
CertStoreType	整数	1:强制RSA, 2:强制SM2, 3:优先RSA, 4:优先SM2
DefaultDN	字符串	默认证书DN
IsKeySpec	字符串	是否按秘钥用法过滤，“1”为过滤，“0”不过滤。
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.9.3回调函数参数
参数名称	类 型	返回值说明
errorCode	整数	错误码
signedData	字符串	BASE64编码后的签名
3.2.9.4功能说明
对输入的原文做detached方式签名，再做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/

/*
3.2.10（方法）Detached方式验签名
3.2.10.1名称
IWSADetachedVerify(PortGrade, signedMsg, PlainText, SucceedFunction)
3.2.10.2参数
参数名称	类  型	参数描述
PortGrade	整数	1为一代验签接口，2为 二代验签接口
signedMsg	字符串	Base64编码的签名
PlainText	字符串	原文
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.10.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
3.2.10.4功能说明
对输入的签名先作Base64解码，然后解开哈希值与原文产生的哈希值比对检查，如果一致，表明成功，然后开始校验证书的签名、有效期、证书链的有效性以及CRL。如果上述操作都成功，则errorCode为0，否则设置errorCode为对应的错误信息。
用户可以通过查看errorCode错误码表，对照错误码表得到详细错误信息。
*/

void IWSADetachedVerifyIS(DWORD*& errorCode, const string& portGrade, const string& signedData, const BYTE*& plainText, const DWORD& plainTextSize) {
	CRYPT_VERIFY_MESSAGE_PARA verifyPara;
	BYTE* textData;
	PCCERT_CONTEXT pSignerCertContext = NULL;
	PCERT_INFO pSignerCertInfo = NULL;
	DWORD cbSignerCertInfo;
	HCERTSTORE hSignerCertStore;

	TCHAR pszNameString[256];
	DWORD dwKeySpec;
	HCRYPTPROV hCryptProv;

	BYTE* pbDecoded;
	DWORD cbBinary;

	DWORD indexCert = 0;

	HCRYPTMSG hMsg;

	LPCSTR signerCertStore = MY;

	// Initialize the VerifyParams data structure.
	verifyPara.cbSize = sizeof(CRYPT_VERIFY_MESSAGE_PARA);
	verifyPara.dwMsgAndCertEncodingType = MY_TYPE;
	verifyPara.hCryptProv = NULL;
	verifyPara.pfnGetSignerCertificate = NULL;
	verifyPara.pvGetArg = NULL;

	IWSABase64DecodeIS(errorCode, pbDecoded, cbBinary, signedData);

	//---------------------------------------------------------------
	// Get the certificate for the signer.
	//---------------------------------------------------------------
	//  Open a message for decoding.

	if (hMsg = CryptMsgOpenToDecode(MY_TYPE,      // encoding type
		0,                     // flags attached not release
		0,                     // use the default message type
							   // the message type is 
							   // listed in the message header
		NULL,                  // cryptographic provider 
							   // use NULL for the default provider
		NULL,                  // recipient information
		NULL))                 // stream information
	{
		printf("The message to decode is open. \n");
	}
	else
	{
		MyCertHandleError("OpenToDecode failed");
	}

	//---------------------------------------------------------------
	//  Update the message with an encoded BLOB.

	if (CryptMsgUpdate(
		hMsg,                 // handle to the message
		pbDecoded, // pointer to the encoded BLOB
		cbBinary, // size of the encoded BLOB
		TRUE))                // 
	{
		printf("The encoded BLOB has been added to the message. \n");
	}
	else
	{
		MyCertHandleError("Decode MsgUpdate failed");
	}

	//---------------------------------------------------------------
	// Get the size of memory required for the certificate.

	if (CryptMsgGetParam(
		hMsg,                         // handle to the message
		CMSG_SIGNER_CERT_INFO_PARAM,  // parameter type
		0,                            // index
		NULL,
		&cbSignerCertInfo))           // size of the returned 
									  // information
	{
		printf("%d bytes needed for the buffer.\n",
			cbSignerCertInfo);
	}
	else
	{
		MyCertHandleError("Verify SIGNER_CERT_INFO #1 failed.");
	}

	//---------------------------------------------------------------
	// Allocate memory.

	if (!(pSignerCertInfo = (PCERT_INFO)malloc(cbSignerCertInfo)))
	{
		MyCertHandleError("Verify memory allocation failed.");
	}

	//---------------------------------------------------------------
	// Get the message certificate information (CERT_INFO
	// structure).

	if (!(CryptMsgGetParam(
		hMsg,                         // handle to the message
		CMSG_SIGNER_CERT_INFO_PARAM,  // parameter type
		0,                            // index
		pSignerCertInfo,              // address for returned 
									  // information
		&cbSignerCertInfo)))          // size of the returned 
									  // information
	{
		MyCertHandleError("Verify SIGNER_CERT_INFO #2 failed");
	}

	//---------------------------------------------------------------
// Open a certificate store in memory using CERT_STORE_PROV_MSG,
// which initializes it with the certificates from the message.

	if (hSignerCertStore = CertOpenStore(
		CERT_STORE_PROV_MSG,         // store provider type 
		MY_TYPE,            // encoding type
		NULL,                        // cryptographic provider
									 // use NULL for the default
		0,                           // flags
		hMsg))                       // handle to the message
	{
		printf("The certificate store to be used for message " \
			"verification has been opened.\n");
	}
	else
	{
		MyCertHandleError("Verify open store failed");
	}

	//---------------------------------------------------------------
	// Find the signer's certificate in the store.

	if (pSignerCertContext = CertGetSubjectCertificateFromStore(
		hSignerCertStore,       // handle to the store
		MY_TYPE,   // encoding type
		pSignerCertInfo))   // pointer to retrieved CERT_CONTEXT
	{
		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_RDN_TYPE,
			0,
			NULL,
			pszNameString, // null-terminated string
			MAX_NAME) > 1)
		{
			printf("The message signer is  %s \n", pszNameString);
		}
		else
		{
			MyCertHandleError("Getting the signer's name failed.\n");
		}
	}
	else
	{
		MyCertHandleError("Verify GetSubjectCert failed");
	}

	DWORD* plainTextSizeNonConst = const_cast<DWORD*> (&plainTextSize);

	//dwSignerIndex not considered
	if (!CryptVerifyDetachedMessageSignature(&verifyPara, 0, pbDecoded, cbBinary, 1, &plainText, plainTextSizeNonConst, &pSignerCertContext)) {
		MyCertHandleError("[x] Detached Signature verify failed.");
	}
	*errorCode = 0;
}

/*
3.2.11（方法）Attached方式签名
3.2.11.1名称
IWSAAttachedSign(PortGrade, PlainText, CertIndex, DigestArithmetic, SucceedFunction)

3.2.11.2参数
参数名称	类  型	参数描述
PortGrade	整数	1为一代签名接口，2为 二代签名接口
PlainText	字符串	原文
CertIndex	整数	证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.11.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
signedData	字符串	Base64编码的签名包
3.2.11.4功能说明
对输入的原文做attached方式签名，将签名的结果根据大小判断是否压缩，再做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/

void IWSAAttachedSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& certIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore) {
	DWORD cbSignedBlob;
	BYTE* pbSignedBlob;
	if (SignIS(plainText, plainTextSize, pbSignedBlob, cbSignedBlob, certIndex, isKeySpec, FALSE, digestArithmetic, signerCertStore)) {
		IWSABase64EncodeIS(errorCode, signedData, pbSignedBlob, cbSignedBlob);
	}
}

/*
3.2.12（方法）Attached方式签名使用默认DN  一代KEY
3.2.12.1名称
IWSAAttachedSignDefaultDN(PlainText, DefaultDN, IsKeySpec, DigestArithmetic, SucceedFunction)

3.2.12.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
DefaultDN	字符串	默认证书DN
IsKeySpec	字符串	是否按秘钥用法过滤，“1”为过滤，“0”不过滤。
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.12.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
signedData	字符串	Base64编码的签名包
3.2.12.4功能说明
对输入的原文做attached方式签名，将签名的结果根据大小判断是否压缩，再做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/
void IWSAAttachedSignDefaultDNIS(DWORD*& errorCode, string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const string& defaultDN, const boolean isKeySpec, const string& digestArithmetic, const string& signerCertStore) {
	DWORD cbSignedBlob;
	BYTE* pbSignedBlob;
	if (SignIS(plainText, plainTextSize, pbSignedBlob, cbSignedBlob, defaultDN, isKeySpec, TRUE, digestArithmetic, signerCertStore)) {
		IWSABase64EncodeIS(errorCode, signedData, pbSignedBlob, cbSignedBlob);
	}
}

/*
3.2.13（方法）Attached方式签名使用默认DN  二代KEY
3.2.13.1名称
IWSAAttachedSignAdvDefaultDN (PlainText,CertStoreSM2,CertStoreType,DefaultDN,IsKeySpec,DigestArithmetic,SucceedFunction)

3.2.13.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
CertStoreSM2	字符串	国密证书存储区
CertStoreType	整数	1：强制RSA, 2：强制SM2, 3：优先RSA, 4：优先SM2
DefaultDN	字符串	默认证书DN
IsKeySpec	字符串	是否按秘钥用法过滤，“1”为过滤，“0”不过滤。
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.13.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
signedData	字符串	Base64编码的签名包
3.2.13.4功能说明
对输入的原文做attached方式签名，将签名的结果根据大小判断是否压缩，再做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/

/*
3.2.14（方法）Attached方式验签名
3.2.14.1名称
IWSAAttachedVerify(PortGrade, signedMsg, SucceedFunction)

3.2.14.2参数
参数名称	类  型	参数描述
PortGrade	整数	1为一代验签接口，2为 二代验签接口
signedMsg	字符串	Base64格式的签名包
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.14.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
PlainText	字符串	原文
certDN	字符串	签名证书DN
3.2.14.4功能说明
对输入的签名先作Base64解码，根据签名包大小判断是否需要解压缩，然后验证签名，如果成功，就开始校验签名证书的签名、有效期、证书链的有效性以及CRL。如果都成功，则errorCode为0，否则设置errorCode为对应的错误信息。验证签名成功后，返回 原文，签名证书DN，错误码三项。
用户可以通过查看errorCode错误码表，对照错误码表得到详细错误信息。
*/

void IWSAAttachedVerifyIS(DWORD*& errorCode, string& plainText, string& certDN, const string& portGrade, const string& signedData) {
	CRYPT_VERIFY_MESSAGE_PARA verifyPara;
	BYTE* textData;

	BYTE* pbDecoded;
	DWORD cbBinary;

	DWORD cbDecodedMessageBlob;
	BYTE* pbDecodedMessageBlob = NULL;
	PCCERT_CONTEXT* ppSignerCert = NULL;

	//---------------------------------------------------------------
	//  The following variables are used only in the decoding phase.

	HCRYPTMSG hMsg;
	HCERTSTORE hStoreHandle;           // certificate store handle
	DWORD cbData = sizeof(DWORD);
	DWORD cbSignerCertInfo;
	PCERT_INFO pSignerCertInfo;
	PCCERT_CONTEXT pSignerCertContext;

	//---------------------------------------------------------------
	//    Buffer to hold the name of the subject of a certificate.

	char pszNameString[MAX_NAME];


	// Initialize the VerifyParams data structure.
	verifyPara.cbSize = sizeof(CRYPT_VERIFY_MESSAGE_PARA);
	verifyPara.dwMsgAndCertEncodingType = MY_TYPE;
	verifyPara.hCryptProv = NULL;
	verifyPara.pfnGetSignerCertificate = NULL;
	verifyPara.pvGetArg = NULL;

	IWSABase64DecodeIS(errorCode, pbDecoded, cbBinary, signedData);

	// First, call CryptVerifyMessageSignature to get the length 
   // of the buffer needed to hold the decoded message.
	if (CryptVerifyMessageSignature(&verifyPara,0,pbDecoded,cbBinary,NULL,&cbDecodedMessageBlob,NULL))
	{
		printf(TEXT("%d bytes needed for the decoded message.\n"),cbDecodedMessageBlob);
	}
	else
	{
		MyCertHandleError(TEXT("Verification message failed. \n"));
	}

	//---------------------------------------------------------------
	//   Allocate memory for the decoded message.
	if (!(pbDecodedMessageBlob =(BYTE*)malloc(cbDecodedMessageBlob)))
	{
		MyCertHandleError(TEXT("Memory allocation error allocating decode BLOB."));
	}

	//---------------------------------------------------------------
	// Call CryptVerifyMessageSignature again to verify the signature
	// and, if successful, copy the decoded message into the buffer. 
	// This will validate the signature against the certificate in 
	// the local store.
	if (CryptVerifyMessageSignature(&verifyPara, 0, pbDecoded, cbBinary, pbDecodedMessageBlob, &cbDecodedMessageBlob, NULL))
	{
		printf(TEXT("The verified message is \"%s\".\n"),pbDecodedMessageBlob);
		*errorCode = 0;
		plainText = string((char *)pbDecodedMessageBlob,cbDecodedMessageBlob);
		
		//---------------------------------------------------------------
		//  Open a message for decoding.

		if (hMsg = CryptMsgOpenToDecode(MY_TYPE,      // encoding type
			0,                     // flags attached not release
			0,                     // use the default message type
								   // the message type is 
								   // listed in the message header
			NULL,                  // cryptographic provider 
								   // use NULL for the default provider
			NULL,                  // recipient information
			NULL))                 // stream information
		{
			printf("The message to decode is open. \n");
		}
		else
		{
			MyCertHandleError("OpenToDecode failed");
		}

		//---------------------------------------------------------------
		//  Update the message with an encoded BLOB.

		if (CryptMsgUpdate(
			hMsg,                 // handle to the message
			pbDecoded, // pointer to the encoded BLOB
			cbBinary, // size of the encoded BLOB
			TRUE))                // 
		{
			printf("The encoded BLOB has been added to the message. \n");
		}
		else
		{
			MyCertHandleError("Decode MsgUpdate failed");
		}

		//---------------------------------------------------------------
		// Get the size of memory required for the certificate.

		if (CryptMsgGetParam(
			hMsg,                         // handle to the message
			CMSG_SIGNER_CERT_INFO_PARAM,  // parameter type
			0,                            // index
			NULL,
			&cbSignerCertInfo))           // size of the returned 
										  // information
		{
			printf("%d bytes needed for the buffer.\n",
				cbSignerCertInfo);
		}
		else
		{
			MyCertHandleError("Verify SIGNER_CERT_INFO #1 failed.");
		}

		//---------------------------------------------------------------
		// Allocate memory.

		if (!(pSignerCertInfo = (PCERT_INFO)malloc(cbSignerCertInfo)))
		{
			MyCertHandleError("Verify memory allocation failed.");
		}

		//---------------------------------------------------------------
		// Get the message certificate information (CERT_INFO
		// structure).

		if (!(CryptMsgGetParam(
			hMsg,                         // handle to the message
			CMSG_SIGNER_CERT_INFO_PARAM,  // parameter type
			0,                            // index
			pSignerCertInfo,              // address for returned 
										  // information
			&cbSignerCertInfo)))          // size of the returned 
										  // information
		{
			MyCertHandleError("Verify SIGNER_CERT_INFO #2 failed");
		}

		//---------------------------------------------------------------
	// Open a certificate store in memory using CERT_STORE_PROV_MSG,
	// which initializes it with the certificates from the message.

		if (hStoreHandle = CertOpenStore(
			CERT_STORE_PROV_MSG,         // store provider type 
			MY_TYPE,            // encoding type
			NULL,                        // cryptographic provider
										 // use NULL for the default
			0,                           // flags
			hMsg))                       // handle to the message
		{
			printf("The certificate store to be used for message " \
				"verification has been opened.\n");
		}
		else
		{
			MyCertHandleError("Verify open store failed");
		}

		//---------------------------------------------------------------
		// Find the signer's certificate in the store.

		if (pSignerCertContext = CertGetSubjectCertificateFromStore(
			hStoreHandle,       // handle to the store
			MY_TYPE,   // encoding type
			pSignerCertInfo))   // pointer to retrieved CERT_CONTEXT
		{
			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_RDN_TYPE,
				0,
				NULL,
				pszNameString, // null-terminated string
				MAX_NAME) > 1)
			{
				printf("The message signer is  %s \n", pszNameString);
				certDN = pszNameString;
			}
			else
			{
				MyCertHandleError("Getting the signer's name failed.\n");
			}
		}
		else
		{
			MyCertHandleError("Verify GetSubjectCert failed");
		}
	}
	else
	{
		MyCertHandleError(TEXT("Verification message failed. \n"));
	}
}

/*
3.2.15（方法）RAW裸签方式签名
3.2.15.1名称
IWSARawSign(PlainText,CertIndex,DigestArithmetic,SucceedFunction)
3.2.15.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
CertIndex	整数	证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.15.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
signedData	字符串	Base64编码的签名包
3.2.15.4功能说明
对输入的原文做RAW方式签名，再将签名的结果做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/
void IWSARawSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& signerCertIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore) {
	BCRYPT_ALG_HANDLE       hAlg = NULL;
	BCRYPT_HASH_HANDLE      hHash = NULL;
	NTSTATUS                status = STATUS_UNSUCCESSFUL;
	DWORD                   cbData = 0,
		cbHash = 0,
		cbHashObject = 0,
		cbBlob = 0,
		cbSignature = 0;
	PBYTE                   pbHashObject = NULL;
	PBYTE                   pbHash = NULL,
		pbBlob = NULL,
		pbSignature = NULL;

	HCERTSTORE hSignerCertStore = NULL;
	PCCERT_CONTEXT pSignerCertContext = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;

	DWORD indexCert = 0;
	TCHAR pszNameString[256];
	DWORD dwKeySpec;

	NCRYPT_PROV_HANDLE      hProv = NULL;

	SECURITY_STATUS         secStatus = ERROR_SUCCESS;

	BCRYPT_PKCS1_PADDING_INFO padding_PKCS1 = BCRYPT_PKCS1_PADDING_INFO();
	BCRYPT_PSS_PADDING_INFO padding_PSS = BCRYPT_PSS_PADDING_INFO(); // not used
	padding_PSS.cbSalt = 32;

	LPCWSTR algoHash;

	if (digestArithmetic == SHA256) {
		algoHash = BCRYPT_SHA256_ALGORITHM;
	}
	else if (digestArithmetic == SHA1) { algoHash = BCRYPT_SHA1_ALGORITHM; }
	else if (digestArithmetic == MD5) { algoHash = BCRYPT_MD5_ALGORITHM; }
	else { MyCertHandleError("[x] Illegal Digest Algorithm Error."); }
	
	padding_PKCS1.pszAlgId = algoHash;
	padding_PSS.pszAlgId = algoHash;

	//open an algorithm handle <=> getAlgHandle
	if (!NT_SUCCESS(status = BCryptOpenAlgorithmProvider(
		&hAlg,
		algoHash,
		NULL,
		0)))
	{
		MyCertHandleError("**** Error returned by BCryptOpenAlgorithmProvider\n");
	}
	//calculate the size of the buffer to hold the hash object <=> getHashObjectLen() tous sont objets
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAlg,
		BCRYPT_OBJECT_LENGTH,
		(PBYTE)&cbHashObject,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptGetProperty\n"));
	}

	//allocate the hash object on the heap
	pbHashObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHashObject);
	if (NULL == pbHashObject)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	//calculate the length of the hash <=> getHashLen()
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAlg,
		BCRYPT_HASH_LENGTH,
		(PBYTE)&cbHash,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptGetProperty\n"));
	}

	//allocate the hash buffer on the heap
	pbHash = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHash);
	if (NULL == pbHash)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	//create a hash <=> new hash
	if (!NT_SUCCESS(status = BCryptCreateHash(
		hAlg,
		&hHash,
		pbHashObject,
		cbHashObject,
		NULL,
		0,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptCreateHash\n"));
	}

	//hash some data
	if (!NT_SUCCESS(status = BCryptHashData(
		hHash,
		(PBYTE)plainText,
		plainTextSize,
		0)))
	{
		MyCertHandleError(TEXT("**** Error 0x%x returned by BCryptHashData\n"));
	}
	//retrive the hash
	if (!NT_SUCCESS(status = BCryptFinishHash(
		hHash,
		pbHash,
		cbHash,
		0)))
	{
		MyCertHandleError(TEXT("**** Error 0x%x returned by BCryptFinishHash\n"));
	}
	
	// Open the certificate store.
	if (!(hSignerCertStore = CertOpenSystemStore(NULL,signerCertStore.c_str()))){MyCertHandleError("The MY store could not be opened. \n");}
	
	while (pSignerCertContext = CertEnumCertificatesInStore(hSignerCertStore, pSignerCertContext)) {
		if (indexCert++ == signerCertIndex) {
			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_SIMPLE_DISPLAY_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
					TEXT("%s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_RDN_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The RDM_TYPE message signer's name is %s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
			if (!(CryptAcquireCertificatePrivateKey(
				pSignerCertContext,
				CRYPT_ACQUIRE_ONLY_NCRYPT_KEY_FLAG,
				NULL,
				&hKey,
				&dwKeySpec,
				FALSE))) {
				MyCertHandleError(TEXT("CryptAcquireCertificatePrivateKey.\n"));
			}
			else {
				if (isKeySpec && dwKeySpec != CERT_NCRYPT_KEY_SPEC) MyCertHandleError("[x] Key is filtered.");
				cout << "Acquire Private KeyUsage Success: " << dwKeySpec << endl;
			}
			break;
		}
	}

	//sign the hash
	if (FAILED(secStatus = NCryptSignHash(
		hKey,
		&padding_PKCS1,
		pbHash,
		cbHash,
		NULL,
		0,
		&cbSignature,
		BCRYPT_PAD_PKCS1)))
	{
		MyCertHandleError(TEXT("**** Error returned by NCryptSignHash\n"));
	}

	//allocate the signature buffer
	pbSignature = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbSignature);
	if (NULL == pbSignature)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	if (FAILED(secStatus = NCryptSignHash(
		hKey,
		&padding_PKCS1,
		pbHash,
		cbHash,
		pbSignature,
		cbSignature,
		&cbSignature,
		BCRYPT_PAD_PKCS1)))
	{
		MyCertHandleError(TEXT("**** Error returned by NCryptSignHash\n"));
	}

	*errorCode = 0;
	IWSABase64EncodeIS(errorCode,signedData,pbSignature,cbSignature);

}

/*
3.2.16（方法）RAW裸签方式签名使用默认DN
3.2.16.1名称
IWSARawSignDefaultDN(PlainText, DefaultDN, IsKeySpec, DigestArithmetic, SucceedFunction)
3.2.16.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
DefaultDN	字符串	默认证书DN
IsKeySpec	字符串	是否按秘钥用法过滤，“1”为过滤，“0”不过滤。
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.16.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
signedData	字符串	Base64编码的签名包
3.2.16.4功能说明
对输入的原文做RAW方式签名，再将签名的结果做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/
void IWSARawSignDefaultDNIS(DWORD*& errorCode, string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const string& defaultDN, const boolean isKeySpec, const string& digestArithmetic, const string& signerCertStore) {
	BCRYPT_ALG_HANDLE       hAlg = NULL;
	BCRYPT_HASH_HANDLE      hHash = NULL;
	NTSTATUS                status = STATUS_UNSUCCESSFUL;
	DWORD                   cbData = 0,
		cbHash = 0,
		cbHashObject = 0,
		cbBlob = 0,
		cbSignature = 0;
	PBYTE                   pbHashObject = NULL;
	PBYTE                   pbHash = NULL,
		pbBlob = NULL,
		pbSignature = NULL;

	HCERTSTORE hSignerCertStore = NULL;
	PCCERT_CONTEXT pSignerCertContext = NULL, pPreSignerCertContext = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;

	DWORD indexCert = 0;
	TCHAR pszNameString[256];
	DWORD dwKeySpec;

	NCRYPT_PROV_HANDLE      hProv = NULL;

	SECURITY_STATUS         secStatus = ERROR_SUCCESS;

	BCRYPT_PKCS1_PADDING_INFO padding_PKCS1 = BCRYPT_PKCS1_PADDING_INFO();
	BCRYPT_PSS_PADDING_INFO padding_PSS = BCRYPT_PSS_PADDING_INFO(); // not used
	padding_PSS.cbSalt = 32;

	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, defaultDN.c_str(), -1, NULL, 0);
	LPWSTR lpSigner = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, defaultDN.c_str(), -1, lpSigner, wchars_num);

	LPCWSTR algoHash;

	if (digestArithmetic == SHA256) {
		algoHash = BCRYPT_SHA256_ALGORITHM;
	}
	else if (digestArithmetic == SHA1) { algoHash = BCRYPT_SHA1_ALGORITHM; }
	else if (digestArithmetic == MD5) { algoHash = BCRYPT_MD5_ALGORITHM; }
	else { MyCertHandleError("[x] Illegal Digest Algorithm Error."); }

	padding_PKCS1.pszAlgId = algoHash;
	padding_PSS.pszAlgId = algoHash;

	//open an algorithm handle <=> getAlgHandle
	if (!NT_SUCCESS(status = BCryptOpenAlgorithmProvider(
		&hAlg,
		algoHash,
		NULL,
		0)))
	{
		MyCertHandleError("**** Error returned by BCryptOpenAlgorithmProvider\n");
	}
	//calculate the size of the buffer to hold the hash object <=> getHashObjectLen() tous sont objets
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAlg,
		BCRYPT_OBJECT_LENGTH,
		(PBYTE)&cbHashObject,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptGetProperty\n"));
	}

	//allocate the hash object on the heap
	pbHashObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHashObject);
	if (NULL == pbHashObject)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	//calculate the length of the hash <=> getHashLen()
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAlg,
		BCRYPT_HASH_LENGTH,
		(PBYTE)&cbHash,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptGetProperty\n"));
	}

	//allocate the hash buffer on the heap
	pbHash = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHash);
	if (NULL == pbHash)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	//create a hash <=> new hash
	if (!NT_SUCCESS(status = BCryptCreateHash(
		hAlg,
		&hHash,
		pbHashObject,
		cbHashObject,
		NULL,
		0,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptCreateHash\n"));
	}

	//hash some data
	if (!NT_SUCCESS(status = BCryptHashData(
		hHash,
		(PBYTE)plainText,
		plainTextSize,
		0)))
	{
		MyCertHandleError(TEXT("**** Error 0x%x returned by BCryptHashData\n"));
	}
	//retrive the hash
	if (!NT_SUCCESS(status = BCryptFinishHash(
		hHash,
		pbHash,
		cbHash,
		0)))
	{
		MyCertHandleError(TEXT("**** Error 0x%x returned by BCryptFinishHash\n"));
	}

	// Open the certificate store.
	if (!(hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore.c_str()))) { MyCertHandleError("The MY store could not be opened. \n"); }

	while ((pSignerCertContext = CertFindCertificateInStore(
		hSignerCertStore,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		lpSigner,
		pPreSignerCertContext)))
	{
		pPreSignerCertContext = pSignerCertContext;
		//---------------------------------------------------------------
		// Get and print the name of the message signer.
		// The following two calls to CertGetNameString with different
		// values for the second parameter get two different forms of 
		// the certificate subject's name.
		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
				TEXT("%s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_RDN_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The RDM_TYPE message signer's name is %s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
		if (!(CryptAcquireCertificatePrivateKey(
			pSignerCertContext,
			CRYPT_ACQUIRE_ONLY_NCRYPT_KEY_FLAG,
			NULL,
			&hKey,
			&dwKeySpec,
			NULL))) {
			MyCertHandleError(TEXT("CryptAcquireCertificatePrivateKey.\n"));
		}
		else {
			if (isKeySpec && dwKeySpec != CERT_NCRYPT_KEY_SPEC) {
				continue;
			}
			cout << "Acquire Private KeyUsage Success: " << dwKeySpec << endl;
			break;
		}
	}

	//sign the hash
	if (FAILED(secStatus = NCryptSignHash(
		hKey,
		&padding_PKCS1,
		pbHash,
		cbHash,
		NULL,
		0,
		&cbSignature,
		BCRYPT_PAD_PKCS1)))
	{
		MyCertHandleError(TEXT("**** Error returned by NCryptSignHash\n"));
	}

	//allocate the signature buffer
	pbSignature = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbSignature);
	if (NULL == pbSignature)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	if (FAILED(secStatus = NCryptSignHash(
		hKey,
		&padding_PKCS1,
		pbHash,
		cbHash,
		pbSignature,
		cbSignature,
		&cbSignature,
		BCRYPT_PAD_PKCS1)))
	{
		MyCertHandleError(TEXT("**** Error returned by NCryptSignHash\n"));
	}

	* errorCode = 0;
	IWSABase64EncodeIS(errorCode, signedData, pbSignature, cbSignature);
}

/*
3.2.17（方法）RAW裸签方式验签名
3.2.17.1名称
IWSARawVerify (PlainText,SignMsg,CertIndex,DigestArithmetic,SucceedFunction)
3.2.17.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
signedMsg	字符串	Base64格式的签名包
CertIndex	整数	证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.17.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
3.2.17.4功能说明
对输入的签名先作Base64解码，然后验证签名，如果成功，则errorCode为0，否则设errorCode为对应的错误信息。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/
void IWSARawVerifyIS(DWORD*& errorCode, const string& signedData, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& signerCertIndex, const string& digestArithmetic) {
	BCRYPT_ALG_HANDLE       hAlg = NULL;
	BCRYPT_HASH_HANDLE      hHash = NULL;
	NTSTATUS                status = STATUS_UNSUCCESSFUL;
	DWORD                   cbData = 0,
		cbHash = 0,
		cbHashObject = 0,
		cbSignature = 0;
	PBYTE                   pbHashObject = NULL;
	PBYTE                   pbHash = NULL,pbSignature = NULL;

	HCERTSTORE hSignerCertStore = NULL;
	PCCERT_CONTEXT pSignerCertContext = NULL;
	BCRYPT_KEY_HANDLE hKey = NULL;

	DWORD indexCert = 0;
	TCHAR pszNameString[256];
	DWORD dwKeySpec;

	NCRYPT_PROV_HANDLE      hProv = NULL;

	SECURITY_STATUS         secStatus = ERROR_SUCCESS;

	BCRYPT_PKCS1_PADDING_INFO padding_PKCS1 = BCRYPT_PKCS1_PADDING_INFO();
	BCRYPT_PSS_PADDING_INFO padding_PSS = BCRYPT_PSS_PADDING_INFO(); // not used
	padding_PSS.cbSalt = 32;

	LPCSTR signerCertStore = MY;

	LPCWSTR algoHash;

	if (digestArithmetic == SHA256) {
		algoHash = BCRYPT_SHA256_ALGORITHM;
	}
	else if (digestArithmetic == SHA1) { algoHash = BCRYPT_SHA1_ALGORITHM; }
	else if (digestArithmetic == MD5) { algoHash = BCRYPT_MD5_ALGORITHM; }
	else { MyCertHandleError("[x] Illegal Digest Algorithm Error."); }

	padding_PKCS1.pszAlgId = algoHash;
	padding_PSS.pszAlgId = algoHash;

	IWSABase64DecodeIS(errorCode, pbSignature, cbSignature, signedData);

	//open an algorithm handle <=> getAlgHandle
	if (!NT_SUCCESS(status = BCryptOpenAlgorithmProvider(
		&hAlg,
		algoHash,
		NULL,
		0)))
	{
		MyCertHandleError("**** Error returned by BCryptOpenAlgorithmProvider\n");
	}
	//calculate the size of the buffer to hold the hash object <=> getHashObjectLen() tous sont objets
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAlg,
		BCRYPT_OBJECT_LENGTH,
		(PBYTE)&cbHashObject,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptGetProperty\n"));
	}

	//allocate the hash object on the heap
	pbHashObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHashObject);
	if (NULL == pbHashObject)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	//calculate the length of the hash <=> getHashLen()
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAlg,
		BCRYPT_HASH_LENGTH,
		(PBYTE)&cbHash,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptGetProperty\n"));
	}

	//allocate the hash buffer on the heap
	pbHash = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHash);
	if (NULL == pbHash)
	{
		MyCertHandleError(TEXT("**** memory allocation failed\n"));
	}

	//create a hash <=> new hash
	if (!NT_SUCCESS(status = BCryptCreateHash(
		hAlg,
		&hHash,
		pbHashObject,
		cbHashObject,
		NULL,
		0,
		0)))
	{
		MyCertHandleError(TEXT("**** Error returned by BCryptCreateHash\n"));
	}

	//hash some data
	if (!NT_SUCCESS(status = BCryptHashData(
		hHash,
		(PBYTE)plainText,
		plainTextSize,
		0)))
	{
		MyCertHandleError(TEXT("**** Error 0x%x returned by BCryptHashData\n"));
	}
	//retrieve the hash
	if (!NT_SUCCESS(status = BCryptFinishHash(
		hHash,
		pbHash,
		cbHash,
		0)))
	{
		MyCertHandleError(TEXT("**** Error 0x%x returned by BCryptFinishHash\n"));
	}

	// Open the certificate store.
	if (!(hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore))) { MyCertHandleError("The MY store could not be opened. \n"); }

	while (pSignerCertContext = CertEnumCertificatesInStore(hSignerCertStore, pSignerCertContext)) {
		if (indexCert++ == signerCertIndex) {
			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_SIMPLE_DISPLAY_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
					TEXT("%s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

			if (CertGetNameString(
				pSignerCertContext,
				CERT_NAME_RDN_TYPE,
				0,
				NULL,
				pszNameString,
				MAX_NAME) > 1)
			{
				_tprintf(
					TEXT("The RDM_TYPE message signer's name is %s \n"),
					pszNameString);
			}
			else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
			break;
		}
	}

	LPSTR lp = const_cast<LPSTR>(szOID_RSA_RSA);
	DWORD cbInfo; 
	if (CryptImportPublicKeyInfoEx2(MY_TYPE, &pSignerCertContext->pCertInfo->SubjectPublicKeyInfo,  CRYPT_OID_INFO_PUBKEY_SIGN_KEY_FLAG, NULL, &hKey)) {
			if (!NT_SUCCESS(status = BCryptVerifySignature(
				hKey,
				&padding_PKCS1,
				pbHash,
				cbHash,
				pbSignature,
				cbSignature,
				BCRYPT_PAD_PKCS1)))
			{
				MyCertHandleError("**** Error returned by BCryptVerifySignature\n");
			}
	}
	else { MyCertHandleError("[x] Memory allocation failed.\n"); }
	*errorCode = 0;
}
