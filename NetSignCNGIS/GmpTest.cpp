﻿//#include "Header.h"
#include "gmpxx.h"
#include <Windows.h>
using namespace std;

void testGmp()
{
    
    mpz_t t; //mpz_t 为GMP内置大数类型
    mpz_init(t);    //大数t使用前要进行初始化，以便动态分配空间
    mpz_ui_pow_ui(t, 2, 100);	//GMP所有函数基本都是以mpz打头
    gmp_printf("2^100=%Zd\n", t);   //输出大数，大数的格式化标志为%Zd
    mpz_clear(t);
    //scanf_s("%s");
    
    mpz_class a,b;
    a = 100;
    b = 99;
    mpz_class c = a * b;
    //cout << c << endl;
    gmp_printf("%Zd\n",c);
    

}