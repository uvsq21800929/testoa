#pragma once
#define CERT_STORE_SM2 "CertStoreSM2"
#define CERT_STORE_RSA "CertStoreRSA"
#define KEY_SPEC "Keyspec"
#define EMPTY_PARAM ""
#define BANNER "=================="
#define VERSION "NetSignCNG  1.0.16.1 Build 20161010"

#define SHA1 "SHA1"
#define MD5 "MD5"
#define SHA256 "SHA256"

#define PORTGRADE1 "PortGrade1"
#define PORTGRADE2 "PortGrade2"

#define MY "MY"
#define ROOT "ROOT"
#define CA "CA"
#define ADDRESSBOOK "ADDRESSBOOK"

