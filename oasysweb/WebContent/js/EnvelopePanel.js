const SYMALGO = "3DES";
const DIGESTALGO = "SHA256";
const SIGNSTORE = "Sign";
const ENVELOPESTORE = "SignAndEncrypt";
const KEYSPEC = 0;

var EnvelopePanel = function(objectType) {
	var window = new Ext.Window(
			{
				id : 'EnvelopeWin',
				title : 'Merci de choisir les certificates pour l\'enveloppe (et pour la signature) ',
				iconCls : 'menu-diary',
				width : 950,
				height : 600,
				modal : true,
				minWidth : 400,
				minHeight : 300,
				layout : 'anchor',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				items : [ this.setup() ],
				buttons : [
						{
							text : '选择',
							iconCls : 'btn-save',
							handler : function() {
								const grid = Ext.getCmp("EnvelopeGrid");

								const selectRecords = grid.getSelectionModel().getSelections();

								var ids = new Array();

								for (var i = 0; i < selectRecords.length; i++) {
									ids.push(selectRecords[i].data.certIndex);
								}

								const signatureController = Ext.getDom("idIS");
								signatureController.dataset.sign = ids;

								// console.log(signatureController);

								if (selectRecords.length == 0) {
									Ext.ux.Toast.msg("信息", "请选择使用的证书！");
									return;
								}

								switch (objectType) {
								
								case 'MessageView':
									var reView = Ext.getCmp('MessageView');
									reView.removeAll();
									reView.add(new MessageForm());
									reView.doLayout(true);
									break;		
								case 'DocumentView':
									new DocumentForm(null);
									break;
								case 'MailForm':
									var mailform = Ext.getCmp('MailForm');
									var mailStatus = Ext.getCmp('mail.mailStatus');
									mailStatus.setValue(1);
									
									var originalVal = encodeURIComponent(Ext.getCmp('mail.content').getValue());
									originalVal = decodeURIComponent(originalVal.replace(/%E2%80%8B/ig,"")); 

									const envelopeIS=envelopeSignIS(originalVal);		
									// Ext.getCmp('mail.content').setValue(envelopeIS);
																		
									mailform.getForm().submit(
													{
														waitMsg : '正在发送邮件,请稍候...',
														params : {
															'mail.content' : envelopeIS
														},
														success : function(mailform, o) {
															Ext.Msg.confirm('操作信息','邮件发送成功！继续发邮件?',
																			function(btn) {
																				if (btn == 'yes') {
																					var mailform = Ext.getCmp('MailForm');
																					mailform.getForm().reset();
																				} else {
																					var tabs = Ext.getCmp('centerTabPanel');
																					tabs.remove('MailForm');
																				}
																			});
														},
														failure : function(mailform, o) {
															Ext.ux.Toast.msg('错误信息',o.result.msg);
														}
													});
									break;
								default:
									console
											.log(`Sorry, we are out of ${expr}.`);
								}

								window.close();

							}
						}, {
							text : '取消',
							iconCls : 'btn-cancel',
							handler : function() {
								window.close();
							}
						} ]
			});
	window.show();
};

/**
 * 初始化数据
 */
EnvelopePanel.prototype.store = function() {
	const obj = new IWSAgent();
	obj.IWSASetAsyncMode(false);
	var comSign;
	var certCount;
	var certListObj;
	var json = [];
	certListObj = obj.IWSAGetAllCertsListInfo("", "SignAndEncrypt", 0);
	var store = new Ext.data.ArrayStore({
		fields : [ 'certIndex', 'certDN', 'issuerDN', 'certSN', 'validBegin',
				'validEnd', 'CertStore', 'KeyUsage', 'CertType' ],
		idIndex : 0
	// id for each record will be the first element
	});
	var data = [];
	for (var i = 0; i < certListObj.length; i++) {
		data[i] = [ i, certListObj[i].certDN, certListObj[i].issuerDN,
				certListObj[i].certSN, certListObj[i].validBegin,
				certListObj[i].validEnd, certListObj[i].CertStore,
				certListObj[i].KeyUsage, certListObj[i].CertType ];
	}

	store.loadData(data);
	// console.log(store);
	return store;
};

/**
 * 建立视图
 */
EnvelopePanel.prototype.setup = function() {
	return this.grid();
};

/**
 * 建立DataGrid
 */
EnvelopePanel.prototype.grid = function() {
	var sm = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false
	});
	var cm = new Ext.grid.ColumnModel({
		columns : [ sm, new Ext.grid.RowNumberer(), {
			header : 'Certificate DN',
			dataIndex : 'certDN',
			width : 500
		}, {
			header : 'Issuer DN',
			dataIndex : 'issuerDN',
			width : 100
		}, {
			header : 'SN',
			dataIndex : 'certSN',
			width : 80
		}, {
			header : 'Validate begin',
			dataIndex : 'validBegin',
			width : 100
		}, {
			header : 'Validate end',
			dataIndex : 'validEnd',
			width : 100
		} ],
		defaults : {
			sortable : false,
			menuDisabled : true,
			width : 165
		}
	});

	var store = this.store();

	// console.log(store);

	var grid = new Ext.grid.GridPanel({
		id : 'EnvelopeGrid',
		// tbar : this.topbar(),
		store : store,
		trackMouseOver : true,
		disableSelection : false,
		loadMask : true,
		autoHeight : true,
		cm : cm,
		sm : sm,
		viewConfig : {
			forceFit : false,
			enableRowBody : false,
			showPreview : false
		},
		bbar : new Ext.PagingToolbar({
			pageSize : 10,
			store : store,
			displayInfo : true,
			displayMsg : '当前显示从{0}至{1}， 共{2}条记录',
			emptyMsg : "当前没有记录"
		})
	});
	return grid;

};


function envelopeIS(plainText){
	obj.IWSASetAsyncMode(false);
	const index = Ext.getDom('idIS').dataset.sign;
	var EnvelopedataOBJ = obj.IWSAEncryptedEnvelop(plainText, index[0], SYMALGO);
	ErrorCode = EnvelopedataOBJ[0];
	EnvelopedData = EnvelopedataOBJ[1];
	return EnvelopedData;
}

function decryptIS(envelope){
	obj.IWSASetAsyncMode(false);
	var dataOBJ = obj.IWSADecryptEnvelop(envelope);
	var errorCode = dataOBJ[0];
	var PlainText = dataOBJ[1];
	var certDN = dataOBJ[2];
	return PlainText;
}

function envelopeSignIS(plainText){
	obj.IWSASetAsyncMode(false);
	const index = Ext.getDom('idIS').dataset.sign.split(',');
	index.sort();
	console.log(index);
	var EnvelopedataOBJ = obj.IWSAEncryptedSignEnvelop(plainText, index[0], index[1], DIGESTALGO, SYMALGO);
	ErrorCode = EnvelopedataOBJ[0];
	EnvelopedData = EnvelopedataOBJ[1];
	return EnvelopedData;
}

function envelopeSignFileIS(plainText){
	obj.IWSASetAsyncMode(false);
	const index = Ext.getDom('idIS').dataset.sign.split(',');
	index.sort();
	console.log(index);
	obj.IWSAAddFile(plainText);
	var EnvelopedataOBJ = obj.IWSAFormFileEncryptedSignEnvelop(index[0], index[1], DIGESTALGO, SYMALGO);
	ErrorCode = EnvelopedataOBJ[0];
	EnvelopedData = EnvelopedataOBJ[1];
	return EnvelopedData;
}

