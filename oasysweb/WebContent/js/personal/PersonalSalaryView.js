// https://www.zhengrongshuo.com/code/show/8149248
PersonalSalaryView = function() {
	return new Ext.Panel(
			{
				id : 'PersonalSalaryView',
				title : '个人薪酬纪录',
				iconCls : 'menu-personal-salary',
				autoScroll : true,
				items : [
						new Ext.FormPanel(
								{
									height : 35,
									frame : true,
									id : 'PersonalSalaryForm',
									layout : 'column',
									defaults : {
										xtype : 'label'
									},
									items : [
											{
												text : "查询条件：日期从"
											},
											{
												name : "Q_startTime_D_GE",
												width : 100,
												xtype : "datefield",
												format : "Y-m-d"
											},
											{
												text : "至"
											},
											{
												name : "Q_endTime_D_LE",
												width : 100,
												xtype : "datefield",
												format : "Y-m-d"
											},
											{
												xtype : "button",
												text : "查询",
												iconCls : "search",
												handler : function() {
													var searchPanel = Ext
															.getCmp('PersonalSalaryForm');
													var grid = Ext
															.getCmp('PersonalSalaryGrid');
													if (searchPanel.getForm()
															.isValid()) {
														searchPanel
																.getForm()
																.submit(
																		{
																			waitMsg : '正在提交查询',
																			method : 'GET',
																			url : __ctxPath
																					+ '/hrm/listSalaryPayoff.do',
																			success : function(
																					formPanel,
																					action) {
																				var result = Ext.util.JSON
																						.decode(action.response.responseText);
																				grid
																						.getStore()
																						.loadData(
																								result);
																			}
																		});
													}
												}
											} ]
								}), this.setup() ]
			});
};
/**
 * 
 * 建立视图
 * 
 */
PersonalSalaryView.prototype.setup = function() {
	return this.grid();
};
/**
 * 
 * 建立DataGrid
 * 
 */
PersonalSalaryView.prototype.grid = function() {
	var sm = new Ext.grid.CheckboxSelectionModel();
	var cm = new Ext.grid.ColumnModel({
		columns : [
				sm,
				new Ext.grid.RowNumberer(),
				{
					header : 'recordId',
					dataIndex : 'recordId',
					hidden : true
				},
				{
					header : '员工姓名',
					dataIndex : 'fullname'
				},
				{
					header : '档案编号',
					dataIndex : 'profileNo'
				},
				{
					header : '身份证号',
					dataIndex : 'idNo'
				},
				{
					header : '薪标金额',
					dataIndex : 'standAmount',
					renderer : function(e) {
						return '<img src="' + __ctxPath
								+ '/images/flag/customer/rmb.png"/>' + e;
					}
				},
				{
					header : '实发金额',
					dataIndex : 'acutalAmount',
					renderer : function(e) {
						return '<img src="' + __ctxPath
								+ '/images/flag/customer/rmb.png"/>' + e;
					}
				},
				{
					header : "薪酬日期",
					width : 130,
					dataIndex : "startTime",
					renderer : function(h, g, e, i, f) {
						return h.substring(0, 10) + "--"
								+ e.data.endTime.substring(0, 10);
					}
				} ],
		defaults : {
			sortable : true,
			menuDisabled : false,
			width : 100
		}
	});
	var store = this.store();
	store.load({
		params : {
			start : 0,
			limit : 25
		}
	});
	var grid = new Ext.grid.GridPanel({
		id : 'PersonalSalaryGrid',
		store : store,
		trackMouseOver : true,
		disableSelection : false,
		loadMask : true,
		autoHeight : true,
		cm : cm,
		sm : sm,
		viewConfig : {
			forceFit : true,
			enableRowBody : false,
			showPreview : false
		},
		bbar : new Ext.PagingToolbar({
			pageSize : 25,
			store : store,
			displayInfo : true,
			displayMsg : '当前显示从{0}至{1}， 共{2}条记录',
			emptyMsg : "当前没有记录"
		})
	});
	return grid;
};
/**
 * 
 * 初始化数据
 * 
 */
PersonalSalaryView.prototype.store = function() {
	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			method : 'GET',
			url : __ctxPath + '/hrm/listSalaryPayoff.do'
		}),
		root : "result",
		reader : new Ext.data.JsonReader({
			root : 'result',
			totalProperty : 'totalCounts',
			id : 'id',
			fields : [ {
				name : 'recordId',
				type : 'int'
			}, 'fullname', 'userId', 'profileNo', 'idNo', 'standAmount',
					'acutalAmount', 'startTime' ]
		}),
		remoteSort : true
	});
	store.setDefaultSort('recordId', 'desc');
	return store;
};