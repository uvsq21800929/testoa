var CertificatePanel = function(objectType) {
	var window = new Ext.Window(
			{
				id : 'CertificateWin',
				title : 'Merci de choisir le certificate désiré',
				iconCls : 'menu-diary',
				width : 950,
				height : 600,
				modal : true,
				minWidth : 400,
				minHeight : 300,
				layout : 'anchor',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				items : [ this.setup() ],
				buttons : [
						{
							text : '选择',
							iconCls : 'btn-save',
							handler : function() {
								const grid = Ext.getCmp("CertificateGrid");

								const selectRecords = grid.getSelectionModel()
										.getSelections();

								var ids = 0;

								for (var i = 0; i < selectRecords.length; i++) {
									ids = selectRecords[i].data.certIndex;
								}

								// console.log(ids);

								const signatureController = Ext
										.getDom("idIS");
								signatureController.dataset.sign = ids;

								// console.log(signatureController);

								if (selectRecords.length == 0) {
									Ext.ux.Toast.msg("信息", "请选择使用的证书！");
									return;
								}

								switch (objectType) {
								case 'LoginWin':
									new App.LoginWin().show();
									break;
								case 'DiaryForm':
									new DiaryForm();
									break;
								case 'MessageView':
									var reView = Ext.getCmp('MessageView');
									reView.removeAll();
									reView.add(new MessageForm());
									reView.doLayout(true);
									break;
								case 'PersonalMailBoxView':
									var tab = Ext.getCmp('centerTabPanel');
									var mailForm = Ext.getCmp('MailForm');
									if (mailForm == null) {
										mailForm = new MailForm('', '', '');
										tab.add(mailForm);
										tab.activate(mailForm);
									} else {
										tab.remove(mailForm);
										mailForm = new MailForm('', '', '');
										tab.add(mailForm);
										tab.activate(mailForm);
									}

									// expected output: "Mangoes and papayas are
									// $2.79 a pound."
									break;
								case 'DocumentView':
									new DocumentForm(null);
									break;
								case 'MailForm':
									var mailform = Ext.getCmp('MailForm');
									var mailStatus = Ext
											.getCmp('mail.mailStatus');
									mailStatus.setValue(1);
									mailform
											.getForm()
											.submit(
													{
														waitMsg : '正在发送邮件,请稍候...',
														success : function(
																mailform, o) {
															Ext.Msg
																	.confirm(
																			'操作信息',
																			'邮件发送成功！继续发邮件?',
																			function(
																					btn) {
																				if (btn == 'yes') {
																					var mailform = Ext
																							.getCmp('MailForm');
																					mailform
																							.getForm()
																							.reset();
																				} else {
																					var tabs = Ext
																							.getCmp('centerTabPanel');
																					tabs
																							.remove('MailForm');
																				}
																			});
														},
														failure : function(
																mailform, o) {
															Ext.ux.Toast
																	.msg(
																			'错误信息',
																			o.result.msg);
														}
													});
									break;
								default:
									console
											.log(`Sorry, we are out of ${expr}.`);
								}

								window.close();

							}
						}, {
							text : '取消',
							iconCls : 'btn-cancel',
							handler : function() {
								window.close();
							}
						} ]
			});
	window.show();
};

/**
 * 初始化数据
 */
CertificatePanel.prototype.store = function() {
	const obj = new IWSAgent();
	obj.IWSASetAsyncMode(false);
	var comSign;
	var certCount;
	var certListObj;
	var json = [];

	if (!!window.ActiveXObject || "ActiveXObject" in window) {
		// 是

		/*
		 * if(window.navigator.platform == "Win32") {//IE是32位的！ comSign = new
		 * ActiveXObject('NetSign.InfoSecNetSignGM.1'); } else {//IE是64位的！
		 * comSign = new ActiveXObject('NetSign.InfoSecNetSignGM2_64.1'); }
		 */

		certCount = comSign.NSGetAllCertsInfo(1);

		for (var i = 0; i < certcount; i++) {
			var row1 = {};
			row1.certDN = comSign.NSGetCertInfo_Subject(i);
			row1.issuerDN = comSign.NSGetCertInfo_Issuer(i);
			row1.certSN = comSign.NSGetCertInfo_SN(i);
			row1.validBegin = comSign.NSGetCertInfo_ValidBegin(i);
			row1.validEnd = comSign.NSGetCertInfo_ValidEnd(i);
			json.push(row1);
		}
		certListObj = json;

	} else {

		certListObj = obj.IWSAGetAllCertsListInfo("", "Sign", 0);
	}

	var store = new Ext.data.ArrayStore({
		fields : [ 'certIndex', 'certDN', 'issuerDN', 'certSN', 'validBegin',
				'validEnd', 'CertStore', 'KeyUsage', 'CertType' ],
		idIndex : 0
	// id for each record will be the first element
	});

	var data = [];

	for (var i = 0; i < certListObj.length; i++) {
		data[i] = [ i, certListObj[i].certDN, certListObj[i].issuerDN,
				certListObj[i].certSN, certListObj[i].validBegin,
				certListObj[i].validEnd, certListObj[i].CertStore,
				certListObj[i].KeyUsage, certListObj[i].CertType ];
	}

	store.loadData(data);
	// console.log(store);
	return store;
};

/**
 * 建立视图
 */
CertificatePanel.prototype.setup = function() {
	return this.grid();
};

/**
 * 建立DataGrid
 */
CertificatePanel.prototype.grid = function() {
	var sm = new Ext.grid.CheckboxSelectionModel();
	var cm = new Ext.grid.ColumnModel({
		columns : [ sm, new Ext.grid.RowNumberer(), {
			header : 'Certificate DN',
			dataIndex : 'certDN'
		}, {
			header : 'Issuer DN',
			dataIndex : 'issuerDN'
		}, {
			header : 'Certificate SN',
			dataIndex : 'certSN'
		}, {
			header : 'Validate begin',
			dataIndex : 'validBegin'
		}, {
			header : 'Validate end',
			dataIndex : 'validEnd'
		} ],
		defaults : {
			sortable : true,
			menuDisabled : false,
			width : 165
		}
	});

	var store = this.store();

	// console.log(store);

	var grid = new Ext.grid.GridPanel({
		id : 'CertificateGrid',
		// tbar : this.topbar(),
		store : store,
		trackMouseOver : true,
		disableSelection : false,
		loadMask : true,
		autoHeight : true,
		cm : cm,
		sm : sm,
		viewConfig : {
			forceFit : false,
			enableRowBody : false,
			showPreview : false
		},
		bbar : new Ext.PagingToolbar({
			pageSize : 10,
			store : store,
			displayInfo : true,
			displayMsg : '当前显示从{0}至{1}， 共{2}条记录',
			emptyMsg : "当前没有记录"
		})
	});

	grid.addListener('rowdblclick', function(grid, rowindex, e) {
		grid.getSelectionModel().each(function(rec) {
			// DiaryView.edit(rec.data.diaryId);
		});
	});
	return grid;

};

function signIS(plainText) {
	obj.IWSASetAsyncMode(false);
	const index = Ext.getDom('idIS').dataset.sign;
	var SigndataOBJ = obj.IWSADetachedSign(1, plainText, index, "SHA256");// 普通Key
	ErrorCode = SigndataOBJ[0];
	SignData = SigndataOBJ[1];
	return SignData;
};

let Certificate= class {
		constructor(index, certDN,issuerDN,certSN,validBegin,validEnd, CertStore,KeyUsage,CertType,pubKey) {
		    this.index = index;
		    this.certDN = certDN;
		    this.issuerDN = issuerDN;
		    this.certSN = certSN;
		    this.validBegin = validBegin;
		    this.validEnd = validEnd;
		    this.CertStore = CertStore;
		    this.KeyUsage = KeyUsage;
		    this.CertType = CertType;
		    this.pubKey = pubKey;
		  }
};

function preVerify(){
	const obj = new IWSAgent();
	obj.IWSASetAsyncMode(false);
	const index = Ext.getDom('idIS').dataset.sign;
	const certListObj = obj.IWSAGetAllCertsListInfo("", "Sign", 0);
	
	var data=[];
	for(var i=0;i< certListObj.length;i++)	
	{//取出证书各项信息
		var pubKey = obj.IWSAGetCertPublicKeyInfoForIndex(i, null);
		data[i]=new Certificate(i,certListObj[i].certDN,certListObj[i].issuerDN,certListObj[i].certSN,certListObj[i].validBegin,
			certListObj[i].validEnd,certListObj[i].CertStore,certListObj[i].KeyUsage,certListObj[i].CertType,pubKey);
	}
	

	
	//const certObj = obj.IWSAGetCertInfoForIndex(index,null);
	//alert(data);
	//console.log("[*] preVerify");
	//console.log(certObj);
	//alert(data[index]);
	return data[index];

}
