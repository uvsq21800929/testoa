function getCookie(cookieName) {
    var strCookie = document.cookie;
    var arrCookie = strCookie.split("; ");
    for(var i = 0; i < arrCookie.length; i++){
        var arr = arrCookie[i].split("=");
        if(cookieName == arr[0]){
            return arr[1];
        }
    }
    return "";
}

Ext.Ajax.on('beforerequest', function(conn, options) {
		
		var token = getCookie('XSRF-TOKEN');
		console.log(token);
		// Default headers to pass in every request
		Ext.Ajax.defaultHeaders = {
				'X-CSRF-TOKEN' : token
		};
});