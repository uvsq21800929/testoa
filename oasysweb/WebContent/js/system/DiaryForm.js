var DiaryForm = function(diaryId) {

	var signatureController = Ext.getDom('signature');
	//while(signatureController.dataset.sign==undefined || signatureController.dataset.sign==''||signatureController.dataset.sign==null){}
	
	//console.log("[*] Ready to sign with "+signatureController.dataset.sign);
	
	this.diaryId = diaryId;
	var fp = this.setup();
	var window = new Ext.Window({
		id : 'DiaryFormWin',
		title : '日志详细信息',
		iconCls : 'menu-diary',
		width : 500,
		height : 450,
		modal : true,
		minWidth : 300,
		minHeight : 200,
		layout : 'anchor',
		plain : true,
		bodyStyle : 'padding:5px;',
		buttonAlign : 'center',
		items : [ this.setup() ],
		buttons : [ {
			text : '保存',
			iconCls : 'btn-save',
			handler : function() {
				var fp = Ext.getCmp('DiaryForm');
				var originalVal = encodeURIComponent(Ext.getCmp('content').getValue());
				originalVal = decodeURIComponent(originalVal.replace(/%E2%80%8B/ig,"")); 
				Ext.getCmp('content').setValue(originalVal);
				const signatureIS=signIS(originalVal);		
				Ext.getCmp('signature').setValue(signatureIS);
				//console.log('[*] fp content: '+ Ext.getCmp('content').getValue());
				if (fp.getForm().isValid()) {
					fp.getForm().submit({
						method : 'post',
						url : __ctxPath + '/system/saveDiary.do',
						waitMsg : 'Submiting...',
						success : function(fp, action) {
							//console.log('[*] Dairy submit succeeds');
							Ext.ux.Toast.msg('操作信息', '成功信息保存！');
							Ext.getCmp('DiaryGrid').getStore().reload();
							//console.log('[*] Closing');
							window.close();
						},
						failure : function(fp, action) {
							Ext.MessageBox.show({
								title : '操作信息',
								msg : '信息保存出错，请联系管理员！',
								buttons : Ext.MessageBox.OK,
								icon : 'ext-mb-error'
							});
							window.close();
						}
					});
				}else{
					console.log('[*] Dairy not valid');
				}
			}
		}, {
			text : '取消',
			iconCls : 'btn-cancel',
			handler : function() {
				window.close();
			}
		} ]
	});
	window.show();
};

DiaryForm.prototype.setup = function() {

	var formPanel = new Ext.FormPanel({
		layout : 'form',
		id : 'DiaryForm',
		frame : true,
		defaults : {
			widht : 400,
			anchor : '100%,100%'
		},
		formId : 'DiaryFormId',
		defaultType : 'textfield',
		items : [ {
			name : 'diary.diaryId',
			id : 'diaryId',
			xtype : 'hidden',
			value : this.diaryId == null ? '' : this.diaryId
		}, {
			xtype : 'hidden',
			name : 'diary.appUser.userId',
			id : 'userId'
		}, {
			fieldLabel : '日期',
			xtype : 'datefield',
			name : 'diary.dayTime',
			id : 'dayTime',
			readOnly:true,
			format : 'Y-m-d'				
		},{
			fieldLabel : '日志类型',
			xtype : 'combo',
			hiddenName : 'diary.diaryType',
			id : 'diaryType',
			mode : 'local',
			editable : false,
			triggerAction : 'all',
			store : [ [ '0', '个人日志' ], [ '1', '工作日志' ] ]
		}, {
			fieldLabel : '内容',
			xtype : 'htmleditor',
			name : 'diary.content',
			id : 'content'
		},{
			xtype : 'hidden',
			name : 'signature',
			id : 'signature'
		} ]
	});

	if (this.diaryId != null && this.diaryId != 'undefined') {
		//console.log("[*] DiaryId="+this.diaryId+" "+formPanel.getForm().getValues());
		formPanel.load({
			deferredRender : false, //True by default to defer the rendering of child items to the browsers DOM until a tab is activated. False will render all contained items as soon as the layout is rendered
			url : __ctxPath + '/system/getISDiary.do?diaryId=' + this.diaryId,
			method : 'GET',
			waitMsg : '正在载入数据...',
			success : function(form, action) {
				//console.log("[*] get function succeeds2");
				var result = action.result.data;
				var dayTime = getDateFromFormat(result.dayTime, 'yyyy-MM-dd');
				Ext.getCmp('dayTime').setValue(new Date(dayTime));
			},
			failure : function(form, action) {
				Ext.ux.Toast.msg('编辑', '载入失败');
			}
		});
	}
	return formPanel;

};


