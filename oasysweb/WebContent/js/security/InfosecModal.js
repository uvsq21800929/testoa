	//1、页面初始化时，将模态框添加到页面中指定的div中
    /*$(function (){
  		var modalDiv = $("#modalDiv");//指定的div
  		//modalDiv.empty();
		modalDiv.append(
			'<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">'
				+'<div class="modal-dialog" style="width:900px!important">'
					+'<div class="modal-content">'
						+'<div class="modal-header" style="height:40px;color:#fff;background:#FF8800">'
							+'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;'
							+'</button>'
							+'<h5 class="modal-title" id="myModalLabel">请选择你要用的证书'
							+'</h5>'
						+'</div>'
						+'<div class="modal-body">请在列表中选择证书'
							+'<table id="tableList" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">'
							+'</table>'
						+'</div>'
						+'<div class="modal-footer" style="text-align:center">'
							+'<button type="button" class="btn-warning button orange bigrounded" onClick=\"selectCerButton(this)\">确定</button>&nbsp;'
							+'&nbsp;<button type="button" class="btn-default button bigrounded" onClick=\"CloseButton(this)\"data-dismiss="modal">关闭</button>'
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>'
		);
  	});*/
  	//全局变量
  	var obj = new IWSAgent();	
  	var cerDN = null;
  	var PlainText = null;
	var ErrorCode = -1 ;
	var SignData = "" ;
	var CertListObj ;
	var comSign ;
	var SelectSn = "";
	var Selectdn = "";
    //2、点击签名按钮事件
  	function InfosecAttachedSign(strPlainText){
		ErrorCode = 0;
		PlainText = strPlainText;//原文
		
		var modalDiv = $("#modalDiv");//指定的div
  		//modalDiv.empty();
		modalDiv.append(
			'<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">'
				+'<div class="modal-dialog" style="width:900px!important;margin-left:20%">'
					+'<div class="modal-content">'
						+'<div class="modal-header" style="height:40px;color:#fff;background:#FF8800">'
							+'<button type="button" class="close" onClick=\"CloseButton(this)\" data-dismiss="modal" aria-hidden="true">&times;'
							+'</button>'
							+'<h5 class="modal-title" id="myModalLabel">请选择你要用的证书'
							+'</h5>'
						+'</div>'
						+'<div class="modal-body">请在列表中选择证书'
							+'<table id="tableList" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">'
							+'</table>'
						+'</div>'
						+'<div class="modal-footer" style="text-align:center">'
							+'<button type="button" class="btn-warning button orange bigrounded" onClick=\"selectCerButton(this)\">确定</button>&nbsp;'
							+'&nbsp;<button type="button" class="btn-default button bigrounded" onClick=\"CloseButton(this)\"data-dismiss="modal">关闭</button>'
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>'
		);
		
		obj.IWSASetAsyncMode(false);
		
		var json = [];
		CertListObj = json;
		if (!!window.ActiveXObject || "ActiveXObject" in window){ 
		//是
			
			/*if(window.navigator.platform == "Win32")
			{//IE是32位的！
				comSign = new ActiveXObject('NetSign.InfoSecNetSignGM.1');					
			}			
			else
			{//IE是64位的！
				comSign = new ActiveXObject('NetSign.InfoSecNetSignGM2_64.1');				
			}*/	
			
			 certcount = comSign.NSGetAllCertsInfo(1);    
			
			for(var i=0;i<certcount;i++)
			{
				var row1 = {};
				row1.certDN= comSign.NSGetCertInfo_Subject(i);
				row1.issuerDN = comSign.NSGetCertInfo_Issuer(i);
				row1.certSN= comSign.NSGetCertInfo_SN(i);
				row1.validBegin = comSign.NSGetCertInfo_ValidBegin(i);
				row1.validEnd= comSign.NSGetCertInfo_ValidEnd(i);       
				json.push(row1);
			}
			CertListObj = json ;				
			
		}else{
		//不是 
		CertListObj = obj.IWSAGetAllCertsListInfo("","Sign",0);
		}		
		buttonAddAllCert_Succeed(CertListObj); 		
  	}	
	function buttonAddAllCert_Succeed(CertListData){
		var table = $("#tableList");
		table.empty();
		table.append('<tr style="background:#aca2a0; color:#fff;text-align: center;height:40px;">'
					+'<td></td>'
					+'<td>证书主题</td>'
					+'<td>颁发者</td>'
					+'<td>截止日期</td>'
				+'</tr>'
		);
		for(var i=0;i< CertListData.length;i++){//取出证书各项信息
			CertListData[i].certDN;
			CertListData[i].issuerDN;
			CertListData[i].certSN;
			CertListData[i].validBegin;
			CertListData[i].validEnd;
			CertListData[i].CertStore; 
			CertListData[i].KeyUsage;
			CertListData[i].CertType;
			table.append('<tr style="height:40px;text-align:center;" onclick="selectCheckOne(this)">'
					+'<td width="30px">'
						+'<input name="index" type="checkbox" value="'+i+'"/>'
					+'</td>'
					+'<td width="50px">'+CertListData[i].certDN+'</td>'
					+'<td width="50px">'+CertListData[i].issuerDN+'</td>'
					+'<td width="50px">'+CertListData[i].validEnd+'</td>'
				+'</tr>'
			);
		}
		//设置checkbox为单选
		selectCheckOne = function (obj){
	        var checks = document.getElementsByName("index"); //获取当前单选框控件name 属性值 
	        for(var i=0;i<=checks.length-1;i++){ 
	             if(checks[i]==obj && obj.checked){ 
	                checks[i].checked = true; 
	             }else{ 
	                checks[i].checked = false; 
	             } 
	        } 
		};
		$("#tableList tr").click(function(){
			/*当单击表格行时,把单选按钮设为选中状态*/
	        $(this).find("input[type='checkbox']").prop("checked",true);
		});
		// 鼠标移动时改变行的样式
	    $("#tableList tr").hover(function(){
	        $(this).addClass('selected');
	    },function(){
	        $(this).removeClass('selected');
	    });
		//return;
	}
	//4、点击确定把选中的证书序列号提交到签名方法
  	function selectCerButton(){
  		var index = $('input[name="index"]:checked').val();//证书序列号
		if(CertListObj.length>index)
		{
			SelectSn = CertListObj[index].certSN;
			Selectdn = CertListObj[index].issuerDN;								
		}	
  		buttonAttachedSign_onclick(index);
		
		$('#myModal').modal('hide');
  	}
	function CloseButton()
	{
  		ErrorCode = -20051;
  	}
  	//5、签名方法
	function buttonAttachedSign_onclick(index) { 
		
		if(typeof(index)=="undefined")
		{
			alert("请选择证书!");
		}else{
			//$('#myModal').modal('hide');
			
			if (!!window.ActiveXObject || "ActiveXObject" in window)
			{ //是		
				if(CertListObj.length>index)
				{
					SelectSn = CertListObj[index].certSN;
					Selectdn = CertListObj[index].issuerDN;					
					//comSign.NSSetPlainText(PlainText);
					//SignData = comSign.NSAttachedSignByIssuerSN(SelectSn,Selectdn);
					//ErrorCode = comSign.errorCode;			
				}				
			}else
			{//不是			 
				obj.IWSASetAsyncMode(false);
				var SigndataOBJ = obj.IWSAAttachedSign(2,PlainText,index,"SHA256");//普通Key Attached签名
				
				ErrorCode = SigndataOBJ[0] ;
				SignData = SigndataOBJ[1] ;
			}
		}
		return;	
	}
	//
	function InfosecGetError() 
	{ 		
		return ErrorCode ;		
	}
	function InfosecGetSignData() 
	{ 		
		return SignData ;		
	}	
	function InfosecGetSelectSn() 
	{
		return SelectSn ;
	}
	function InfosecGetSelectDn()
	{
		return Selectdn ;
	}
	function InfosecSetCom(infosecCom)
	{
		comSign = infosecCom;
	}
	