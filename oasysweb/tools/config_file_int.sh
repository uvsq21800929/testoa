sum=''
TEST_FILE='config_file_int_old'
PREFIX='../src/'
for FILE in `ls ../src/ | egrep '(.*\.xml|.*\.properties)'`
do
	sum=${sum}`sha256sum ${PREFIX}$FILE`
done

if [ -f $TEST_FILE ]
then
	old_compare=`cat $TEST_FILE`
	if [ "`echo $sum | sha256sum`" == "`echo $old_compare | sha256sum`" ]
	then
		echo "[*] Configuration Integrity check finished, Same files."
	else
		echo "[x] Configuration Integrity failed."
		echo $old_compare
		echo $sum
	fi
else
	echo $sum > $TEST_FILE
	echo "Premier sample: sauvegarde réussi."
fi



