part1=''
part2=''
TEST_FILE='config_file_int_old'
for FILE in `ls ../src/*.xml`
do
	part1=${part1}`sha256sum $FILE`
	#echo ${part1}
done

for FILE in `ls ../src/*.properties`
do
        part2=${part2}`sha256sum $FILE`
	#echo ${part2}
done

to_compare=`echo "${part1}${part2}"`
if [ -f $TEST_FILE ]
then
	old_compare=`cat $TEST_FILE`
	if [ "`echo $to_compare | sha256sum`" == "`echo $old_compare | sha256sum`" ]
	then
		echo "[*] Configuration Integrity check finished, Same files."
	else
		echo "[x] Configuration Integrity failed."
		echo $old_compare
		echo $to_compare
	fi
else
	echo $to_compare > $TEST_FILE
	echo "Premier sample: sauvegarde réussi."
fi



