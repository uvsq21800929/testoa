package com.htsoft.oa.model.system;
/*
 *  广州宏天软件有限公司 OA办公管理系统   --  http://www.jee-soft.cn
 *  Copyright (C) 2008-2009 GuangZhou HongTian Software Company
*/

import java.util.HashSet;
import java.util.Set;

import org.jbpm.api.identity.Group;
import org.jbpm.api.task.Participation;
import org.springframework.security.core.GrantedAuthority;

import com.google.gson.annotations.Expose;
import com.htsoft.core.model.BaseModel;

public class AppRole extends BaseModel implements GrantedAuthority, Group {
	 /*
	 * public static final String ROLE_PUBLIC="ROLE_PUBLIC";
	 * 
	 * public static final String ROLE_ANONYMOUS="ROLE_ANONYMOUS";
	 * 
	 * public static final String ROLE_ADMIN="ROLE_ADMIN";
	 */

	/*
	public static final String ROLE_AUTHENTICATED = "ROLE_AUTHENTICATED";
	public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	public static final String ROLE_ADMINISTRATION = "ROLE_ADMINISTRATIONMANAGER";
	public static final String ROLE_HUMANRESOURCE = "ROLE_HUMANRESOURCEMANAGER";
	public static final String ROLE_DOCUMENT = "ROLE_DOCUMENTMANAGER";
	public static final String ROLE_INFORMATION = "ROLE_INFORMATIONMANAGER";
	public static final String ROLE_CLIENT = "ROLE_CLIENTMANAGER";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	 */
	
	/**
	 * 超级管理员的角色ID
	 */
	public static final Long SUPER_ROLEID = -1l;
	/**
	 * 超级权限
	 */
	public static final String SUPER_RIGHTS = "__ALL";

	@Expose
	protected Long roleId;
	@Expose
	protected String roleName;
	@Expose
	protected String roleDesc;
	@Expose
	protected Short status;
	@Expose
	protected Short isDefaultIn;

	@Expose
	protected Set<AppRight> rights;

	protected Set<AppFunction> functions = new HashSet<AppFunction>();
	protected Set<AppUser> appUsers = new HashSet<AppUser>();

	public AppRole() {
		
	}
	
	public AppRole(String roleName) {
		this.roleName = roleName;
	}


	public AppRole(AppRole appRole) {
		this.setAppUsers(appRole.getAppUsers());
		this.setFunctions(appRole.getFunctions());
		this.setIsDefaultIn(appRole.getIsDefaultIn());
		this.setRights(appRole.getRights());
		this.setRoleDesc(appRole.getRoleDesc());
		this.setRoleId(appRole.getRoleId());
		this.setRoleName(appRole.getRoleName());
		this.setStatus(appRole.getStatus());
		this.setVersion(appRole.getVersion());
	}


	public Short getIsDefaultIn() {
		return isDefaultIn;
	}

	public void setIsDefaultIn(Short isDefaultIn) {
		this.isDefaultIn = isDefaultIn;
	}

	public Set<AppUser> getAppUsers() {
		return appUsers;
	}

	public void setAppUsers(Set<AppUser> appUsers) {
		this.appUsers = appUsers;
	}

	public Set<AppRight> getRights() {
		return rights;
	}

	public void setRights(Set<AppRight> rights) {
		this.rights = rights;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public String getAuthority() {
		return roleName;
	}

	public int compareTo(Object o) {
		return 0;
	}

	@Override
	public String getId() {
		return roleId.toString();
	}

	@Override
	public String getName() {
		return roleName;
	}

	@Override
	public String getType() {// 作为参与的侯选人
		return Participation.CANDIDATE;
	}

	public Set<AppFunction> getFunctions() {
		return functions;
	}

	public void setFunctions(Set<AppFunction> functions) {
		this.functions = functions;
	}

}
