package com.htsoft.oa.action.system;
/*
 *  广州宏天软件有限公司 OA办公管理系统   --  http://www.jee-soft.cn
 *  Copyright (C) 2008-2009 GuangZhou HongTian Software Company
*/

import java.math.BigInteger;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import nl.captcha.Captcha;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;/*
											import org.springframework.security.AuthenticationManager;
											import org.springframework.security.context.SecurityContext;
											import org.springframework.security.context.SecurityContextHolder;
											import org.springframework.security.providers.UsernamePasswordAuthenticationToken;
											import org.springframework.security.ui.rememberme.TokenBasedRememberMeServices;
											import org.springframework.security.ui.webapp.AuthenticationProcessingFilter;*/
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

import com.htsoft.core.security.model.AppUserLoginCredential;

import com.htsoft.core.security.model.CertificateAuthenticationTokenIS;
import com.htsoft.core.security.model.CertificateIS;
import com.htsoft.core.util.StringUtil;
import com.htsoft.core.web.action.BaseAction;
import com.htsoft.oa.model.system.AppUser;
import com.htsoft.oa.service.system.AppUserService;

public class LoginAction extends BaseAction {
	private AppUser user;
	private String username;
	private String password;
	private String checkCode;// 验证码
	private String res;
	private BigInteger challenge;
	private CertificateIS certificate;
	private String PIN;
	private String sign;

	// must be same to app-security.xml
	private String key = "RememberAppUser";

	// private String rememberMe;//自动登录
	@Resource
	private AppUserService userService;
	@Resource(name = "customAuthenticationManager")
	private AuthenticationManager authenticationManager = null;

	/**
	 * 登录
	 * 
	 * @return
	 */
	public String login() {
		StringBuffer msg = new StringBuffer("{msg:'");
		Captcha captcha = (Captcha) getSession().getAttribute(Captcha.NAME);
		Boolean login = false;

		String newPassword = null;

		if (!"".equals(username) && username != null) { // username != "" ou null
			setUser(userService.findByUserName(username));
			if (user != null) {
				if (StringUtils.isNotEmpty(password)) {
					newPassword = StringUtil.encryptSha256(password); // encryptSha256
					if (user.getPassword().equalsIgnoreCase(newPassword)) { // sans case => à modifier password encrypté
						if (captcha.isCorrect(checkCode)) {
							if (user.getStatus() == 1) {
								login = true;
							} else
								msg.append("此用户已被禁用.'");
						} else
							msg.append("验证码不正确.'");
					} else
						msg.append("密码不正确.'");
				} else
					msg.append("密码不能为空.'");
			} else
				msg.append("用户不存在.'");

		}

		if (login) {
			UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username,
					password);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			securityContext.setAuthentication(authenticationManager.authenticate(authRequest));
			SecurityContextHolder.setContext(securityContext);
			getSession().setAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY, username);
			String rememberMe = getRequest().getParameter("_spring_security_remember_me");
			if (rememberMe != null && rememberMe.equals("on")) {
				// 加入cookie
				long tokenValiditySeconds = 1209600; // 14 days
				long tokenExpiryTime = System.currentTimeMillis() + (tokenValiditySeconds * 1000);
				// DigestUtils.md5Hex(username + ":" + tokenExpiryTime + ":" + password + ":" +
				// getKey());
				String signatureValue = DigestUtils
						.md5Hex(username + ":" + tokenExpiryTime + ":" + user.getPassword() + ":" + key);

				String tokenValue = username + ":" + tokenExpiryTime + ":" + signatureValue;
				String tokenValueBase64 = new String(Base64.encodeBase64(tokenValue.getBytes()));
				getResponse().addCookie(makeValidCookie(tokenExpiryTime, tokenValueBase64));

			}

			setJsonString("{success:true}");
		} else {
			msg.append(",failure:true}");
			setJsonString(msg.toString());
		}
		return SUCCESS;
	}

	/**
	 * 登录InfoSec
	 * 
	 * @return
	 */
	public String loginIS() {
		//System.out.println("[*] Res: " + res);
		Captcha captcha = (Captcha) getSession().getAttribute(Captcha.NAME);
		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, new AppUserLoginCredential(password, challenge, new BigInteger(res, 16)));
		CertificateAuthenticationTokenIS authCertRequest = new CertificateAuthenticationTokenIS(certificate, PIN, sign);
		Authentication authCert = authenticationManager.authenticate(authCertRequest);
		Authentication auth = authenticationManager.authenticate(authRequest);
		if (null != auth && null != authCert) {
			getSession().setAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY, username);
			String rememberMe = getRequest().getParameter("_spring_security_remember_me");
			if (rememberMe != null && rememberMe.equals("on")) {
				// 加入cookie
				long tokenValiditySeconds = 1209600; // 14 days
				long tokenExpiryTime = System.currentTimeMillis() + (tokenValiditySeconds * 1000);
				// DigestUtils.md5Hex(username + ":" + tokenExpiryTime + ":" + password + ":" +
				// getKey());
				String signatureValue = DigestUtils.md5Hex(username + ":" + tokenExpiryTime + ":" + password + ":" + key);
				String tokenValue = username + ":" + tokenExpiryTime + ":" + signatureValue;
				
				String tokenValueBase64 = new String(Base64.encodeBase64(tokenValue.getBytes()));
				
				getResponse().addCookie(makeValidCookie(tokenExpiryTime, tokenValueBase64));
			}
			setJsonString("{success:true}");
		} else {
			setJsonString("{msg:'Authentication failed.',failure:true}");
		}
		return SUCCESS;
	}

	// add Cookie
	protected Cookie makeValidCookie(long expiryTime, String tokenValueBase64) {
		HttpServletRequest request = getRequest();
		Cookie cookie = new Cookie(TokenBasedRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY,
				tokenValueBase64);
		cookie.setMaxAge(60 * 60 * 24 * 365 * 5); // 5 years
		cookie.setPath(
				org.springframework.util.StringUtils.hasLength(request.getContextPath()) ? request.getContextPath()
						: "/");
		/*
		 * set cookie path Path=<path-value> 可选 指定一个 URL 路径，这个路径必须出现在要请求的资源的路径中才可以发送
		 * Cookie 首部。字符 %x2F ("/") 可以解释为文件目录分隔符，此目录的下级目录也满足匹配的条件（例如，如果 path=/docs，那么
		 * "/docs", "/docs/Web/" 或者 "/docs/Web/HTTP" 都满足匹配的条件）。
		 */
		return cookie;
	}

	/**
	 * Générer une challenge
	 * 
	 * @return
	 */
	public String challenge() {
		StringBuffer sb = new StringBuffer();
		Random rand = new Random();
		BigInteger number = new BigInteger(8, rand); // 8 bits
		setChallenge(number);
		sb.append("{success:true,challenge:").append(number.toString()).append("}");
		System.out.println("[*] Login Challenge: " + sb.toString());
		setJsonString(sb.toString());
		return SUCCESS;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public BigInteger getChallenge() {
		return challenge;
	}

	public void setChallenge(BigInteger challenge) {
		this.challenge = challenge;
	}

	public String getRes() {
		return res;
	}

	public void setRes(String response) {
		this.res = response;
	}

	public CertificateIS getCertificate() {
		return certificate;
	}

	public void setCertificate(CertificateIS certificate) {
		this.certificate = certificate;
	}

	public String getPIN() {
		return PIN;
	}

	public void setPIN(String pIN) {
		PIN = pIN;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	
}
