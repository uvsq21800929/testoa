package com.htsoft.core.security.model;

import java.math.BigInteger;

public class AppUserLoginCredential {
	private String password;
	private BigInteger challenge;
	private BigInteger response;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public BigInteger getChallenge() {
		return challenge;
	}
	public void setChallenge(BigInteger challenge) {
		this.challenge = challenge;
	}
	
	public BigInteger getResponse() {
		return response;
	}
	public void setResponse(BigInteger response) {
		this.response = response;
	}
	public AppUserLoginCredential(String password, BigInteger challenge, BigInteger response) {
		super();
		this.password = password;
		this.challenge = challenge;
		this.response = response;
	}
	@Override
	public String toString() {
		return "AppUserLoginCredential [password=" + password + ", challenge=" + challenge + ", response=" + response
				+ "]";
	}

	
	
	

}
