package com.htsoft.core.security;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;

import com.htsoft.core.security.model.AppUserLoginCredential;
import com.htsoft.oa.dao.system.AppRoleDao;
import com.htsoft.oa.dao.system.AppUserDao;
import com.htsoft.oa.model.system.AppRight;
import com.htsoft.oa.model.system.AppRole;
import com.htsoft.oa.model.system.AppRoleMappedIS;
import com.htsoft.oa.model.system.AppUser;
import com.htsoft.oa.model.system.role.AdministrationManager;
import com.htsoft.oa.model.system.role.AnonymousUser;
import com.htsoft.oa.model.system.role.AuthenticatedUser;
import com.htsoft.oa.model.system.role.ClientManager;
import com.htsoft.oa.model.system.role.DocumentManager;
import com.htsoft.oa.model.system.role.HumanResourceManager;
import com.htsoft.oa.model.system.role.InformationManager;
import com.htsoft.oa.model.system.role.SuperAdmin;
import com.htsoft.oa.service.system.AppUserService;

public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Resource
	private AppUserDao appUserDao;

	public AppUserDao getAppUserDao() {
		return appUserDao;
	}

	public void setAppUserDao(AppUserDao appUserDao) {
		this.appUserDao = appUserDao;
	}

	@Resource
	private AppRoleDao appRoleDao;

	public AppRoleDao getAppRoleDao() {
		return appRoleDao;
	}

	public void setAppRoleDao(AppRoleDao appRoleDao) {
		this.appRoleDao = appRoleDao;
	}

	private String key;

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		Assert.hasLength(this.key, "key must have length");
		// String key = ""; // 128 bit key
		String username = authentication.getName();
		AppUserLoginCredential credential = (AppUserLoginCredential) authentication.getCredentials();
		String password = credential.getPassword();
		BigInteger challenge = credential.getChallenge();
		BigInteger response = credential.getResponse();
		String decryptedPassword = null;

		// Create key and cipher
		Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher;

		// can not load roles automatiquement
		// String sqlUser = "select * from user_role as UR, app_role as AR, app_user as
		// au where au.userId=ur.userId and UR.roleId=AR.roleId and au.username=?";
		// String[] objs = new String[1];
		// objs[0] = username;
		// AppUser user = appUserDao.findUserDetailByJdbcIS(sqlUser, objs,
		// AppUser.class);

		AppUser user = appUserDao.findByUserNameIS(username);
		if (null != user) {
			try {
				cipher = Cipher.getInstance("AES");
				cipher.init(Cipher.DECRYPT_MODE, aesKey);
				decryptedPassword = new String(cipher.doFinal(Base64.decodeBase64((user.getPassword().getBytes())))); // plaintext -> byte[] -> base64 (pour l'affichage)
				Mac sha512Hmac;
				try {
					final byte[] byteKey = decryptedPassword.getBytes();
					sha512Hmac = Mac.getInstance("HmacSHA256");
					SecretKeySpec keySpec = new SecretKeySpec(byteKey, "HmacSHA256");
					sha512Hmac.init(keySpec);
					byte[] macData = sha512Hmac.doFinal(challenge.toString().getBytes());				
					if (new BigInteger(macData).intValue() == response.intValue()) {// OVERFLOW!!!
						AppRole appRole = appRoleDao.getByRoleName(AuthenticatedUser.roleIcon);
						AuthenticatedUser authenticatedRole = new AuthenticatedUser(appRole);
						user.getAuthenticatedIS(authenticatedRole);
						Long[] userId = new Long[1];
						userId[0] = user.getUserId();
						String sql = "select * from user_role as UR, app_role as AR where UR.userId=? and UR.roleId=AR.roleId";
						List<AppRole> userRoles = appRoleDao.findByJdbcIS(sql, userId, AppRole.class);
						for (AppRole ar : userRoles) {
							if (ar.getAuthority().equals(AnonymousUser.roleIcon))
								user.getRoles().add(new AnonymousUser(ar));
							if (ar.getAuthority().equals(AuthenticatedUser.roleIcon))
								user.getRoles().add(new AuthenticatedUser(ar));
							if (ar.getAuthority().equals(AdministrationManager.roleIcon))
								user.getRoles().add(new AdministrationManager(ar));
							if (ar.getAuthority().equals(ClientManager.roleIcon))
								user.getRoles().add(new ClientManager(ar));
							if (ar.getAuthority().equals(InformationManager.roleIcon))
								user.getRoles().add(new InformationManager(ar));
							if (ar.getAuthority().equals(DocumentManager.roleIcon))
								user.getRoles().add(new DocumentManager(ar));
							if (ar.getAuthority().equals(HumanResourceManager.roleIcon))
								user.getRoles().add(new HumanResourceManager(ar));
							if (ar.getAuthority().equals(SuperAdmin.roleIcon))
								user.getRoles().add(new SuperAdmin(ar));
						}
						for (AppRole ar : user.getRoles()) {
							Set<AppRight> rights = ar.getRights();
							for (AppRight r : rights) {
								user.addRightIS(r);
							}
						}

						System.out.println("[*] Login Success. " + user.getRights() + " " + user.getRoles());

						Authentication auth = new UsernamePasswordAuthenticationToken(user, credential,
								user.getRoles());
						SecurityContext securityContext = SecurityContextHolder.getContext();
						securityContext.setAuthentication(auth); // authentication
						// SecurityContextHolder.setContext(securityContext);

						return auth;

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// Put any cleanup here
					// System.out.println("[*]");
				}
			} catch (NoSuchAlgorithmException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchPaddingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("[*] Login failed.");
		return null;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
