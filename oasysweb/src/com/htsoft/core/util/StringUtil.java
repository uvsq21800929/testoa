package com.htsoft.core.util;

import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
/*
 *  广州宏天软件有限公司 OA办公管理系统   -- http://www.jee-soft.cn
 *  Copyright (C) 2008-2009 GuangZhou HongTian Software Company
*/
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

import com.htsoft.core.util.cryptacular.PemUtil;

import edu.emory.mathcs.backport.java.util.Arrays;
import sun.security.util.DerInputStream;
import sun.security.util.DerValue;

/**
 * 字符转换类
 * 
 * @author csx
 * 
 */
public class StringUtil {

	/**
	 * 将16进制字符转换成byte[]数组。与byte2Hex功能相反。
	 *
	 * @param string 16进制字符串
	 * @return byte[]数组
	 */
	public static byte[] hex2Byte(String string) {
		if (string == null || string.length() < 1) {
			return null;
		}
		// 因为一个byte生成两个字符，长度对应1:2，所以byte[]数组长度是字符串长度一半
		byte[] bytes = new byte[string.length() / 2];
		// 遍历byte[]数组，遍历次数是字符串长度一半
		for (int i = 0; i < string.length() / 2; i++) {
			// 截取没两个字符的前一个，将其转为int数值
			int high = Integer.parseInt(string.substring(i * 2, i * 2 + 1), 16);
			// 截取没两个字符的后一个，将其转为int数值
			int low = Integer.parseInt(string.substring(i * 2 + 1, i * 2 + 2), 16);
			// 高位字符对应的int值*16+低位的int值，强转成byte数值即可
			// 如dd，高位13*16+低位13=221(强转成byte二进制11011101，对应十进制-35)
			bytes[i] = (byte) (high * 16 + low);
		}
		return bytes;
	}

	public static byte[] toBytes(String str) {
		if (str == null || str.trim().equals("")) {
			return new byte[0];
		}

		byte[] bytes = new byte[str.length() / 2];
		for (int i = 0; i < str.length() / 2; i++) {
			String subStr = str.substring(i * 2, i * 2 + 2);
			bytes[i] = (byte) Integer.parseInt(subStr, 16);
		}

		return bytes;
	}

	/**
	 * 把字符串中的带‘与"转成\'与\"
	 * 
	 * @param orgStr
	 * @return
	 */
	public static String convertQuot(String orgStr) {
		return orgStr.replace("'", "\\'").replace("\"", "\\\"");
	}

	public static synchronized String encryptSha256(String inputStr) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");

			byte digest[] = md.digest(inputStr.getBytes("UTF-8"));

			return new String(Base64.encodeBase64(digest));

			// return (new BASE64Encoder()).encode(digest);
			// return new String(Hex.encodeHex(digest));
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		/*
		 * String password = "111"; String result = encryptSha256(password);
		 * 
		 * // byte[]dis={-10, -32, -95, -30, -84, 65, -108, 90, -102, -89, -1, // -118,
		 * -118, -86, 12, -21, -63, 42, 59, -52, -104, 26, -110, -102, // -43, -49,
		 * -127, 10, 9, 14, 17, -82}; // System.out.println("array:"+ new
		 * String(Hex.encodeHex(dis))); System.out.println("result:" + result);
		 */
		
		String pubKeyStr = "3082010a0282010100be44fda694cd24c04f43215112df10989c852e30224e177d1907946fea9c585b9aaad059a86f15730c1f640163944075d466a268fc80142c1fcb087cae1d2c175c983f2dc8c0116a4b895cbef2e655593afc45f7a3a7cc0060056a90e54f21a3aad65d1295f1317c234c5637f26d94609bdf61e5baea7caa053a31d2bc5afc4fc0ade4b1f6ca814f8e1dd21936d9ba806485e6dd65d701d02ef0119494f2ed4f42680cd83f01119afaecbda44da582855cd3a311861976735a497845b89e834bc705fbd21ea10557128a0732d784290a2cfae43474492e540d11811cf2e311c93db305763b2c48d4b04a25b5823d941683439e1499b33a5b8c64645f3c656cf30203010001";
		String n = "00be44fda694cd24c04f43215112df10989c852e30224e177d1907946fea9c585b9aaad059a86f15730c1f640163944075d466a268fc80142c1fcb087cae1d2c175c983f2dc8c0116a4b895cbef2e655593afc45f7a3a7cc0060056a90e54f21a3aad65d1295f1317c234c5637f26d94609bdf61e5baea7caa053a31d2bc5afc4fc0ade4b1f6ca814f8e1dd21936d9ba806485e6dd65d701d02ef0119494f2ed4f42680cd83f01119afaecbda44da582855cd3a311861976735a497845b89e834bc705fbd21ea10557128a0732d784290a2cfae43474492e540d11811cf2e311c93db305763b2c48d4b04a25b5823d941683439e1499b33a5b8c64645f3c656cf3";
		String e = "010001";
		String pubKey_ = "3082020a0282020100bec8198361c305ac4f502e1e07f2012f076b6ddac46d73d59d71faf22403b494f3946a10308dcb447b7ef294c42659666ff136726778a1d96561f189138d2a66514c3e0378b8991f81597179e3b278a327a0c0fb8ee14f07c312d6ce028ba438086843a0a7723510e67d84e76fe8f35419b5933c75029da82cfed0b66612f4be39802eb12480379d627fc21c75eda7c39f4abe80d2555d1a55e3c742bdfa4210d3c61bc4e62b609febf2638c8deda1a8e7377d8a509a0e2c8876807a918af064c7a26bbbedac936d8bf1755280d645b0d8c20bf96edd97f22e86fda332d477bda9a16ffda93aedecd9f317562c528df144415458ee613c72e0b8fb078f9d7269bac9b17b68bf73b8b017533a29f4c5d3172ae52390cd5442eb4011df9968c5b138ca826e5503a46bad34866847adac8120d173f65971b0d34001bff599dd040aaf7367cc0e6c0f193d33c8a088eb016f21af15b3589757a60cdf7094f17af70fdf1278bf74ca5726258d57d5ed66c47189bd2ac8d8d26d575f695cf8c54c6184dee03cd765d2f84153b131f42904c6242306c22e1bda54af1ac2eb44731f295d396ffb2ac4471a4677b2f1bf6f05981b467cf6536f93c187fb7654a81e43890e96e7ff62e89269f631e79dd0a491b1e52289d72de245460076cac8f02719331313f7d228d5fad1c4e106a5b57286210c97ad38180848333cf70438adda041853";
		String pubKey2 ="30818902818100bbec7bc3f6e21b3820c2cda83c5b04ba0cfc125ec878ab2180a78fc9d25922b0ea1ce5c9bf8d036c58a8e2a83d2d42f88e63484d0d4f733971db6c3e91e416c4f206650009467e9d1ef0a894f0cd4a86eeb60ab37cb3c2fdf7b484818d864f495f964b6c6014f968a80e702a479565c96305a60f918178b350307f43676736d90203010001";
		String pubKey3= "30819f300d06092a864886f70d010101050003818d0030818902818100c8d3e16602ef1168e424707474ac78eb00a99295d8ab2b8e9e78cb2b1b6bae936d527d022d568a80ac3fb5819024a731b1027dbd637c9977ee49b69a830cae1aca5cd9ab2a13b990428b299419760fbfc15ba0172cd187110e2016c443a9887b259579ee33920f91f4623282df661a1ac0841aaa3b59596809f9fe2178dec78b0203010001";

		int eBegin = pubKeyStr.lastIndexOf("0203");
		String eStr = pubKeyStr.substring(eBegin+4, pubKeyStr.length());
		pubKeyStr = pubKeyStr.substring(0, eBegin);
		
		int nBegin = pubKeyStr.indexOf("3082010a02820101");
		String nStr = pubKeyStr.substring(nBegin+16); 
		System.out.println(nStr);
		System.out.println(eStr);
		try {
			BigInteger number_n = new BigInteger(nStr,16);
			BigInteger number_e = new BigInteger(eStr,16);
			System.out.println(number_n);
			System.out.println(number_e);
			RSAPublicKeySpec pubSpec = new RSAPublicKeySpec(number_n, number_e);
			KeyFactory factory = KeyFactory.getInstance("RSA");
			PublicKey pubKey = factory.generatePublic(pubSpec);
			System.out.println(pubKey);
			//RSA_PEM.FromPEM(pubKey);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/*
		byte[] keyBytes = StringUtil.hex2Byte(pubKey); 需要公钥pubKey,收到的为PKCS8 PKCS8->PUBKEY->NEW X509ENCODEDKEY
		//SubjectPublicKeyInfo spkInfo = SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(keyBytes));
		java.security.Security.addProvider(
		         new org.bouncycastle.jce.provider.BouncyCastleProvider()
		);

		RSAPublicKey publicKey;
		
		try {
			//ASN1Primitive primitive = spkInfo.parsePublicKey();
		//	byte[] publicKeyPKCS1 = primitive.getEncoded();

			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
			publicKey = (RSAPublicKey) keyFactory.generatePublic(keySpec);
			System.out.println(publicKey);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

	public static String html2Text(String inputString) {
		String htmlStr = inputString; // 含html标签的字符串
		String textStr = "";
		java.util.regex.Pattern p_script;
		java.util.regex.Matcher m_script;
		java.util.regex.Pattern p_style;
		java.util.regex.Matcher m_style;
		java.util.regex.Pattern p_html;
		java.util.regex.Matcher m_html;

		try {
			String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; // 定义script的正则表达式{或<script>]*?>[\s\S]*?<\/script>
																										// }
			String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; // 定义style的正则表达式{或<style>]*?>[\s\S]*?<\/style>
																									// }
			String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

			p_script = java.util.regex.Pattern.compile(regEx_script, java.util.regex.Pattern.CASE_INSENSITIVE);
			m_script = p_script.matcher(htmlStr);
			htmlStr = m_script.replaceAll(""); // 过滤script标签

			p_style = java.util.regex.Pattern.compile(regEx_style, java.util.regex.Pattern.CASE_INSENSITIVE);
			m_style = p_style.matcher(htmlStr);
			htmlStr = m_style.replaceAll(""); // 过滤style标签

			p_html = java.util.regex.Pattern.compile(regEx_html, java.util.regex.Pattern.CASE_INSENSITIVE);
			m_html = p_html.matcher(htmlStr);
			htmlStr = m_html.replaceAll(""); // 过滤html标签

			textStr = htmlStr;

		} catch (Exception e) {
			System.err.println("Html2Text: " + e.getMessage());
		}

		return textStr;// 返回文本字符串
	}
}
